.. _best-pkging-practices:

Best Packaging Practices
********************************************************************************************************************************

Debian's quality is largely due to the `Debian
Policy <https://www.debian.org/doc/debian-policy/>`__, which defines
explicit baseline requirements that all Debian packages must fulfill.
Yet there is also a shared history of experience which goes beyond the
Debian Policy, an accumulation of years of experience in packaging. Many
very talented people have created great tools, tools which help you, the
Debian maintainer, create and maintain excellent packages.

This chapter provides some best practices for Debian developers. All
recommendations are merely that, and are not requirements or policy.
These are just some subjective hints, advice and pointers collected from
Debian developers. Feel free to pick and choose whatever works best for
you.

.. _bpp-debian-rules:

Best practices for ``debian/rules``
================================================================================================================================

The following recommendations apply to the ``debian/rules`` file. Since
``debian/rules`` controls the build process and selects the files that
go into the package (directly or indirectly), it's usually the file
maintainers spend the most time on.

.. _helper-scripts:

Helper scripts
--------------------------------------------------------------------------------------------------------------------------------

The rationale for using helper scripts in ``debian/rules`` is that they
let maintainers use and share common logic among many packages. Take for
instance the question of installing menu entries: you need to put the
file into ``/usr/share/menu`` (or ``/usr/lib/menu`` for executable
binary menufiles, if this is needed), and add commands to the maintainer
scripts to register and unregister the menu entries. Since this is a
very common thing for packages to do, why should each maintainer rewrite
all this on their own, sometimes with bugs? Also, supposing the menu
directory changed, every package would have to be changed.

Helper scripts take care of these issues. Assuming you comply with the
conventions expected by the helper script, the helper takes care of all
the details. Changes in policy can be made in the helper script; then
packages just need to be rebuilt with the new version of the helper and
no other changes.

:ref:`tools` contains a couple of different helpers. The most common
and best (in our opinion) helper system is ``debhelper``. Previous
helper systems, such as ``debmake``, were monolithic: you couldn't pick
and choose which part of the helper you found useful, but had to use the
helper to do everything. ``debhelper``, however, is a number of separate
little ``dh_*`` programs. For instance, ``dh_installman`` installs and
compresses man pages, ``dh_installmenu`` installs menu files, and so on.
Thus, it offers enough flexibility to be able to use the little helper
scripts, where useful, in conjunction with hand-crafted commands in
``debian/rules``.

You can get started with ``debhelper`` by reading debhelper 1, and
looking at the examples that come with the package. ``dh_make``, from
the ``dh-make`` package (see :ref:`dh-make`), can be used to convert
a vanilla source package to a ``debhelper``\ ized package. This
shortcut, though, should not convince you that you do not need to bother
understanding the individual ``dh_*`` helpers. If you are going to use a
helper, you do need to take the time to learn to use that helper, to
learn its expectations and behavior.

.. _multiple-patches:

Separating your patches into multiple files
--------------------------------------------------------------------------------------------------------------------------------

Big, complex packages may have many bugs that you need to deal with. If
you correct a number of bugs directly in the source, and you're not
careful, it can get hard to differentiate the various patches that you
applied. It can get quite messy when you have to update the package to a
new upstream version which integrates some of the fixes (but not all).
You can't take the total set of diffs (e.g., from ``.diff.gz``) and work
out which patch sets to back out as a unit as bugs are fixed upstream.

Fortunately, with the source format “3.0 (quilt)” it is now possible to
keep patches separate without having to modify ``debian/rules`` to set
up a patch system. Patches are stored in ``debian/patches/`` and when
the source package is unpacked patches listed in
``debian/patches/series`` are automatically applied. As the name
implies, patches can be managed with ``quilt``.

When using the older source “1.0”, it's also possible to separate
patches but a dedicated patch system must be used: the patch files are
shipped within the Debian patch file (``.diff.gz``), usually within the
``debian/`` directory. The only difference is that they aren't applied
immediately by ``dpkg-source``, but by the ``build`` rule of
``debian/rules``, through a dependency on the ``patch`` rule.
Conversely, they are reverted in the ``clean`` rule, through a
dependency on the ``unpatch`` rule.

``quilt`` is the recommended tool for this. It does all of the above,
and also allows one to manage patch series. See the ``quilt`` package
for more information.

.. _multiple-binary:

Multiple binary packages
--------------------------------------------------------------------------------------------------------------------------------

A single source package will often build several binary packages, either
to provide several flavors of the same software (e.g., the ``vim``
source package) or to make several small packages instead of a big one
(e.g., so the user can install only the subset needed, and thus save
some disk space, see for example the ``libxml2`` source package).

The second case can be easily managed in ``debian/rules``. You just need
to move the appropriate files from the build directory into the
package's temporary trees. You can do this using ``install`` or
``dh_install`` from ``debhelper``. Be sure to check the different
permutations of the various packages, ensuring that you have the
inter-package dependencies set right in ``debian/control``.

The first case is a bit more difficult since it involves multiple
recompiles of the same software but with different configuration
options. The ``vim`` source package is an example of how to manage this
using a hand-crafted ``debian/rules`` file.

.. _bpp-debian-control:

Best practices for ``debian/control``
================================================================================================================================

The following practices are relevant to the ``debian/control`` file.
They supplement the `Policy on package
descriptions <https://www.debian.org/doc/debian-policy/ch-binary.html#s-descriptions>`__.

The description of the package, as defined by the corresponding field in
the ``control`` file, contains both the package synopsis and the long
description for the package. :ref:`bpp-desc-basics` describes common guidelines for both
parts of the package description. Following that, :ref:`bpp-pkg-synopsis` provides guidelines specific
to the synopsis, and :ref:`bpp-pkg-desc` contains
guidelines specific to the description.

.. _bpp-desc-basics:

General guidelines for package descriptions
--------------------------------------------------------------------------------------------------------------------------------

The package description should be written for the average likely user,
the average person who will use and benefit from the package. For
instance, development packages are for developers, and can be technical
in their language. More general-purpose applications, such as editors,
should be written for a less technical user.

Our review of package descriptions lead us to conclude that most package
descriptions are technical, that is, are not written to make sense for
non-technical users. Unless your package really is only for technical
users, this is a problem.

How do you write for non-technical users? Avoid jargon. Avoid referring
to other applications or frameworks that the user might not be familiar
with — GNOME or KDE is fine, since users are probably familiar with
these terms, but GTK is probably not. Try not to assume any knowledge at
all. If you must use technical terms, introduce them.

Be objective. Package descriptions are not the place for advocating your
package, no matter how much you love it. Remember that the reader may
not care about the same things you care about.

References to the names of any other software packages, protocol names,
standards, or specifications should use their canonical forms, if one
exists. For example, use X Window System, X11, or X; not X Windows,
X-Windows, or X Window. Use GTK, not GTK+ or gtk. Use GNOME, not Gnome.
Use PostScript, not Postscript or postscript.

If you are having problems writing your description, you may wish to
send it along to ``debian-l10n-english@lists.debian.org`` and request
feedback.

.. _bpp-pkg-synopsis:

The package synopsis, or short description
--------------------------------------------------------------------------------------------------------------------------------

Policy says the synopsis line (the short description) must be concise,
not repeating the package name, but also informative.

The synopsis functions as a phrase describing the package, not a
complete sentence, so sentential punctuation is inappropriate: it does
not need extra capital letters or a final period (full stop). It should
also omit any initial indefinite or definite article — "a", "an", or
"the". Thus for instance:

.. code-block:: debcontrol

   Package: libeg0
   Description: exemplification support library

Technically this is a noun phrase minus articles, as opposed to a verb
phrase. A good heuristic is that it should be possible to substitute the
package *name* and *synopsis* into this formula:

The package *name* provides {a,an,the,some} *synopsis*.

Sets of related packages may use an alternative scheme that divides the
synopsis into two parts, the first a description of the whole suite and
the second a summary of the package's role within it:

.. code-block:: debcontrol

   Package: eg-tools
   Description: simple exemplification system (utilities)
                             
   Package: eg-doc
   Description: simple exemplification system - documentation

These synopses follow a modified formula. Where a package "*name*" has a
synopsis "*suite* (*role*)" or "*suite* - *role*", the elements should
be phrased so that they fit into the formula:

The package *name* provides {a,an,the} *role* for the *suite*.

.. _bpp-pkg-desc:

The long description
--------------------------------------------------------------------------------------------------------------------------------

The long description is the primary information available to the user
about a package before they install it. It should provide all the
information needed to let the user decide whether to install the
package. Assume that the user has already read the package synopsis.

The long description should consist of full and complete sentences.

The first paragraph of the long description should answer the following
questions: what does the package do? what task does it help the user
accomplish? It is important to describe this in a non-technical way,
unless of course the audience for the package is necessarily technical.

Long descriptions of related packages,
for example built from the same source,
can share paragraphs in order to increase consistency
and reduce the workload for translators,
but you need at least one separate paragraph describing
the package's specific role.

The following paragraphs should answer the following questions: Why do I
as a user need this package? What other features does the package have?
What outstanding features and deficiencies are there compared to other
packages (e.g., if you need X, use Y instead)? Is this package related
to other packages in some way that is not handled by the package manager
(e.g., is this the client for the foo server)?

Be careful to avoid spelling and grammar mistakes. Ensure that you
spell-check it. Both ``ispell`` and ``aspell`` have special modes for
checking ``debian/control`` files:

::

   ispell -d american -g debian/control

::

   aspell -d en -D -c debian/control

Users usually expect these questions to be answered in the package
description:

-  What does the package do? If it is an add-on to another package, then
   the short description of the package we are an add-on to should be
   put in here.

-  Why should I want this package? This is related to the above, but not
   the same (this is a mail user agent; this is cool, fast, interfaces
   with PGP and LDAP and IMAP, has features X, Y, and Z).

-  If this package should not be installed directly, but is pulled in by
   another package, this should be mentioned.

-  If the package is ``experimental``, or there are other reasons it
   should not be used, if there are other packages that should be used
   instead, it should be here as well.

-  How is this package different from the competition? Is it a better
   implementation? more features? different features? Why should I
   choose this package?

.. _bpp-upstream-info:

Upstream home page
--------------------------------------------------------------------------------------------------------------------------------

We recommend that you add the URL for the package's home page in the
``Homepage`` field of the ``Source`` section in ``debian/control``.
Adding this information in the package description itself is considered
deprecated.

.. _bpp-vcs:

Version Control System location
--------------------------------------------------------------------------------------------------------------------------------

There are additional fields for the location of the Version Control
System in ``debian/control``.

.. _s6.2.5.1:

Vcs-Browser
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Value of this field should be a ``https://`` URL pointing to a
web-browsable copy of the Version Control System repository used to
maintain the given package, if available.

The information is meant to be useful for the final user, willing to
browse the latest work done on the package (e.g. when looking for the
patch fixing a bug tagged as ``pending`` in the bug tracking system).

.. _s6.2.5.2:

Vcs-\*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Value of this field should be a string identifying unequivocally the
location of the Version Control System repository used to maintain the
given package, if available. ``*`` identifies the Version Control
System; currently the following systems are supported by the package
tracking system: ``arch``, ``bzr`` (Bazaar), ``cvs``, ``darcs``,
``git``, ``hg`` (Mercurial), ``mtn`` (Monotone), ``svn`` (Subversion).

The information is meant to be useful for a user knowledgeable in the
given Version Control System and willing to build the current version of
a package from the VCS sources. Other uses of this information might
include automatic building of the latest VCS version of the given
package. To this end the location pointed to by the field should better
be version agnostic and point to the main branch (for VCSs supporting
such a concept). Also, the location pointed to should be accessible to
the final user; fulfilling this requirement might imply pointing to an
anonymous access of the repository instead of pointing to an
SSH-accessible version of the same.

In the following example, an instance of the field for a Git
repository of the ``vim`` package is shown. Note how the URL is in the
``https://`` scheme (instead of ``ssh://``). The use of the
``Vcs-Browser`` and ``Homepage`` fields described above is also shown.

.. code-block:: debcontrol

     Source: vim
     <snip>
     Vcs-Git: https://salsa.debian.org/vim-team/vim.git
     Vcs-Browser: https://salsa.debian.org/vim-team/vim
     Homepage: https://www.vim.org

Maintaining the packaging in a version control system, and setting
a Vcs-\* header is good practice and makes it easier for
others to contribute changes.

Almost all packages in Debian that use a version control
system use Git; if you create a new package, using Git is a good
idea simply because it's the system that contributors will
be familiar with.

`DEP-14 <https://dep-team.pages.debian.net/deps/dep14/>`_ defines
a common layout for Debian packages.

.. _bpp-debian-changelog:

Best practices for ``debian/changelog``
================================================================================================================================

The following practices supplement the `Policy on changelog
files <https://www.debian.org/doc/debian-policy/ch-docs.html#s-changelogs>`__.

.. _bpp-changelog-do:

Writing useful changelog entries
--------------------------------------------------------------------------------------------------------------------------------

The changelog entry for a package revision documents changes in that
revision, and only them. Concentrate on describing significant and
user-visible changes that were made since the last version.

Focus on *what* was changed — who, how and when are usually less
important. Having said that, remember to politely attribute people who
have provided notable help in making the package (e.g., those who have
sent in patches).

There's no need to elaborate the trivial and obvious changes. You can
also aggregate several changes in one entry. On the other hand, don't be
overly terse if you have undertaken a major change. Be especially clear
if there are changes that affect the behaviour of the program. For
further explanations, use the ``README.Debian`` file.

Use common English so that the majority of readers can comprehend it.
Avoid abbreviations, tech-speak and jargon when explaining changes that
close bugs, especially for bugs filed by users that did not strike you
as particularly technically savvy. Be polite, don't swear.

It is sometimes desirable to prefix changelog entries with the names of
the files that were changed. However, there's no need to explicitly list
each and every last one of the changed files, especially if the change
was small or repetitive. You may use wildcards.

When referring to bugs, don't assume anything. Say what the problem was,
how it was fixed, and append the closes: #nnnnn string. See
:ref:`upload-bugfix` for more information.

.. _bpp-changelog-urgency:

Selecting the upload urgency
--------------------------------------------------------------------------------------------------------------------------------

The release team have indicated that they expect most uploads to
``unstable`` to use **urgency=medium**. That is, you should choose
**urgency=medium** unless there is some particular reason for the
upload to migrate to ``testing`` more quickly or slowly (see also
:ref:`testing-unstable`). For example, you might select
**urgency=low** if the changes since the last upload are large and
might be disruptive in unanticipated ways.

The delays are currently 2, 5 or 10 days, depending on the urgency
(high, medium or low). The actual numbers are actually controled by
the `britney configuration
<https://release.debian.org/britney/britney.conf>`__ which also
includes accelerated migrations when Autopkgtest passes.

.. _bpp-changelog-misconceptions:

Common misconceptions about changelog entries
--------------------------------------------------------------------------------------------------------------------------------

The changelog entries should **not** document generic packaging issues
(Hey, if you're looking for foo.conf, it's in /etc/blah/.), since
administrators and users are supposed to be at least remotely acquainted
with how such things are generally arranged on Debian systems. Do,
however, mention if you change the location of a configuration file.

The only bugs closed with a changelog entry should be those that are
actually fixed in the same package revision. Closing unrelated bugs in
the changelog is bad practice. See :ref:`upload-bugfix`.

The changelog entries should **not** be used for random discussion with
bug reporters (I don't see segfaults when starting foo with option bar;
send in more info), general statements on life, the universe and
everything (sorry this upload took me so long, but I caught the flu), or
pleas for help (the bug list on this package is huge, please lend me a
hand). Such things usually won't be noticed by their target audience,
but may annoy people who wish to read information about actual changes
in the package. See :ref:`bug-answering` for more information on how
to use the bug tracking system.

It is an old tradition to acknowledge bugs fixed in non-maintainer
uploads in the first changelog entry of the proper maintainer upload. As
we have version tracking now, it is enough to keep the NMUed changelog
entries and just mention this fact in your own changelog entry.

.. _bpp-changelog-errors:

Common errors in changelog entries
--------------------------------------------------------------------------------------------------------------------------------

The following examples demonstrate some common errors or examples of bad
style in changelog entries.

::

     * Fixed all outstanding bugs.

This doesn't tell readers anything too useful, obviously.

::

     * Applied patch from Jane Random.

What was the patch about?

::

     * Late night install target overhaul.

Overhaul which accomplished what? Is the mention of late night supposed
to remind us that we shouldn't trust that code?

::

     * Fix vsync fw glitch w/ ancient CRTs.

Too many acronyms (what does "fw" mean, "firmware"?), and it's not
overly clear what the glitch was actually about, or how it was fixed.

::

     * This is not a bug, closes: #nnnnnn.

First of all, there's absolutely no need to upload the package to convey
this information; instead, use the bug tracking system. Secondly,
there's no explanation as to why the report is not a bug.

::

     * Has been fixed for ages, but I forgot to close; closes: #54321.

If for some reason you didn't mention the bug number in a previous
changelog entry, there's no problem, just close the bug normally in the
BTS. There's no need to touch the changelog file, presuming the
description of the fix is already in (this applies to the fixes by the
upstream authors/maintainers as well; you don't have to track bugs that
they fixed ages ago in your changelog).

::

     * Closes: #12345, #12346, #15432

Where's the description? If you can't think of a descriptive message,
start by inserting the title of each different bug.

.. _bpp-news-debian:

Supplementing changelogs with ``NEWS.Debian`` files
--------------------------------------------------------------------------------------------------------------------------------

Important news about changes in a package can also be put in
``NEWS.Debian`` files. The news will be displayed by tools like
``apt-listchanges``, before all the rest of the changelogs. This is the
preferred means to let the user know about significant changes in a
package. It is better than using ``debconf`` notes since it is less
annoying and the user can go back and refer to the ``NEWS.Debian`` file
after the install. And it's better than listing major changes in
``README.Debian``, since the user can easily miss such notes.

The file format is the same as a debian changelog file, but leave off
the asterisks and describe each news item with a full paragraph when
necessary rather than the more concise summaries that would go in a
changelog. It's a good idea to run your file through
``dpkg-parsechangelog`` to check its formatting as it will not be
automatically checked during build as the changelog is. Here is an
example of a real ``NEWS.Debian`` file:

::

   cron (3.0pl1-74) unstable; urgency=low

       The checksecurity script is no longer included with the cron package:
       it now has its own package, checksecurity. If you liked the
       functionality provided with that script, please install the new
       package.

    -- Steve Greenland <stevegr@debian.org>  Sat,  6 Sep 2003 17:15:03 -0500

The ``NEWS.Debian`` file is installed as
``/usr/share/doc/``\ *package*\ ``/NEWS.Debian.gz``. It is compressed,
and always has that name even in Debian native packages. If you use
``debhelper``, ``dh_installchangelogs`` will install ``debian/NEWS``
files for you.

Unlike changelog files, you need not update ``NEWS.Debian`` files with
every release. Only update them if you have something particularly
newsworthy that user should know about. If you have no news at all,
there's no need to ship a ``NEWS.Debian`` file in your package. No news
is good news!

.. _bpp-security:

Best practices around security
================================================================================================================================

A set of suggestions and links to other reference documents around
security aspects for packaging can be found at the `Developer's Best
Practices for OS Security chapter inside the Securing Debian Manual
<https://www.debian.org/doc/manuals/securing-debian-manual/ch09.en.html>`__.

.. _bpp-debian-maint-scripts:

Best practices for maintainer scripts
================================================================================================================================

Maintainer scripts include the files ``debian/postinst``,
``debian/preinst``, ``debian/prerm`` and ``debian/postrm``. These
scripts take care of any package installation or deinstallation setup
that isn't handled merely by the creation or removal of files and
directories. The following instructions supplement the `Debian
Policy <https://www.debian.org/doc/debian-policy/>`__.

Maintainer scripts must be idempotent. That means that you need to make
sure nothing bad will happen if the script is called twice where it
would usually be called once.

Standard input and output may be redirected (e.g. into pipes) for
logging purposes, so don't rely on them being a tty.

All prompting or interactive configuration should be kept to a minimum.
When it is necessary, you should use the ``debconf`` package for the
interface. Remember that prompting in any case can only be in the
``configure`` stage of the ``postinst`` script.

Keep the maintainer scripts as simple as possible. We suggest you use
pure POSIX shell scripts. Remember, if you do need any bash features,
the maintainer script must have a bash shebang line. POSIX shell or Bash
are preferred to Perl, since they enable ``debhelper`` to easily add
bits to the scripts.

If you change your maintainer scripts, be sure to test package removal,
double installation, and purging. Be sure that a purged package is
completely gone, that is, it must remove any files created, directly or
indirectly, in any maintainer script.

If you need to check for the existence of a command, you should use
something like

.. code-block:: sh

   if command -v install-docs > /dev/null; then ...

You can use this function to search ``$PATH`` for a command name, passed
as an argument. It returns true (zero) if the command was found, and
false if not. This is really the best way, since ``command -v`` is a
shell-builtin for many shells and is defined in POSIX.

Using ``which`` is an acceptable alternative, since it is from the required 
``debianutils`` package.

.. _bpp-config-mgmt:

Configuration management with ``debconf``
================================================================================================================================

``Debconf`` is a configuration management system that can be used by all
the various packaging scripts (``postinst`` mainly) to request feedback
from the user concerning how to configure the package. Direct user
interactions must now be avoided in favor of ``debconf`` interaction.
This will enable non-interactive installations in the future.

Debconf is a great tool but it is often poorly used. Many common
mistakes are listed in the debconf-devel 7 man page. It is something
that you must read if you decide to use debconf. Also, we document some
best practices here.

These guidelines include some writing style and typography
recommendations, general considerations about debconf usage as well as
more specific recommendations for some parts of the distribution (the
installation system for instance).

.. _s6.5.1:

Do not abuse debconf
--------------------------------------------------------------------------------------------------------------------------------

Since debconf appeared in Debian, it has been widely abused and several
criticisms received by the Debian distribution come from debconf abuse
with the need of answering a wide bunch of questions before getting any
little thing installed.

Keep usage notes to where they belong: the ``NEWS.Debian``, or
``README.Debian`` file. Only use notes for important notes that may
directly affect the package usability. Remember that notes will always
block the install until confirmed or bother the user by email.

Carefully choose the questions' priorities in maintainer scripts. See
debconf-devel 7 for details about priorities. Most questions should use
medium and low priorities.

.. _s6.5.2:

General recommendations for authors and translators
--------------------------------------------------------------------------------------------------------------------------------

.. _s6.5.2.1:

Write correct English
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Most Debian package maintainers are not native English speakers. So,
writing properly phrased templates may not be easy for them.

Please use (and abuse) ``debian-l10n-english@lists.debian.org`` mailing
list. Have your templates proofread.

Badly written templates give a poor image of your package, of your
work... or even of Debian itself.

Avoid technical jargon as much as possible. If some terms sound common
to you, they may be impossible to understand for others. If you cannot
avoid them, try to explain them (use the extended description). When
doing so, try to balance between verbosity and simplicity.

.. _s6.5.2.2:

Be kind to translators
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Debconf templates may be translated. Debconf, along with its sister
package ``po-debconf``, offers a simple framework for getting templates
translated by translation teams or even individuals.

Please use gettext-based templates. Install ``po-debconf`` on your
development system and read its documentation (``man po-debconf`` is a
good start).

Avoid changing templates too often. Changing template text induces more
work for translators, which will get their translation fuzzied. A fuzzy
translation is a string for which the original changed since it was
translated, therefore requiring some update by a translator to be
usable. When changes are small enough, the original translation is kept
in PO files but marked as ``fuzzy``.

If you plan to do changes to your original templates, please use the
notification system provided with the ``po-debconf`` package, namely the
``podebconf-report-po``, to contact translators. Most active translators
are very responsive and getting their work included along with your
modified templates will save you additional uploads. If you use
gettext-based templates, the translator's name and e-mail addresses are
mentioned in the PO files headers and will be used by
``podebconf-report-po``.

A recommended use of that utility is:

.. code-block:: sh

   cd debian/po && podebconf-report-po --call --languageteam --withtranslators --deadline="+10 days"

This command will first synchronize the PO and POT files in
``debian/po`` with the template files listed in
``debian/po/POTFILES.in``. Then, it will send a call for new
translations, in the ``debian-i18n@lists.debian.org`` mailing list.
Finally, it will also send a call for translation updates to the
language team (mentioned in the ``Language-Team`` field of each PO file)
as well as the last translator (mentioned in ``Last-translator``).

Giving a deadline to translators is always appreciated, so that they can
organize their work. Please remember that some translation teams have a
formalized translate/review process and a delay lower than 10 days is
considered as unreasonable. A shorter delay puts too much pressure on
translation teams and should be kept for very minor changes.

If in doubt, you may also contact the translation team for a given
language (debian-l10n-xxxxx@lists.debian.org), or the
``debian-i18n@lists.debian.org`` mailing list.

.. _s6.5.2.3:

Unfuzzy complete translations when correcting typos and spelling
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When the text of a debconf template is corrected and you are **sure**
that the change does **not** affect translations, please be kind to
translators and *unfuzzy* their translations.

If you don't do so, the whole template will not be translated as long as
a translator will send you an update.

To *unfuzzy* translations, you can use ``msguntypot`` (part of the
``po4a`` package).

1. Regenerate the POT and PO files.

   .. code-block:: sh

      debconf-updatepo

2. Make a copy of the POT file.

   .. code-block:: sh

      cp templates.pot templates.pot.orig

3. Make a copy of all the PO files.

   .. code-block:: sh

      mkdir po_fridge; cp *.po po_fridge

4. Change the debconf template files to fix the typos.

5. Regenerate the POT and PO files (again).

   .. code-block:: sh

      debconf-updatepo

   At this point, the typo fix fuzzied all the translations, and this
   unfortunate change is the only one between the PO files of your main
   directory and the one from the fridge. Here is how to solve this.

6. Discard fuzzy translation, restore the ones from the fridge.

   .. code-block:: sh

      cp po_fridge/*.po .

7. Manually merge the PO files with the new POT file, but taking the
   useless fuzzy into account.

   .. code-block:: sh

      msguntypot -o templates.pot.orig -n templates.pot *.po

8. Clean up.

   .. code-block:: sh

      rm -rf templates.pot.orig po_fridge

.. _s6.5.2.4:

Do not make assumptions about interfaces
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Templates text should not make reference to widgets belonging to some
debconf interfaces. Sentences like *If you answer Yes...* have no
meaning for users of graphical interfaces that use checkboxes for
boolean questions.

String templates should also avoid mentioning the default values in
their description. First, because this is redundant with the values seen
by the users. Also, because these default values may be different from
the maintainer choices (for instance, when the debconf database was
preseeded).

More generally speaking, try to avoid referring to user actions. Just
give facts.

.. _s6.5.2.5:

Do not use first person
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You should avoid the use of first person (*I will do this...* or *We
recommend...*). The computer is not a person and the Debconf templates
do not speak for the Debian developers. You should use neutral
construction. Those of you who already wrote scientific publications,
just write your templates like you would write a scientific paper.
However, try using the active voice if still possible, like *Enable this
if ...* instead of *This can be enabled if...*.

.. _s6.5.2.6:

Be gender neutral
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As a way of showing our commitment to our `diversity
statement <https://www.debian.org/intro/diversity>`__, please use
gender-neutral constructions in your writing. This means avoiding
pronouns like he/she when referring to a role (like "maintainer") whose
gender is unknown. Instead, you should use the plural form (`singular
they <https://en.wikipedia.org/wiki/Singular_they>`__).

.. _s6.5.3:

Templates fields definition
--------------------------------------------------------------------------------------------------------------------------------

This part gives some information which is mostly taken from the
debconf-devel 7 manual page.

.. _s6.5.3.1:

Type
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _s6.5.3.1.1:

string
^^^^^^

Results in a free-form input field that the user can type any string
into.

.. _s6.5.3.1.2:

password
^^^^^^^^

Prompts the user for a password. Use this with caution; be aware that
the password the user enters will be written to debconf's database. You
should probably clean that value out of the database as soon as is
possible.

.. _s6.5.3.1.3:

boolean
^^^^^^^

A true/false choice. Remember: true/false, **not yes/no**...

.. _s6.5.3.1.4:

select
^^^^^^

A choice between one of a number of values. The choices must be
specified in a field named 'Choices'. Separate the possible values with
commas and spaces, like this: ``Choices: yes, no, maybe``.

If choices are translatable strings, the 'Choices' field may be marked
as translatable by using ``__Choices``. The double underscore will split
out each choice in a separate string.

The ``po-debconf`` system also offers interesting possibilities to only
mark **some** choices as translatable. Example:

::

   Template: foo/bar
   Type: Select
   #flag:translate:3
   __Choices: PAL, SECAM, Other
   _Description: TV standard:
    Please choose the TV standard used in your country.

In that example, only the 'Other' string is translatable while others
are acronyms that should not be translated. The above allows only
'Other' to be included in PO and POT files.

The debconf templates flag system offers many such possibilities. The
po-debconf 7 manual page lists all these possibilities.

.. _s6.5.3.1.5:

multiselect
^^^^^^^^^^^

Like the select data type, except the user can choose any number of
items from the choices list (or chose none of them).

.. _s6.5.3.1.6:

note
^^^^

Rather than being a question per se, this datatype indicates a note that
can be displayed to the user. It should be used only for important notes
that the user really should see, since debconf will go to great pains to
make sure the user sees it; halting the install for them to press a key,
and even mailing the note to them in some cases.

.. _s6.5.3.1.7:

text
^^^^

This type is now considered obsolete: don't use it.

.. _s6.5.3.1.8:

error
^^^^^

This type is designed to handle error messages. It is mostly similar to
the note type. Front ends may present it differently (for instance, the
dialog front end of cdebconf draws a red screen instead of the usual
blue one).

It is recommended to use this type for any message that needs user
attention for a correction of any kind.

.. _s6.5.3.2:

Description: short and extended description
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Template descriptions have two parts: short and extended. The short
description is in the Description: line of the template.

The short description should be kept short (50 characters or so) so that
it may be accommodated by most debconf interfaces. Keeping it short also
helps translators, as usually translations tend to end up being longer
than the original.

The short description should be able to stand on its own. Some
interfaces do not show the long description by default, or only if the
user explicitly asks for it or even do not show it at all. Avoid things
like: "What do you want to do?"

The short description does not necessarily have to be a full sentence.
This is part of the keep it short and efficient recommendation.

The extended description should not repeat the short description word
for word. If you can't think up a long description, then first, think
some more. Post to debian-devel. Ask for help. Take a writing class!
That extended description is important. If after all that you still
can't come up with anything, leave it blank.

The extended description should use complete sentences. Paragraphs
should be kept short for improved readability. Do not mix two ideas in
the same paragraph but rather use another paragraph.

Don't be too verbose. User tend to ignore too long screens. 20 lines are
by experience a border you shouldn't cross, because that means that in
the classical dialog interface, people will need to scroll, and lot of
people just don't do that.

The extended description should **never** include a question.

For specific rules depending on templates type (string, boolean, etc.),
please read below.

.. _s6.5.3.3:

Choices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This field should be used for select and multiselect types. It contains
the possible choices that will be presented to users. These choices
should be separated by commas.

.. _s6.5.3.4:

Default
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This field is optional. It contains the default answer for string,
select and multiselect templates. For multiselect templates, it may
contain a comma-separated list of choices.

.. _s6.5.4:

Template fields specific style guide
--------------------------------------------------------------------------------------------------------------------------------

.. _s6.5.4.1:

Type field
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No specific indication except: use the appropriate type by referring to
the previous section.

.. _s6.5.4.2:

Description field
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Below are specific instructions for properly writing the Description
(short and extended) depending on the template type.

.. _s6.5.4.2.1:

String/password templates
^^^^^^^^^^^^^^^^^^^^^^^^^

-  The short description is a prompt and **not** a title. Avoid question
   style prompts (IP Address?) in favour of opened prompts (IP
   address:). The use of colons is recommended.

-  The extended description is a complement to the short description. In
   the extended part, explain what is being asked, rather than ask the
   same question again using longer words. Use complete sentences. Terse
   writing style is strongly discouraged.

.. _s6.5.4.2.2:

Boolean templates
^^^^^^^^^^^^^^^^^

-  The short description should be phrased in the form of a question,
   which should be kept short and should generally end with a question
   mark. Terse writing style is permitted and even encouraged if the
   question is rather long (remember that translations are often longer
   than original versions).

-  Again, please avoid referring to specific interface widgets. A common
   mistake for such templates is if you answer Yes-type constructions.

.. _s6.5.4.2.3:

Select/Multiselect
^^^^^^^^^^^^^^^^^^

-  The short description is a prompt and **not** a title. Do **not** use
   useless "Please choose..." constructions. Users are clever enough to
   figure out they have to choose something... :)

-  The extended description will complete the short description. It may
   refer to the available choices. It may also mention that the user may
   choose more than one of the available choices, if the template is a
   multiselect one (although the interface often makes this clear).

.. _s6.5.4.2.4:

Notes
^^^^^

-  The short description should be considered to be a **title**.

-  The extended description is what will be displayed as a more detailed
   explanation of the note. Phrases, no terse writing style.

-  **Do not abuse debconf.** Notes are the most common way to abuse
   debconf. As written in the debconf-devel manual page: it's best to
   use them only for warning about very serious problems. The
   ``NEWS.Debian`` or ``README.Debian`` files are the appropriate
   location for a lot of notes. If, by reading this, you consider
   converting your Note type templates to entries in ``NEWS.Debian`` or
   ``README.Debian``, please consider keeping existing translations for
   the future.

.. _s6.5.4.3:

Choices field
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If the Choices are likely to change often, please consider using the
\__Choices trick. This will split each individual choice into a single
string, which will considerably help translators for doing their work.

.. _s6.5.4.4:

Default field
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If the default value for a select template is likely to vary depending
on the user language (for instance, if the choice is a language choice),
please use the \_Default trick, documented in po-debconf 7.

This special field allows translators to put the most appropriate choice
according to their own language. It will become the default choice when
their language is used while your own mentioned Default Choice will be
used when using English.

Do not use an empty default field. If you don't want to use default
values, do not use Default at all.

If you use po-debconf (and you **should**; see :ref:`s6.5.2.2`), consider making this field translatable, if
you think it may be translated.

Example, taken from the geneweb package templates:

::

   Template: geneweb/lang
   Type: select
   __Choices: Afrikaans (af), Bulgarian (bg), Catalan (ca), Chinese (zh), Czech (cs), Danish (da), Dutch (nl), English (en), Esperanto (eo), Estonian (et), Finnish (fi), French (fr), German (de), Hebrew (he), Icelandic (is), Italian (it), Latvian (lv), Norwegian (no), Polish (pl), Portuguese (pt), Romanian (ro), Russian (ru), Spanish (es), Swedish (sv)
   # This is the default choice. Translators may put their own language here
   # instead of the default.
   # WARNING : you MUST use the ENGLISH NAME of your language
   # For instance, the French translator will need to put French (fr) here.
   _Default: English[ translators, please see comment in PO files]
   _Description: Geneweb default language:

Note the use of brackets, which allow internal comments in debconf
fields. Also note the use of comments, which will show up in files the
translators will work with.

The comments are needed as the \_Default trick is a bit confusing: the
translators may put in their own choice.

.. _bpp-i18n:

Internationalization
================================================================================================================================

This section contains global information for developers to make
translators' lives easier. More information for translators and
developers interested in internationalization are available in the
`Internationalisation and localisation in
Debian <https://people.debian.org/~jfs/debconf6/html/>`__ documentation.

.. _bpp-i18n-debconf:

Handling debconf translations
--------------------------------------------------------------------------------------------------------------------------------

Like porters, translators have a difficult task. They work on many
packages and must collaborate with many different maintainers. Moreover,
most of the time, they are not native English speakers, so you may need
to be particularly patient with them.

The goal of ``debconf`` was to make package configuration easier for
maintainers and for users. Originally, translation of debconf templates
was handled with ``debconf-mergetemplate``. However, that technique is
now deprecated; the best way to accomplish ``debconf``
internationalization is by using the ``po-debconf`` package. This method
is easier both for maintainer and translators; transition scripts are
provided.

Using ``po-debconf``, the translation is stored in ``.po`` files
(drawing from ``gettext`` translation techniques). Special template
files contain the original messages and mark which fields are
translatable. When you change the value of a translatable field, by
calling ``debconf-updatepo``, the translation is marked as needing
attention from the translators. Then, at build time, the
``dh_installdebconf`` program takes care of all the needed magic to add
the template along with the up-to-date translations into the binary
packages. Refer to the po-debconf 7 manual page for details.

.. _bpp-i18n-docs:

Internationalized documentation
--------------------------------------------------------------------------------------------------------------------------------

Internationalizing documentation is crucial for users, but a lot of
labor. There's no way to eliminate all that work, but you can make
things easier for translators.

If you maintain documentation of any size, it is easier for translators
if they have access to a source control system. That lets translators
see the differences between two versions of the documentation, so, for
instance, they can see what needs to be retranslated. It is recommended
that the translated documentation maintain a note about what source
control revision the translation is based on. An interesting system is
provided by
`doc-check <https://salsa.debian.org/installer-team/installation-guide/blob/master/scripts/doc-check>`__
in the ``debian-installer`` package, which shows an overview of the
translation status for any given language, using structured comments for
the current revision of the file to be translated and, for a translated
file, the revision of the original file the translation is based on. You
might wish to adapt and provide that in your VCS area.

If you maintain XML or SGML documentation, we suggest that you isolate
any language-independent information and define those as entities in a
separate file that is included by all the different translations. This
makes it much easier, for instance, to keep URLs up to date across
multiple files.

Some tools (e.g. ``po4a``, ``poxml``, or the ``translate-toolkit``) are
specialized in extracting the translatable material from different
formats. They produce PO files, a format quite common to translators,
which permits seeing what needs to be re-translated when the translated
document is updated.

.. _bpp-common-situations:

Common packaging situations
================================================================================================================================

.. _bpp-autotools:

Packages using ``autoconf``/``automake``
--------------------------------------------------------------------------------------------------------------------------------

Keeping ``autoconf``'s ``config.sub`` and ``config.guess`` files up to
date is critical for porters, especially on more volatile architectures.
Some very good packaging practices for any package using ``autoconf``
and/or ``automake`` have been synthesized in
``/usr/share/doc/autotools-dev/README.Debian.gz`` from the
``autotools-dev`` package. You're strongly encouraged to read this file
and to follow the given recommendations.

.. _bpp-libraries:

Libraries
--------------------------------------------------------------------------------------------------------------------------------

Libraries are always difficult to package for various reasons. The
policy imposes many constraints to ease their maintenance and to make
sure upgrades are as simple as possible when a new upstream version
comes out. Breakage in a library can result in dozens of dependent
packages breaking.

Good practices for library packaging have been grouped in `the library
packaging
guide <https://www.netfort.gr.jp/~dancer/column/libpkg-guide/>`__.

.. _bpp-docs:

Documentation
--------------------------------------------------------------------------------------------------------------------------------

Be sure to follow the `Policy on
documentation <https://www.debian.org/doc/debian-policy/ch-docs.html>`__.

If your package contains documentation built from XML or SGML, we
recommend you not ship the XML or SGML source in the binary package(s).
If users want the source of the documentation, they should retrieve the
source package.

Policy specifies that documentation should be shipped in HTML format. We
also recommend shipping documentation in PDF and plain text format if
convenient and if output of reasonable quality is possible. However, it
is generally not appropriate to ship plain text versions of
documentation whose source format is HTML.

Major shipped manuals should register themselves with ``doc-base`` on
installation. See the ``doc-base`` package documentation for more
information.

Debian policy (section 12.1) directs that manual pages should accompany
every program, utility, and function, and suggests them for other
objects like configuration files. If the work you are packaging does not
have such manual pages, consider writing them for inclusion in your
package, and submitting them upstream.

The manpages do not need to be written directly in the troff format.
Popular source formats are DocBook, POD and reST, which can be converted
using ``xsltproc``, ``pod2man`` and ``rst2man`` respectively. To a
lesser extent, the ``help2man`` program can also be used to write a
stub.

.. _bpp-other:

Specific types of packages
--------------------------------------------------------------------------------------------------------------------------------

Several specific types of packages have special sub-policies and
corresponding packaging rules and practices:

-  Perl related packages have a `Perl
   policy <https://www.debian.org/doc/packaging-manuals/perl-policy/>`__;
   some examples of packages following that policy are
   ``libdbd-pg-perl`` (binary perl module) or ``libmldbm-perl`` (arch
   independent perl module).

-  Python related packages have their Python policy; see
   ``/usr/share/doc/python/python-policy.txt.gz`` in the ``python``
   package.

-  Emacs related packages have the `emacs
   policy <https://www.debian.org/doc/packaging-manuals/debian-emacs-policy>`__.

-  Java related packages have their `java
   policy <https://www.debian.org/doc/packaging-manuals/java-policy/>`__.

-  OCaml related packages have their own policy, found in
   ``/usr/share/doc/ocaml/ocaml_packaging_policy.gz`` from the ``ocaml``
   package. A good example is the ``camlzip`` source package.

-  Packages providing XML or SGML DTDs should conform to the
   recommendations found in the ``sgml-base-doc`` package.

-  Lisp packages should register themselves with
   ``common-lisp-controller``, about which see
   ``/usr/share/doc/common-lisp-controller/README.packaging``.

-  Rust packaging is described in the `Debian Rust Team Book
   <https://rust-team.pages.debian.net/book/>`__;.

.. _bpp-archindepdata:

Architecture-independent data
--------------------------------------------------------------------------------------------------------------------------------

It is not uncommon to have a large amount of architecture-independent
data packaged with a program. For example, audio files, a collection of
icons, wallpaper patterns, or other graphic files. If the size of this
data is negligible compared to the size of the rest of the package, it's
probably best to keep it all in a single package.

However, if the size of the data is considerable, consider splitting it
out into a separate, architecture-independent package (``_all.deb``). By
doing this, you avoid needless duplication of the same data into ten or
more .debs, one per each architecture. While this adds some extra
overhead into the ``Packages`` files, it saves a lot of disk space on
Debian mirrors. Separating out architecture-independent data also
reduces processing time of ``lintian`` (see :ref:`tools-lint`) when
run over the entire Debian archive.

.. _bpp-locale:

Needing a certain locale during build
--------------------------------------------------------------------------------------------------------------------------------

If you need a certain locale during build, you can create a temporary
file via this trick:

If you set ``LOCPATH`` to the equivalent of ``/usr/lib/locale``, and
``LC_ALL`` to the name of the locale you generate, you should get what
you want without being root. Something like this:

.. code-block:: sh

   LOCALE_PATH=debian/tmpdir/usr/lib/locale
   LOCALE_NAME=en_IN
   LOCALE_CHARSET=UTF-8

   mkdir -p $LOCALE_PATH
   localedef -i $LOCALE_NAME.$LOCALE_CHARSET -f $LOCALE_CHARSET $LOCALE_PATH/$LOCALE_NAME.$LOCALE_CHARSET

   # Using the locale
   LOCPATH=$LOCALE_PATH LC_ALL=$LOCALE_NAME.$LOCALE_CHARSET date

.. _bpp-transition:

Make transition packages deborphan compliant
--------------------------------------------------------------------------------------------------------------------------------

Deborphan is a program for helping users to detect which packages can
safely be removed from the system, i.e. the ones that have no packages
depending on them. The default operation is to search only within the
libs and oldlibs sections, to hunt down unused libraries. But when
passed the right argument, it tries to catch other useless packages.

For example, with ``--guess-dummy``, ``deborphan`` tries to search all
transitional packages which were needed for upgrade but which can now
be removed. For that, it currently looks for the string dummy or transitional
in their short description, though it would be better to search for both
strings, as there are some dummy or transitional packages, which have other
purposes.

So, when you are creating such a package, please make sure to add
``transitional dummy package`` to the short description to make this explicit.
If you are looking for examples, just run: ``apt-cache search .|grep dummy``
or ``apt-cache search .|grep transitional``.

Also, it is recommended to adjust its section to ``oldlibs`` and its
priority to ``optional`` in order to ease ``deborphan``'s job.

.. _bpp-origtargz:

Best practices for ``.orig.tar.{gz,bz2,xz}`` files
--------------------------------------------------------------------------------------------------------------------------------

There are two kinds of original source tarballs: Pristine source and
repackaged upstream source.

.. _pristinesource:

Pristine source
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The defining characteristic of a pristine source tarball is that the
``.orig.tar.{gz,bz2,xz}`` file is byte-for-byte identical to a tarball
officially distributed by the upstream author. [1]_ This makes it
possible to use checksums to easily verify that all changes between
Debian's version and upstream's are contained in the Debian diff. Also,
if the original source is huge, upstream authors and others who already
have the upstream tarball can save download time if they want to inspect
your packaging in detail.

There are no universally accepted guidelines that upstream authors
follow regarding the directory structure inside their tarball, but
``dpkg-source`` is nevertheless able to deal with most upstream tarballs
as pristine source. Its strategy is equivalent to the following:

1. It unpacks the tarball in an empty temporary directory by doing

   .. code-block:: sh

      zcat path/to/packagename_upstream-version.orig.tar.gz | tar xf -

2. If, after this, the temporary directory contains nothing but one
   directory and no other files, ``dpkg-source`` renames that directory
   to *packagename*\ ``-``\ *upstream-version*\ ``(.orig)``. The name of
   the top-level directory in the tarball does not matter, and is
   forgotten.

3. Otherwise, the upstream tarball must have been packaged without a
   common top-level directory (shame on the upstream author!). In this
   case, ``dpkg-source`` renames the temporary directory *itself* to
   *packagename*\ ``-``\ *upstream-version*\ ``(.orig)``.

.. _repackagedorigtargz:

Repackaged upstream source
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You **should** upload packages with a pristine source tarball if
possible, but there are various reasons why it might not be possible.
This is the case if upstream does not distribute the source as gzipped
tar at all, or if upstream's tarball contains non-DFSG-free material
that you must remove before uploading.

In these cases the developer must construct a suitable
``.orig.tar.{gz,bz2,xz}`` file themselves. We refer to such a tarball as
a repackaged upstream source. Note that a repackaged upstream source is
different from a Debian-native package. A repackaged source still comes
with Debian-specific changes in a separate ``.diff.gz`` or
``.debian.tar.{gz,bz2,xz}`` and still has a version number composed of
*upstream-version* and *debian-version*.

There may be cases where it is desirable to repackage the source even
though upstream distributes a ``.tar.{gz,bz2,xz}`` that could in
principle be used in its pristine form. The most obvious is if
*significant* space savings can be achieved by recompressing the tar
archive or by removing genuinely useless cruft from the upstream
archive. Use your own discretion here, but be prepared to defend your
decision if you repackage source that could have been pristine.

A repackaged ``.orig.tar.{gz,bz2,xz}``

1. **should** be documented in the resulting source package. Detailed
   information on how the repackaged source was obtained, and on how
   this can be reproduced should be provided in ``debian/copyright``, 
   ideally in a way that can be done automatically with `uscan 
   <https://manpages.debian.org/uscan.1>`__. If that really doesn't work,
   at least provide a ``get-orig-source`` target in your
   ``debian/rules`` file that repeats the process, even though that 
   was actually deprecated in the `4.1.4 version of the Debian policy
   <https://www.debian.org/doc/debian-policy/upgrading-checklist.html#version-4-1-4>`__.

2. **should not** contain any file that does not come from the upstream
   author(s), or whose contents has been changed by you. [2]_

3. **should**, except where impossible for legal reasons, preserve the
   entire building and portability infrastructure provided by the
   upstream author. For example, it is not a sufficient reason for
   omitting a file that it is used only when building on MS-DOS.
   Similarly, a ``Makefile`` provided by upstream should not be omitted
   even if the first thing your ``debian/rules`` does is to overwrite it
   by running a configure script.

   (*Rationale:* It is common for Debian users who need to build
   software for non-Debian platforms to fetch the source from a Debian
   mirror rather than trying to locate a canonical upstream distribution
   point).

4. **may** use *packagename*\ ``-``\ *upstream-version*\ ``+dfsg``
   (or any other suffix which is added to the tarball name) as
   the name of the top-level directory in its tarball. This makes it
   possible to distinguish pristine tarballs from repackaged ones.

5. **should** be compressed with ``xz`` (or ``gzip`` or ``bzip``) with maximal compression.

.. _changed-binfiles:

Changing binary files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sometimes it is necessary to change binary files contained in the
original tarball, or to add binary files that are not in it. This is
fully supported when using source packages in “3.0 (quilt)” format; see
the dpkg-source1 manual page for details. When using the older format
“1.0”, binary files can't be stored in the ``.diff.gz`` so you must
store a ``uuencode``\ d (or similar) version of the file(s) and decode
it at build time in ``debian/rules`` (and move it in its official
location).

.. _bpp-dbg:

Best practices for debug packages
--------------------------------------------------------------------------------------------------------------------------------

A debug package is a package that contains additional information that
can be used by ``gdb``. Since Debian binaries are stripped by default,
debugging information, including function names and line numbers, is
otherwise not available when running ``gdb`` on Debian binaries. Debug
packages allow users who need this additional debugging information to
install it without bloating a regular system with the information.

The debug packages contain separated debugging symbols that ``gdb`` can
find and load on the fly when debugging a program or library. The
convention in Debian is to keep these symbols in
``/usr/lib/debug/``\ *path*, where *path* is the path to the executable
or library. For example, debugging symbols for ``/usr/bin/foo`` go in
``/usr/lib/debug/usr/bin/foo``, and debugging symbols for
``/usr/lib/libfoo.so.1`` go in ``/usr/lib/debug/usr/lib/libfoo.so.1``.

.. _bpp-dbgsym:

Automatically generated debug packages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Debug symbol packages can be generated automatically for any binary
package that contains executable binaries, and except for corner cases,
it should not be necessary to use the old manually generated ones
anymore. The package name for a automatic generated debug symbol package
ends in ``-dbgsym``.

The ``dbgsym`` packages are not installed into the regular archives, but
in dedicated archives. That means, if you need the debug symbols for
debugging, you need to add this archives to your apt configuration and
then install the ``dbgsym`` package you are interested in. Please read
https://wiki.debian.org/HowToGetABacktrace\  on how to do that.

.. _bpp-dbg-legacy:

Manual -dbg packages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Before the advent of the automatic ``dbgsym`` packages, debug packages
needed to be manually generated. The name of a manual debug packages
ends in ``-dbg``. It is recommended to migrate such old legacy packages
to the new ``dbgsym`` packages whenever possible. The procedure to
convert your package is described in
https://wiki.debian.org/AutomaticDebugPackages\  but the gist is to
use the ``--dbgsym-migration='pkgname-dbg (<< currentversion~)'`` switch
of the ``dh_strip`` command.

However, sometimes it is not possible to convert to the new ``dbgsym``
packages, or you will encounter the old manual -dbg packages in the
archives, so you might need to deal with them. It is not recommended to
create manual -dbg packages for new packages, except if the automatic
ones won't work for some reason.

One reason could be that debug packages contains an entire special
debugging build of a library or other binary. However, usually
separating debugging information from the already built binaries is
sufficient and will also save space and build time.

This is the case, for example, for debugging symbols of Python
extensions. For now the right way to package Python extension debug
symbols is to use ``-dbg`` packages as described in
https://wiki.debian.org/Python/DbgBuilds\ .

To create ``-dbg`` packages, the package maintainer has to explicitly
specify them in ``debian/control``.

The debugging symbols can be extracted from an object file using
``objcopy --only-keep-debug``. Then the object file can be stripped, and
``objcopy --add-gnu-debuglink`` used to specify the path to the
debugging symbol file. objcopy 1 explains in detail how this works.

Note that the debug package should depend on the package that it
provides debugging symbols for, and this dependency should be versioned.
For example:

.. code-block:: debcontrol

   Depends: libfoo (= ${binary:Version})

The ``dh_strip`` command in ``debhelper`` supports creating debug
packages, and can take care of using ``objcopy`` to separate out the
debugging symbols for you. If your package uses ``debhelper/9.20151219``
or newer, you don't need to do anything. ``debhelper`` will generate
debug symbol packages (as ``package``-dbgsym) for you with no additional
changes to your source package.

.. _bpp-meta:

Best practices for meta-packages
--------------------------------------------------------------------------------------------------------------------------------

A meta-package is a mostly empty package that makes it easy to install a
coherent set of packages that can evolve over time. It achieves this by
depending on all the packages of the set. Thanks to the power of APT,
the meta-package maintainer can adjust the dependencies and the user's
system will automatically get the supplementary packages. The dropped
packages that were automatically installed will be also be marked as
removal candidates (and are even automatically removed by ``aptitude``).
``gnome`` and ``linux-image-amd64`` are two examples of meta-packages
(built by the source packages ``meta-gnome2`` and ``linux-latest``).

The long description of the meta-package must clearly document its
purpose so that the user knows what they will lose if they remove the
package. Being explicit about the consequences is recommended. This is
particularly important for meta-packages that are installed during
initial installation and that have not been explicitly installed by the
user. Those tend to be important to ensure smooth system upgrades and
the user should be discouraged from uninstalling them to avoid potential
breakages.

.. [1]
   We cannot prevent upstream authors from changing the tarball they
   distribute without also incrementing the version number, so there can
   be no guarantee that a pristine tarball is identical to what upstream
   *currently* distributing at any point in time. All that can be
   expected is that it is identical to something that upstream once
   *did* distribute. If a difference arises later (say, if upstream
   notices that they weren't using maximal compression in their original
   distribution and then re-\ \ ``gzip`` it), that's just too bad. Since
   there is no good way to upload a new ``.orig.tar.{gz,bz2,xz}`` for
   the same version, there is not even any point in treating this
   situation as a bug.

.. [2]
   As a special exception, if the omission of non-free files would lead
   to the source failing to build without assistance from the Debian
   diff, it might be appropriate to instead edit the files, omitting
   only the non-free parts of them, and/or explain the situation in a
   ``README.source`` file in the root of the source tree. But in that
   case please also urge the upstream author to make the non-free
   components easier to separate from the rest of the source.
