.. _new-maintainer:

Applying to Become a Member
********************************************************************************************************************************

Getting started
================================================================================================================================

So, you've read all the documentation, you've gone through the `Debian
New Maintainers' Guide <https://www.debian.org/doc/maint-guide/>`__ (or
its successor, `Guide for Debian
Maintainers <https://www.debian.org/doc/manuals/debmake-doc/>`__),
understand what everything in the ``hello`` example package is for, and
you're about to Debianize your favorite piece of software. How do you
actually become a Debian developer so that your work can be incorporated
into the Project?

Firstly, subscribe to ``debian-devel@lists.debian.org`` if you haven't
already. Send the word ``subscribe`` in the ``Subject`` of an email to
``debian-devel-REQUEST@lists.debian.org``. In case of problems, contact
the list administrator at ``listmaster@lists.debian.org``. More
information on available mailing lists can be found in
:ref:`mailing-lists`. ``debian-devel-announce@lists.debian.org`` is
another list, which is mandatory for anyone who wishes to follow
Debian's development.

You should subscribe and lurk (that is, read without posting) for a bit
before doing any coding, and you should post about your intentions to
work on something to avoid duplicated effort.

Another good list to subscribe to is
``debian-mentors@lists.debian.org``. See :ref:`mentors` for details. The IRC channel ``#debian`` can also
be helpful; see :ref:`irc-channels`.

When you know how you want to contribute to Debian, you should get in
contact with existing Debian maintainers who are working on similar
tasks. That way, you can learn from experienced developers. For example,
if you are interested in packaging existing software for Debian, you
should try to get a sponsor. A sponsor will work together with you on
your package and upload it to the Debian archive once they are happy
with the packaging work you have done. You can find a sponsor by mailing
the ``debian-mentors@lists.debian.org`` mailing list, describing your
package and yourself and asking for a sponsor (see :ref:`sponsoring`
and https://wiki.debian.org/DebianMentorsFaq\  for more information
on sponsoring). On the other hand, if you are interested in porting
Debian to alternative architectures or kernels you can subscribe to port
specific mailing lists and ask there how to get started. Finally, if you
are interested in documentation or Quality Assurance (QA) work you can
join maintainers already working on these tasks and submit patches and
improvements.

One pitfall could be a too-generic local part in your email address: Terms
like mail, admin, root, master should be avoided, please see
https://www.debian.org/MailingLists/\  for details.

.. _mentors:

Debian mentors and sponsors
================================================================================================================================

The mailing list ``debian-mentors@lists.debian.org`` has been set up for
novice maintainers who seek help with initial packaging and other
developer-related issues. Every new developer is invited to subscribe to
that list (see :ref:`mailing-lists` for details).

Those who prefer one-on-one help (e.g., via private email) should also
post to that list and an experienced developer will volunteer to help.

In addition, if you have some packages ready for inclusion in Debian,
but are waiting for your new member application to go through, you might
be able find a sponsor to upload your package for you. Sponsors are
people who are official Debian Developers, and who are willing to
criticize and upload your packages for you. Please read the
debian-mentors FAQ at https://wiki.debian.org/DebianMentorsFaq\ 
first.

If you wish to be a mentor and/or sponsor, more information is available
in :ref:`newmaint`.

.. _registering:

Registering as a Debian member
================================================================================================================================

Before you decide to register with Debian, you will need to read all the
information available at the `New Members
Corner <https://www.debian.org/devel/join/newmaint>`__. It describes in
detail the preparations you have to do before you can register to become
a Debian member. For example, before you apply, you have to read the
`Debian Social Contract <https://www.debian.org/social_contract>`__.
Registering as a member means that you agree with and pledge to uphold
the Debian Social Contract; it is very important that member are in
accord with the essential ideas behind Debian. Reading the `GNU
Manifesto <https://www.gnu.org/gnu/manifesto.html>`__ would also be a
good idea.

The process of registering as a member is a process of verifying your
identity and intentions, and checking your technical skills. As the
number of people working on Debian has grown to over
|number-of-maintainers| and our systems are used in several very
important places, we have to be careful about being compromised.
Therefore, we need to verify new members before we can give them
accounts on our servers and let them upload packages.

Before you actually register you should have shown that you can do
competent work and will be a good contributor. You show this by
submitting patches through the Bug Tracking System and having a package
sponsored by an existing Debian Developer for a while. Also, we expect
that contributors are interested in the whole project and not just in
maintaining their own packages. If you can help other maintainers by
providing further information on a bug or even a patch, then do so!

Registration requires that you are familiar with Debian's philosophy and
technical documentation. Furthermore, you need a OpenPGP key which has
been signed by an existing Debian maintainer. If your OpenPGP key is not
signed yet, you should try to meet a Debian Developer in person to get
your key signed. There's a `Key Signing Coordination
page <https://wiki.debian.org/Keysigning>`__ which should help you find
a Debian Developer close to you. (If there is no Debian Developer close
to you, alternative ways to pass the ID check may be permitted as an
absolute exception on a case-by-case-basis. See the `identification
page <https://www.debian.org/devel/join/nm-step2>`__ for more
information.)

If you do not have an OpenPGP key yet, generate one. Every developer
needs an OpenPGP key in order to sign and verify package uploads. You
should read the manual for the software you are using, since it has much
important information that is critical to its security. Many more
security failures are due to human error than to software failure or
high-powered spy techniques. See :ref:`key-maint` for more
information on maintaining your public key.

Debian uses the ``GNU Privacy Guard`` (package ``gnupg`` version 2 or
better) as its baseline standard. You can use some other implementation
of OpenPGP as well. Note that OpenPGP is an open standard based on `RFC
9580 <https://www.rfc-editor.org/rfc/rfc9580.html>`__.

`Your key length must be greater than 2048 bits (4096
bits is preferred) <https://keyring.debian.org/creating-key.html>`__;
there is no reason to use a smaller key, and doing so would be much
less secure.

If your public key isn't on a public key server such as
``subkeys.pgp.net``, please read the documentation available at `NM Step
2: Identification <https://www.debian.org/devel/join/nm-step2>`__. That
document contains instructions on how to put your key on the public key
servers. The New Maintainer Group will put your public key on the
servers if it isn't already there.

Some countries restrict the use of cryptographic software by their
citizens. This need not impede one's activities as a Debian package
maintainer however, as it may be perfectly legal to use cryptographic
products for authentication, rather than encryption purposes. If you
live in a country where use of cryptography even for authentication is
forbidden then please contact us so we can make special arrangements.

To apply as a new member, you need an existing Debian Developer to
support your application (an ``advocate``). After you have contributed
to Debian for a while, and you want to apply to become a registered
developer, an existing developer with whom you have worked over the past
months has to express their belief that you can contribute to Debian
successfully.

When you have found an advocate, have your OpenPGP key signed and have
already contributed to Debian for a while, you're ready to apply. You
can simply register on our `application
page <https://nm.debian.org/newnm.php>`__. After you have signed up,
your advocate has to confirm your application. When your advocate has
completed this step you will be assigned an Application Manager who will
go with you through the necessary steps of the New Member process. You
can always check your status on the `applications status
board <https://nm.debian.org/>`__.

For more details, please consult `New Members
Corner <https://www.debian.org/devel/join/newmaint>`__ at the Debian web
site. Make sure that you are familiar with the necessary steps of the
New Member process before actually applying. If you are well prepared,
you can save a lot of time later on.
