Since creation of epub does not work, when the html is based on the
read-the-doc html theme (which is the base for our Debian specific
html theme), we need to build epub based on 'nature' theme.

For this, epub.conf is used as config directory with its own
_templates dir and a layout.html, while the conf.py is shared between 
both (via a symlink).

For epub, sphinx-build is called with
'-c epub.conf -D html_theme=nature'
in the Makefile.
