
msgid ""
msgstr ""
"Project-Id-Version: developers-reference \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-06 08:15+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.3\n"

#: ../developer-duties.rst:4
msgid "Debian Developer's Duties"
msgstr "Doveri di un Debian Developer"

#: ../developer-duties.rst:9
msgid "Package Maintainer's Duties"
msgstr "Doveri di un maintainer di pacchetti"

#: ../developer-duties.rst:11
msgid ""
"As a package maintainer, you're supposed to provide high-quality packages"
" that are well integrated into the system and that adhere to the Debian "
"Policy."
msgstr ""

#: ../developer-duties.rst:18
msgid "Work towards the next ``stable`` release"
msgstr "Lavorare per il prossimo rilascio ``stable``"

#: ../developer-duties.rst:20
msgid ""
"Providing high-quality packages in ``unstable`` is not enough; most users"
" will only benefit from your packages when they are released as part of "
"the next ``stable`` release. You are thus expected to collaborate with "
"the release team to ensure your packages get included."
msgstr ""

#: ../developer-duties.rst:25
msgid ""
"More concretely, you should monitor whether your packages are migrating "
"to ``testing`` (see :ref:`testing`). When the migration doesn't happen "
"after the test period, you should analyze why and work towards fixing "
"this. It might mean fixing your package (in the case of release-critical "
"bugs or failures to build on some architecture) but it can also mean "
"updating (or fixing, or removing from ``testing``) other packages to help"
" complete a transition in which your package is entangled due to its "
"dependencies. The release team might provide you some input on the "
"current blockers of a given transition if you are not able to identify "
"them."
msgstr ""
"Più concretamente, si dovrà controllare che i pacchetti stiano migrando "
"verso ``testing`` (si consulti :ref:`testing`). Quando la migrazione non "
"avviene dopo il periodo di prova, si deve analizzare il motivo e lavorare"
" per correggerlo. Potrebbe significare correggere il pacchetto (nel caso "
"dei bug critici per il rilascio o di fallimenti nella compilazione su "
"alcune architetture), ma potrebbe anche significare l'aggiornamento (o di"
" correzione, o di rimozione da ``testing``) di altri pacchetti per "
"aiutare il completamento di una transizione in cui il proprio pacchetto è"
" incastrato a causa delle sue dipendenze. Il team di rilascio potrebbe "
"fornire alcuni input sugli attuali blocchi di una data transizione, se "
"non si è in grado di identificarli."

#: ../developer-duties.rst:39
msgid "Maintain packages in ``stable``"
msgstr "Mantenere i pacchetti in ``stable``"

#: ../developer-duties.rst:41
msgid ""
"Most of the package maintainer's work goes into providing updated "
"versions of packages in ``unstable``, but their job also entails taking "
"care of the packages in the current ``stable`` release."
msgstr ""
"La maggior parte del lavoro del maintainer del pacchetto è fornire "
"versioni aggiornate dei pacchetti in ``unstable``, ma il loro lavoro "
"comporta anche prendersi cura dei pacchetti nell'attuale rilascio "
"``stable``."

#: ../developer-duties.rst:45
msgid ""
"While changes in ``stable`` are discouraged, they are possible. Whenever "
"a security problem is reported, you should collaborate with the security "
"team to provide a fixed version (see :ref:`bug-security`). When bugs of "
"severity important (or more) are reported against the ``stable`` version "
"of your packages, you should consider providing a targeted fix. You can "
"ask the ``stable`` release team whether they would accept such an update "
"and then prepare a ``stable`` upload (see :ref:`upload-stable`)."
msgstr ""
"Mentre i cambiamenti in ``stable`` sono scoraggiati, sono comunque "
"possibili. Ogni volta che un problema di sicurezza è segnalato, si "
"dovrebbe collaborare con il team di sicurezza per fornire una versione "
"corretta (si veda :ref:`bug-security`). Quando i bug di gravità "
"importante (o più) vengono segnalati per la versione ``stable`` dei "
"propri pacchetti, si dovrebbe valutare la possibilità di fornire una "
"correzione mirata. Si potrà chiedere al team di rilascio ``stable`` se "
"accettano tale tipo di aggiornamento e poi preparare un caricamento "
"``stable`` (si consulti :ref:`upload-stable`)."

#: ../developer-duties.rst:57
msgid "Manage release-critical bugs"
msgstr "Gestire i bug critici per il rilascio"

#: ../developer-duties.rst:59
msgid ""
"Generally you should deal with bug reports on your packages as described "
"in :ref:`bug-handling`. However, there's a special category of bugs that "
"you need to take care of — the so-called release-critical bugs (RC bugs)."
" All bug reports that have severity ``critical``, ``grave`` or "
"``serious`` make the package unsuitable for inclusion in the next "
"``stable`` release. They can thus delay the Debian release (when they "
"affect a package in ``testing``) or block migrations to ``testing`` (when"
" they only affect the package in ``unstable``). In the worst scenario, "
"they will lead to the package's removal. That's why these bugs need to be"
" corrected as quickly as possible."
msgstr ""
"In generale si dovrebbe affrontare le segnalazioni di bug sui propri "
"pacchetti come è descritto in :ref:`bug-handling`. Tuttavia, c'è una "
"categoria speciale di bug di cui vi dovrete prendere cura - i cosiddetti "
"bug critici per il rilascio (RC bug). Tutte le segnalazioni di bug che "
"hanno gravità ``critical``, ``grave`` o ``serious`` rendono il pacchetto "
"non adatto per l'inclusione nel prossimo rilascio ``stable``. Quindi "
"possono ritardare il rilascio di Debian (quando riguardano un pacchetto "
"in ``testing``) o bloccare migrazioni in ``testing`` (quando influiscono "
"solo sul pacchetto in ``unstable``). Nello scenario peggiore, "
"procederanno alla rimozione del pacchetto. Ecco perché questi bug devono "
"essere corretti al più presto."

#: ../developer-duties.rst:70
msgid ""
"If, for any reason, you aren't able fix an RC bug in a package of yours "
"within 2 weeks (for example due to time constraints, or because it's "
"difficult to fix), you should mention it clearly in the bug report and "
"you should tag the bug ``help`` to invite other volunteers to chime in. "
"Be aware that RC bugs are frequently the targets of Non-Maintainer "
"Uploads (see :ref:`nmu`) because they can block the ``testing`` migration"
" of many packages."
msgstr ""
"Se, per qualsiasi motivo, non potete risolvere un bug RC in uno dei "
"vostri pacchetti entro 2 settimane (per esempio a causa di vincoli di "
"tempo, o perché è difficile da correggere), si dovrebbe accennarlo "
"chiaramente nel bug report e si dovrebbe contrassegnare il bug come "
"``help`` in modo da invitare altri volontari a partecipare alla sua "
"risoluzione. Si sia consapevoli che i bug RC sono spesso bersaglio di "
"Non-Maintainer Uploads (si consulti :ref:`nmu`) perché in grado di "
"bloccare la migrazione in ``testing`` di molti pacchetti."

#: ../developer-duties.rst:78
msgid ""
"Lack of attention to RC bugs is often interpreted by the QA team as a "
"sign that the maintainer has disappeared without properly orphaning their"
" package. The MIA team might also get involved, which could result in "
"your packages being orphaned (see :ref:`mia-qa`)."
msgstr ""
"La mancanza di attenzione per i bug RC è spesso interpretata dal team QA "
"come un segno che il maintainer è scomparso senza aver correttamente reso"
" orfano il suo pacchetto. Il team MIA potrebbe anche mettersi in gioco, "
"che potrebbe concretizzarsi nel rendere orfani i vostri pacchetti (si "
"consulti :ref:`mia-qa`)."

#: ../developer-duties.rst:86
msgid "Coordination with upstream developers"
msgstr "Coordinamento con gli sviluppatori originali"

#: ../developer-duties.rst:88
msgid ""
"A big part of your job as Debian maintainer will be to stay in contact "
"with the upstream developers. Debian users will sometimes report bugs "
"that are not specific to Debian to our bug tracking system. These bug "
"reports should be forwarded to the upstream developers so that they can "
"be fixed in a future upstream release. Usually it is best if you can do "
"this, but alternatively, you may ask the bug submitter to do it."
msgstr ""

#: ../developer-duties.rst:95
msgid ""
"While it's not your job to fix non-Debian specific bugs, you may freely "
"do so if you're able. When you make such fixes, be sure to pass them on "
"to the upstream maintainers as well. Debian users and developers will "
"sometimes submit patches to fix upstream bugs — you should evaluate and "
"forward these patches upstream."
msgstr ""
"Anche se non è il proprio lavoro correggere i bug specifici non-Debian, "
"si può liberamente farlo se ne si ha la possibilità. Quando si effettuano"
" queste correzioni, ci si assicuri di trasmetterle anche ai maintainer "
"originali. Utenti e sviluppatori Debian a volte invieranno patch che "
"correggono bug dei sorgenti originali: si dovrebbe valutare e trasmettere"
" queste patch allo sviluppatore originale."

#: ../developer-duties.rst:101
msgid ""
"In cases where a bug report is forwarded upstream, it may be helpful to "
"remember that the bts-link service can help with synchronizing states "
"between the upstream bug tracker and the Debian one."
msgstr ""

#: ../developer-duties.rst:105
msgid ""
"If you need to modify the upstream sources in order to build a policy "
"compliant package, then you should propose a nice fix to the upstream "
"developers which can be included there, so that you won't have to modify "
"the sources of the next upstream version. Whatever changes you need, "
"always try not to fork from the upstream sources."
msgstr ""
"Se si necessita di modificare i sorgenti originali al fine di costruire "
"un pacchetto conforme alla policy, allora si dovrebbe proporre una "
"soluzione accurata agli sviluppatori originali che può essere li "
"applicata, in modo da non dover modificare i sorgenti della prossima "
"versione originale. Qualunque cambiamento necessiti, cerca sempre di non "
"effettuare il fork dai sorgenti originali."

#: ../developer-duties.rst:111
msgid ""
"If you find that the upstream developers are or become hostile towards "
"Debian or the free software community, you may want to re-consider the "
"need to include the software in Debian. Sometimes the social cost to the "
"Debian community is not worth the benefits the software may bring."
msgstr ""
"Se si scopre che gli sviluppatori originali sono o diventano ostili verso"
" Debian o la comunità del software libero, si potrebbe riconsiderare la "
"necessità di includere il software in Debian. A volte il costo sociale "
"per la comunità Debian non vale i benefici che il software può portare."

#: ../developer-duties.rst:117
msgid "Administrative Duties"
msgstr "Doveri amministrativi"

#: ../developer-duties.rst:119
msgid ""
"A project of the size of Debian relies on some administrative "
"infrastructure to keep track of everything. As a project member, you have"
" some duties to ensure everything keeps running smoothly."
msgstr ""
"Un progetto delle dimensioni di Debian si basa su alcune infrastrutture "
"amministrative per tenere traccia di tutto. Come membro del progetto, si "
"hanno alcuni doveri che assicurano che il tutto continui a funzionare "
"senza problemi."

#: ../developer-duties.rst:126
msgid "Maintaining your Debian information"
msgstr "Gestire le vostre informazioni Debian"

#: ../developer-duties.rst:128
msgid ""
"There's a LDAP database containing information about Debian developers at"
" https://db.debian.org/\\ . You should enter your information there and "
"update it as it changes. Most notably, make sure that the address where "
"your debian.org email gets forwarded to is always up to date, as well as "
"the address where you get your debian-private subscription if you choose "
"to subscribe there."
msgstr ""
"C'è un database LDAP contenente le informazioni relative agli "
"sviluppatori Debian su https://db.debian.org/\\ . Si dovrebbe immettere "
"le informazioni li ed aggiornarle appena cambiano. Più in particolare, "
"fare in modo che l'indirizzo al quale la propria email debian.org viene "
"inoltrata sia sempre aggiornato, così come l'indirizzo in cui si hanno le"
" proprie iscrizioni debian private se si sceglie di sottoscriverle."

#: ../developer-duties.rst:135
msgid "For more information about the database, please see :ref:`devel-db`."
msgstr "Per ulteriori informazioni sul database, si consulti :ref:`devel-db`."

#: ../developer-duties.rst:140
msgid "Maintaining your public key"
msgstr "Mantenere la vostra chiave pubblica"

#: ../developer-duties.rst:142
msgid ""
"Be very careful with your private keys. Do not place them on any public "
"servers or multiuser machines, such as the Debian servers (see :ref"
":`server-machines`). Back your keys up; keep a copy offline. Read the "
"documentation that comes with your software; read the `PGP FAQ "
"<http://www.cam.ac.uk.pgp.net/pgpnet/pgp-faq/>`__ and `OpenPGP Best "
"Practices <https://riseup.net/en/security/message-security/openpgp/best-"
"practices>`__."
msgstr ""

#: ../developer-duties.rst:149
msgid ""
"You need to ensure not only that your key is secure against being stolen,"
" but also that it is secure against being lost. Generate and make a copy "
"(best also in paper form) of your revocation certificate; this is needed "
"if your key is lost."
msgstr ""
"È necessario garantire non solo che la vostra chiave è sicura contro il "
"furto, ma anche che è protetta in caso di smarrimento. Generare e fare "
"una copia (meglio anche se in forma cartacea) del certificato di revoca; "
"questo è necessario se la chiave viene persa."

#: ../developer-duties.rst:154
msgid ""
"If you add signatures to your public key, or add user identities, you can"
" update the Debian key ring by sending your key to the key server at "
"``keyring.debian.org``. Updates are processed at least once a month by "
"the ``debian-keyring`` package maintainers."
msgstr ""

#: ../developer-duties.rst:159
#, fuzzy
msgid ""
"If you need to add a completely new key or remove an old key, you need to"
" get the new key signed by another developer. If the old key is "
"compromised or invalid, you also have to add the revocation certificate. "
"If there is no real reason for a new key, the Keyring Maintainers might "
"reject the new key. Details can be found at "
"https://keyring.debian.org/replacing_keys.html\\ ."
msgstr ""
"Se è necessario aggiungere una nuova chiave o rimuovere una vecchia, è "
"necessario che la nuova chiave sia firmata da un altro sviluppatore. Se "
"la vecchia chiave è compromessa o non valida, si deve anche aggiungere il"
" certificato di revoca. Se non vi è alcun motivo reale per una nuova "
"chiave, i Keyring Maintainer potrebbero rifiutare la nuova chiave. "
"Dettagli possono essere trovati presso "
"http://keyring.debian.org/replacing_keys.html\\ ."

#: ../developer-duties.rst:166
msgid "The same key extraction routines discussed in :ref:`registering` apply."
msgstr ""
"Si applichino le stesse routine di estrazione di chiavi discusse nel "
":ref:`registering`."

#: ../developer-duties.rst:169
msgid ""
"You can find a more in-depth discussion of Debian key maintenance in the "
"documentation of the ``debian-keyring`` package and the "
"https://keyring.debian.org/ site."
msgstr ""

#: ../developer-duties.rst:174
msgid "Voting"
msgstr "Votare"

#: ../developer-duties.rst:176
msgid ""
"Even though Debian isn't really a democracy, we use a democratic process "
"to elect our leaders and to approve general resolutions. These procedures"
" are defined by the `Debian Constitution "
"<https://www.debian.org/devel/constitution>`__."
msgstr ""
"Anche se Debian non è davvero una democrazia, usiamo un processo "
"democratico per eleggere i nostri leader e ad approvare risoluzioni "
"generali. Queste procedure sono definite dalla `Costituzione Debian "
"<https://www.debian.org/devel/constitution>`__."

#: ../developer-duties.rst:181
msgid ""
"Other than the yearly leader election, votes are not routinely held, and "
"they are not undertaken lightly. Each proposal is first discussed on the "
"``debian-vote@lists.debian.org`` mailing list and it requires several "
"endorsements before the project secretary starts the voting procedure."
msgstr ""
"Oltre all'annuale elezione del leader, le votazioni non sono tenute "
"regolarmente e non sono intraprese con leggerezza. Ogni proposta è prima "
"discussa sulla mailing list ``debian-vote@lists.debian.org`` e richiede "
"diverse approvazioni prima che il segretario del progetto inizii la "
"procedura di voto."

#: ../developer-duties.rst:186
#, fuzzy
msgid ""
"You don't have to track the pre-vote discussions, as the secretary will "
"issue several calls for votes on ``debian-devel-"
"announce@lists.debian.org`` (and all developers are expected to be "
"subscribed to that list). Democracy doesn't work well if people don't "
"take part in the vote, which is why we encourage all developers to vote. "
"Voting is conducted via OpenPGP-signed/encrypted email messages."
msgstr ""
"Non dovete monitorare le discussioni pre-voto, considerato che il "
"segretario effettuerà diverse chiamate di votazione su ``debian-devel-"
"announce@lists.debian.org`` (e tutti gli sviluppatori dovrebbero essere "
"iscritti a questa lista). La democrazia non funziona bene se le persone "
"non prendono parte al voto, è per questo che invitiamo tutti gli "
"sviluppatori a votare. Le votazioni sono condotte attraverso messaggi "
"email GPG-signed/encrypted."

#: ../developer-duties.rst:194
msgid ""
"The list of all proposals (past and current) is available on the `Debian "
"Voting Information <https://www.debian.org/vote/>`__ page, along with "
"information on how to make, second and vote on proposals."
msgstr ""
"L'elenco di tutte le proposte (passate e presenti) è disponibile sul "
"`Debian Voting Information <https://www.debian.org/vote/>`__ pagina, "
"insieme a informazioni su come fare, supportare e votare proposte."

#: ../developer-duties.rst:201
msgid "Going on vacation gracefully"
msgstr "Andare in vacanza con garbo"

#: ../developer-duties.rst:203
msgid ""
"It is common for developers to have periods of absence, whether those are"
" planned vacations or simply being buried in other work. The important "
"thing to notice is that other developers need to know that you're on "
"vacation so that they can do whatever is needed if a problem occurs with "
"your packages or other duties in the project."
msgstr ""
"È comune per gli sviluppatori avere periodi di assenza, se queste sono le"
" vacanze programmate o semplicemente se sono sepolti in altri lavori. La "
"cosa importante da notare è che gli altri sviluppatori hanno bisogno di "
"sapere che si è in vacanza in modo che possano fare tutto ciò che è "
"necessario in caso di problemi con i propri pacchetti o altri obblighi "
"nel progetto."

#: ../developer-duties.rst:209
msgid ""
"Usually this means that other developers are allowed to NMU (see "
":ref:`nmu`) your package if a big problem (release critical bug, security"
" update, etc.) occurs while you're on vacation. Sometimes it's nothing as"
" critical as that, but it's still appropriate to let others know that "
"you're unavailable."
msgstr ""
"Di solito questo significa che altri sviluppatori sono consentiti NMU (si"
" consulti :ref:`nmu`) per il proprio pacchetto se un grosso problema (bug"
" critico per la release, aggiornamento della sicurezza, etc.), si "
"verifica mentre si è in vacanza. A volte non è niente di così critico "
"come quelli, ma è ancora il caso di far sapere agli altri che non si è "
"disponibili."

#: ../developer-duties.rst:215
msgid ""
"In order to inform the other developers, there are two things that you "
"should do. First send a mail to ``debian-private@lists.debian.org`` with "
"[VAC] prepended to the subject of your message [1]_ and state the period "
"of time when you will be on vacation. You can also give some special "
"instructions on what to do if a problem occurs."
msgstr ""
"Al fine di informare gli altri sviluppatori, ci sono due cose che si "
"dovrebbero fare. In primo luogo inviare una email a ``@debian-private e "
"liste-host;`` con [VAC] anteposto all'argomento del messaggio  [1]_ e "
"indicare il periodo di tempo in cui si sarà in vacanza. Si possono anche "
"dare alcune speciali istruzioni su cosa fare in caso di problemi."

#: ../developer-duties.rst:221
msgid ""
"The other thing to do is to mark yourself as on vacation in the :ref"
":`devel-db` (this information is only accessible to Debian developers). "
"Don't forget to remove the on vacation flag when you come back!"
msgstr ""
"L'altra cosa da fare è quella di segnare se stessi come in vacanza nel "
":ref:`devel-db` (questa informazione è accessibile solo agli sviluppatori"
" Debian). Non dimenticare di togliere il flag vacanza quando si torna!"

#: ../developer-duties.rst:225
#, fuzzy
msgid ""
"Ideally, you should sign up at the `OpenPGP coordination pages "
"<https://wiki.debian.org/Keysigning>`__ when booking a holiday and check "
"if anyone there is looking for signing. This is especially important when"
" people go to exotic places where we don't have any developers yet but "
"where there are people who are interested in applying."
msgstr ""
"Idealmente, si dovrebbe firmare la `GPG coordination pages "
"<https://wiki.debian.org/Keysigning>`__ al momento della prenotazione di "
"una vacanza e verificare se qualcuno ci sta cercando per la firma. Questo"
" è particolarmente importante quando la gente va in luoghi esotici dove "
"non abbiamo ancora degli sviluppatori, ma dove ci sono persone che sono "
"interessati a presentare domanda."

#: ../developer-duties.rst:235
msgid "Retiring"
msgstr "Congedarsi"

#: ../developer-duties.rst:237
msgid ""
"If you choose to leave the Debian project, you should make sure you do "
"the following steps:"
msgstr ""
"Se si sceglie di lasciare il progetto Debian, è necessario assicurarsi di"
" eseguire le seguenti operazioni:"

#: ../developer-duties.rst:240
#, fuzzy
msgid "Orphan all your packages, as described in :ref:`orphaning`."
msgstr "rendete orfani tutti i pacchetti, come descritto in :ref:`orphaning`."

#: ../developer-duties.rst:242
#, fuzzy
msgid "Remove yourself from uploaders for co- or team-maintained packages."
msgstr "rendete orfani tutti i pacchetti, come descritto in :ref:`orphaning`."

#: ../developer-duties.rst:244
msgid ""
"If you received mails via a @debian.org e-mail alias (e.g. "
"press@debian.org) and would like to get removed, open a RT ticket for the"
" Debian System Administrators. Just send an e-mail to "
"``admin@rt.debian.org`` with \"Debian RT\" somewhere in the subject "
"stating from which aliases you'd like to get removed."
msgstr ""
"Se si ricevono mail da un alias e-mail di @debian.org (e.g: "
"press@debian.org) e si desidera essere rimosso, aprire una segnalazione "
"RT per gli Amministratori dei Sistemi Debian. Si invii una e-mail a "
"``admin@rt.debian.org`` con la dicitura \"Debian RT\" da qualche parte "
"nel soggetto indicando da quali alias si desidera esseere rimossi."

#: ../developer-duties.rst:250
msgid ""
"Please remember to also retire from teams, e.g. remove yourself from team"
" wiki pages or salsa groups."
msgstr ""

#: ../developer-duties.rst:253
msgid ""
"Use the link https://nm.debian.org/process/emeritus to log in to "
"nm.debian.org, request emeritus status and write a goodbye message that "
"will be automatically posted on debian-private."
msgstr ""

#: ../developer-duties.rst:257
msgid ""
"Authentication to the NM site requires an SSO browser certificate. You "
"can generate them on https://sso.debian.org."
msgstr ""

#: ../developer-duties.rst:260
msgid ""
"In the case you run into problems opening the retirement process "
"yourself, contact NM front desk using ``nm@debian.org``"
msgstr ""

#: ../developer-duties.rst:263
msgid ""
"It is important that the above process is followed, because finding "
"inactive developers and orphaning their packages takes significant time "
"and effort."
msgstr ""
"È importante che il processo di cui sopra sia seguito, perché trovare "
"sviluppatori inattivi e rendere orfani i loro pacchetti richiede molto "
"tempo e lavoro."

#: ../developer-duties.rst:270
msgid "Returning after retirement"
msgstr "Ritornare dopo il congedo"

#: ../developer-duties.rst:272
#, fuzzy
msgid ""
"A retired developer's account is marked as \"emeritus\" when the process "
"in :ref:`s3.7` is followed, and \"removed\" otherwise. Retired developers"
" with an \"emeritus\" account can get their account re-activated as "
"follows:"
msgstr ""
"l'account di uno sviluppatore congedato è contrassegnato come «emerito» "
"quando il processo in :ref:`s3.7` è seguito e «disabled» in caso "
"contrario. Gli sviluppatori ritirati con un account «emerito» possono "
"ottenere il loro account riattivato come segue:"

#: ../developer-duties.rst:277
msgid ""
"Get access to an salsa account (either by remembering the credentials for"
" your old guest account or by requesting a new one as described at `SSO "
"Debian wiki page "
"<https://wiki.debian.org/DebianSingleSignOn#If_you_ARE_NOT_.28yet.29_a_Debian_Developer>`__."
msgstr ""

#: ../developer-duties.rst:282
msgid "Mail ``nm@debian.org`` for further instructions."
msgstr ""

#: ../developer-duties.rst:284
msgid ""
"Go through a shortened NM process (to ensure that the returning developer"
" still knows important parts of P&P and T&S)."
msgstr ""
"Si passi attraverso un processo di NM accorciato (per garantire che il "
"committente tornando sappia ancora parti importanti della P&P and T&S)."

#: ../developer-duties.rst:287
#, fuzzy
msgid ""
"Retired developers with a \"removed\" account need to go through full NM "
"again."
msgstr ""
"Gli sviluppatori congedati con un account «disabilitato» necessitano "
"nuovamente di passare attraverso NM."

#: ../developer-duties.rst:291
msgid ""
"This is so that the message can be easily filtered by people who don't "
"want to read vacation notices."
msgstr ""
"Questo è come il messaggio può essere facilmente filtrato da persone che "
"non vogliono leggere avvisi di vacanza."

#~ msgid ""
#~ "You can find a more in-depth "
#~ "discussion of Debian key maintenance in"
#~ " the documentation of the ``debian-"
#~ "keyring`` package and the "
#~ "http://keyring.debian.org/ site."
#~ msgstr ""

#~ msgid ""
#~ "Send an gpg-signed email announcing "
#~ "your retirement to ``debian-"
#~ "private@lists.debian.org``."
#~ msgstr "Inviare una email gpg-firmata sul perché si sta lasciando il progetto a"

#~ msgid ""
#~ "Notify the Debian key ring maintainers"
#~ " that you are leaving by opening "
#~ "a ticket in Debian RT by sending"
#~ " a mail to ``keyring@rt.debian.org`` with"
#~ " the words \"Debian RT\" somewhere in"
#~ " the subject line (case doesn't "
#~ "matter)."
#~ msgstr ""

#~ msgid "Contact ``da-manager@debian.org``."
#~ msgstr "Si contatti ``da-manager@debian.org``."

#~ msgid ""
#~ "Prove that they still control the "
#~ "GPG key associated with the account, "
#~ "or provide proof of identify on a"
#~ " new GPG key, with at least two"
#~ " signatures from other developers."
#~ msgstr ""
#~ "Si dimostri che ancora si controlla "
#~ "la chiave GPG associata all'account, o"
#~ " si fornisca la prova di "
#~ "identificazione su una nuova chiave GPG,"
#~ " con almeno due firme da altri "
#~ "sviluppatori."

#~ msgid ""
#~ "Authentification to the NM site requires"
#~ " an SSO browser certificate. You can"
#~ " generate them on https://sso.debian.org."
#~ msgstr ""

#~ msgid ""
#~ "Get access to an alioth account "
#~ "(either by remembering the credentials "
#~ "for your old guest account or by"
#~ " requesting a new one as decribed "
#~ "at `SSO Debian wiki page "
#~ "<https://wiki.debian.org/DebianSingleSignOn#If_you_ARE_NOT_.28yet.29_a_Debian_Developer>`__."
#~ msgstr ""

#~ msgid ""
#~ "Get access to an alioth account "
#~ "(either by remembering the credentials "
#~ "for your old guest account or by"
#~ " requesting a new one as described"
#~ " at `SSO Debian wiki page "
#~ "<https://wiki.debian.org/DebianSingleSignOn#If_you_ARE_NOT_.28yet.29_a_Debian_Developer>`__."
#~ msgstr ""

