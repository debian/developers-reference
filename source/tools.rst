.. _tools:

Overview of Debian Maintainer Tools
********************************************************************************************************************************

This section contains a rough overview of the tools available to
maintainers. The following is by no means complete or definitive, but
just a guide to some of the more popular tools.

Debian maintainer tools are meant to aid developers and free their time
for critical tasks. As Larry Wall says, there's more than one way to do
it.

Some people prefer to use high-level package maintenance tools and some
do not. Debian is officially agnostic on this issue; any tool that gets
the job done is fine. Therefore, this section is not meant to stipulate
to anyone which tools they should use or how they should go about their
duties of maintainership. Nor is it meant to endorse any particular tool
to the exclusion of a competing tool.

Most of the descriptions of these packages come from the actual package
descriptions themselves. Further information can be found in the package
documentation itself. You can also see more info with the command
``apt-cache show`` *package-name*.

.. _tools-core:

Core tools
================================================================================================================================

The following tools are pretty much required for any maintainer.

``dpkg-dev``
--------------------------------------------------------------------------------------------------------------------------------

``dpkg-dev`` contains the tools (including ``dpkg-source``) required to
unpack, build, and upload Debian source packages. These utilities
contain the fundamental, low-level functionality required to create and
manipulate packages; as such, they are essential for any Debian
maintainer.

``debconf``
--------------------------------------------------------------------------------------------------------------------------------

``debconf`` provides a consistent interface to configuring packages
interactively. It is user interface independent, allowing end-users to
configure packages with a text-only interface, an HTML interface, or a
dialog interface. New interfaces can be added as modules.

You can find documentation for this package in the ``debconf-doc``
package.

Many feel that this system should be used for all packages that require
interactive configuration; see :ref:`bpp-config-mgmt`. ``debconf``
is not currently required by Debian Policy, but that may change in the
future.

``fakeroot``
--------------------------------------------------------------------------------------------------------------------------------

``fakeroot`` simulates root privileges. This enables you to build
packages without being root (packages usually want to install files with
root ownership). If you have ``fakeroot`` installed,
``dpkg-buildpackage`` will use it automatically.

.. _tools-lint:

Package lint tools
================================================================================================================================

According to the Free On-line Dictionary of Computing (FOLDOC), ``lint``
is: "A Unix C language processor which carries out more thorough checks
on the code than is usual with C compilers." Package lint tools help
package maintainers by automatically finding common problems and policy
violations in their packages.

.. _lintian:

``lintian``
--------------------------------------------------------------------------------------------------------------------------------

``lintian`` dissects Debian packages and emits information about bugs
and policy violations. It contains automated checks for many aspects of
Debian policy as well as some checks for common errors.

You should periodically get the newest ``lintian`` from ``unstable`` and
check over all your packages. Notice that the ``-i`` option provides
detailed explanations of what each error or warning means, what its
basis in Policy is, and commonly how you can fix the problem.

Refer to :ref:`sanitycheck` for more information on how and when to
use Lintian.

You can also see a summary of all problems reported by Lintian on your
packages at https://lintian.debian.org/\ . These reports contain
the latest ``lintian`` output for the whole development distribution
(``unstable``).

``lintian-brush``
--------------------------------------------------------------------------------------------------------------------------------

``lintian-brush`` contains a set of scripts that can automatically
fix more than 80 common lintian issues in Debian packages.

It comes with a wrapper script that invokes the scripts, updates
the changelog (if desired) and commits each change to version control.

``piuparts``
--------------------------------------------------------------------------------------------------------------------------------

``piuparts`` is the  ``.deb`` package installation, upgrading, and removal
testing tool.

``piuparts`` tests that ``.deb packages`` handle installation, upgrading,
and removal correctly. It does this by creating a minimal Debian
installation in a chroot, and installing, upgrading, and removing packages
in that environment, and comparing the state of the directory tree before
and after. ``piuparts`` reports any files that have been added, removed,
or modified during this process.

``piuparts`` is meant as a quality assurance tool for people who create
``.deb`` packages to test them before they upload them to the Debian
archive.

.. _debdiff:

``debdiff``
--------------------------------------------------------------------------------------------------------------------------------

``debdiff`` (from the ``devscripts`` package, :ref:`devscripts`)
compares file lists and control files of two packages. It is a simple
regression test, as it will help you notice if the number of binary
packages has changed since the last upload, or if something has changed
in the control file. Of course, some of the changes it reports will be
all right, but it can help you prevent various accidents.

You can run it over a pair of binary packages:

.. code-block:: sh

   debdiff package_1-1_arch.deb package_2-1_arch.deb

Or even a pair of changes files:

.. code-block:: sh

   debdiff package_1-1_arch.changes package_2-1_arch.changes

For more information please see debdiff 1.

.. _diffoscope:

``diffoscope``
--------------------------------------------------------------------------------------------------------------------------------

``diffoscope`` provides in-depth comparison of files, archives, and directories.

``diffoscope`` will try to get to the bottom of what makes files or directories
different. It will recursively unpack archives of many kinds and transform
various binary formats into more human readable form to compare them.

Originally developed to compare two ``.deb`` files or two ``changes`` files
nowadays it can compare two tarballs, ISO images, or PDF just as easily and
supports a huge variety of filetypes.

The differences can be shown in a text or HTML report or as JSON output.

``duck``
--------------------------------------------------------------------------------------------------------------------------------

``duck``, the Debian Url ChecKer, processes several fields in the
``debian/control``, ``debian/upstream``, ``debian/copyright``,
``debian/patches/*`` and ``systemd.unit`` files and checks if URLs,
VCS links and email address domains found therein are valid.

``adequate``
--------------------------------------------------------------------------------------------------------------------------------

``adequate`` checks packages installed on the system and reports bugs
and policy violations.

The following checks are currently implemented:

-  broken symlinks
-  missing copyright file
-  obsolete conffiles
-  Python modules not byte-compiled
-  ``/bin`` and ``/sbin`` binaries requiring ``/usr/lib`` libraries
-  missing libraries, undefined symbols, symbol size mismatches
-  license conflicts
-  program name collisions
-  missing alternatives
-  missing ``binfmt`` interpreters and detectors
-  missing ``pkg-config`` dependencies

``i18nspector``
--------------------------------------------------------------------------------------------------------------------------------

``i18nspector`` is a tool for checking translation templates (POT), message
catalogues (PO) and compiled message catalogues (MO) files for common problems.

``cme``
--------------------------------------------------------------------------------------------------------------------------------
``cme`` is a tool from the ``libconfig-model-dpkg-perl`` package is an editor
for dpkg source files with validation. Check the package description to see
what it can do.

``licensecheck``
--------------------------------------------------------------------------------------------------------------------------------
``licensecheck`` attempts to determine the license that applies to each
file passed to it, by searching the start of the file for text belonging
to various licenses.

``blhc``
--------------------------------------------------------------------------------------------------------------------------------
``blhc`` is a tool which checks build logs for missing hardening flags.

.. _tools-helpers:

Helpers for ``debian/rules``
================================================================================================================================

Package building tools make the process of writing ``debian/rules``
files easier. See :ref:`helper-scripts` for more information about
why these might or might not be desired.

``debhelper``
--------------------------------------------------------------------------------------------------------------------------------

``debhelper`` is a collection of programs that can be used in
``debian/rules`` to automate common tasks related to building binary
Debian packages. ``debhelper`` includes programs to install various
files into your package, compress files, fix file permissions, and
integrate your package with the Debian menu system.

Unlike some approaches, ``debhelper`` is broken into several small,
simple commands, which act in a consistent manner. As such, it allows
more fine-grained control than some of the other debian/rules tools.

There are a number of little ``debhelper`` add-on packages, too
transient to document. You can see the list of most of them by doing
``apt-cache search ^dh-``.

When choosing a ``debhelper`` compatibility level for your package, you
should choose the highest compatibility level that is supported in the
most recent stable release. Only use a higher compatibility level if you
need specific features that are provided by that compatibility level
that are not available in earlier levels.

In the past the compatibility level was defined in ``debian/compat``,
however nowadays it is much better to not use that but rather to use a
versioned build-dependency like ``debhelper-compat (=12)``.

.. _dh-make:

``dh-make``
--------------------------------------------------------------------------------------------------------------------------------

The ``dh-make`` package contains ``dh_make``, a program that creates a
skeleton of files necessary to build a Debian package out of a source
tree. As the name suggests, ``dh_make`` is a rewrite of ``debmake``, and
its template files use ``dh_*`` programs from ``debhelper``.

While the rules files generated by ``dh_make`` are in general a
sufficient basis for a working package, they are still just the
groundwork: the burden still lies on the maintainer to finely tune the
generated files and make the package entirely functional and
Policy-compliant.

``equivs``
--------------------------------------------------------------------------------------------------------------------------------

``equivs`` is another package for making packages. It is often suggested
for local use if you need to make a package simply to fulfill
dependencies. It is also sometimes used when making *meta-packages*,
which are packages whose only purpose is to depend on other packages.

.. _tools-builders:

Package builders
================================================================================================================================

The following packages help with the package building process, general
driving of ``dpkg-buildpackage``, as well as handling supporting tasks.

``git-buildpackage``
--------------------------------------------------------------------------------------------------------------------------------

``git-buildpackage`` provides the capability to inject or import Debian
source packages into a Git repository, build a Debian package from the
Git repository, and helps in integrating upstream changes into the
repository.

These utilities provide an infrastructure to facilitate the use of Git
by Debian maintainers. This allows one to keep separate Git branches of
a package for ``stable``, ``unstable`` and possibly ``experimental``
distributions, along with the other benefits of a version control
system.

.. _debootstrap:

``debootstrap``
--------------------------------------------------------------------------------------------------------------------------------

The ``debootstrap`` package and script allows you to bootstrap a Debian
base system into any part of your filesystem. By base system, we mean
the bare minimum of packages required to operate and install the rest of
the system.

Having a system like this can be useful in many ways. For instance, you
can ``chroot`` into it if you want to test your build dependencies. Or
you can test how your package behaves when installed into a bare base
system. Chroot builders use this package; see below.

.. _pbuilder:

``pbuilder``
--------------------------------------------------------------------------------------------------------------------------------

``pbuilder`` constructs a chrooted system, and builds a package inside
the chroot. It is very useful to check that a package's build
dependencies are correct, and to be sure that unnecessary and wrong
build dependencies will not exist in the resulting package.

A related package is ``cowbuilder``, which speeds up the build process
using a COW filesystem on any standard Linux filesystem.

.. _sbuild:

``sbuild``
--------------------------------------------------------------------------------------------------------------------------------

``sbuild`` is another automated builder. It can use chrooted
environments as well. It can be used stand-alone, or as part of a
networked, distributed build environment. As the latter, it is part of
the system used by porters to build binary packages for all the
available architectures. See :ref:`wanna-build` for more
information, and https://buildd.debian.org/\  to see the system in
action.

.. _uploaders:

Package uploaders
================================================================================================================================

The following packages help automate or simplify the process of
uploading packages into the official archive.

.. _dupload:

``dupload``
--------------------------------------------------------------------------------------------------------------------------------

``dupload`` is a package and a script to automatically upload Debian
packages to the Debian archive, to log the upload, and to optionally send
mail about the upload of a package. It supports various kinds of hooks to
extend its functionality, and can be configured for new upload locations
or methods, although by default it provides various hooks performing checks
and comes configured with all Debian upload locations.

.. _dput:

``dput``
--------------------------------------------------------------------------------------------------------------------------------

The ``dput`` package and script do much the same thing as ``dupload``,
but in a different way. Out of the box it supports to run ``dinstall``
in dry-run mode after the upload.

.. _dcut:

``dcut``
--------------------------------------------------------------------------------------------------------------------------------

The ``dcut`` script (part of the package ``dput``, :ref:`dput`) helps
in removing files from the ftp upload directory.

.. _tools-maint-automate:

Maintenance automation
================================================================================================================================

The following tools help automate different maintenance tasks, from
adding changelog entries or signature lines and looking up bugs in Emacs
to making use of the newest and official ``config.sub``.

.. _devscripts:

``devscripts``
--------------------------------------------------------------------------------------------------------------------------------

``devscripts`` is a package containing wrappers and tools that are very
helpful for maintaining your Debian packages. Example scripts include
``debchange`` (or its alias, ``dch``), which manipulates your
``debian/changelog`` file from the command-line, and ``debuild``, which
is a wrapper around ``dpkg-buildpackage``. The ``bts`` utility is also
very helpful to update the state of bug reports on the command line.
``uscan`` can be used to watch for new upstream versions of your
packages (see https://wiki.debian.org/debian/watch for more info on that).
``suspicious-source`` outputs a list of files which are not common source
files.

See the devscripts 1 manual page for a complete list of available
scripts.

``reportbug``
--------------------------------------------------------------------------------------------------------------------------------

``reportbug`` is a tool designed to make the reporting of bugs in Debian
and derived distributions relatively painless.  Its features include:

-  Integration with mutt and mh/nmh mail readers.
-  Access to outstanding bug reports to make it easier to identify
   whether problems have already been reported.
-  Automatic checking for newer versions of packages.

``reportbug`` is designed to be used on systems with an installed mail
transport agent; however, you can edit the configuration file and send
reports using any available mail server.

This package also includes the ``querybts`` script for browsing the
Debian `bug tracking system <https://www.debian.org/Bugs/>`__.

``autotools-dev``
--------------------------------------------------------------------------------------------------------------------------------

``autotools-dev`` contains best practices for people who maintain
packages that use ``autoconf`` and/or ``automake``. Also contains
canonical ``config.sub`` and ``config.guess`` files, which are known to
work on all Debian ports.

``dpkg-repack``
--------------------------------------------------------------------------------------------------------------------------------

``dpkg-repack`` creates a Debian package file out of a package that has
already been installed. If any changes have been made to the package
while it was unpacked (e.g., files in ``/etc`` were modified), the new
package will inherit the changes.

This utility can make it easy to copy packages from one computer to
another, or to recreate packages that are installed on your system but
no longer available elsewhere, or to save the current state of a package
before you upgrade it.

``alien``
--------------------------------------------------------------------------------------------------------------------------------

``alien`` converts binary packages between various packaging formats,
including Debian, RPM (RedHat), LSB (Linux Standard Base), Solaris, and
Slackware packages.

.. _dpkg-dev-el:

``dpkg-dev-el``
--------------------------------------------------------------------------------------------------------------------------------

``dpkg-dev-el`` is an Emacs lisp package that provides assistance when
editing some of the files in the ``debian`` directory of your package.
For instance, there are handy functions for listing a package's current
bugs, and for finalizing the latest entry in a ``debian/changelog``
file.

.. _dpkg-depcheck:

``dpkg-depcheck``
--------------------------------------------------------------------------------------------------------------------------------

``dpkg-depcheck`` (from the ``devscripts`` package, :ref:`devscripts`)
runs a command under ``strace`` to determine all the packages that were
used by the said command.

For Debian packages, this is useful when you have to compose a
``Build-Depends`` line for your new package: running the build process
through ``dpkg-depcheck`` will provide you with a good first
approximation of the build-dependencies. For example:

.. code-block:: sh

   dpkg-depcheck -b debian/rules build

``dpkg-depcheck`` can also be used to check for run-time dependencies,
especially if your package uses exec 2 to run other programs.

For more information please see dpkg-depcheck 1.

.. _debputy:

``debputy``
--------------------------------------------------------------------------------------------------------------------------------

The ``debputy`` tools is new since 2024. While its main purpose is to offer a
new Debian package build paradigm, it includes subcommands that can be used on
any existing Debian package to validate the correctness of most of the files in
``debian/*``, and in many cases also automatically fix them.

To check correctness of files in debian/* run:

.. code-block:: sh

   debputy lint --spellcheck

To format debian/control and debian/tests/control files

.. code-block:: sh

    debputy reformat --style black

Using the ``reformat`` command obsoletes using ``wrap-and-sort -ast``.

The debputy tool also includes a language server which, when integrated with a
code editor, can give real-time feedback on the correctness of files in
``debian/*`` while editing them.

For more information please see debputy 1.

.. _tools-porting:

Porting tools
================================================================================================================================

The following tools are helpful for porters and for cross-compilation.

``dpkg-cross``
--------------------------------------------------------------------------------------------------------------------------------

``dpkg-cross`` is a tool for installing libraries and headers for
cross-compiling in a way similar to ``dpkg``. Furthermore, the
functionality of ``dpkg-buildpackage`` and ``dpkg-shlibdeps`` is
enhanced to support cross-compiling.

.. _tools-doc:

Documentation and information
================================================================================================================================

The following packages provide information for maintainers or help with
building documentation.

``debian-policy``
--------------------------------------------------------------------------------------------------------------------------------

The ``debian-policy`` package contains the Debian Policy Manual
and related documents, which are:

-  Debian Policy Manual
-  Filesystem Hierarchy Standard (FHS)
-  Debian Menu sub-policy
-  Debian Perl sub-policy
-  Debian configuration management specification
-  Machine-readable debian/copyright specification
-  Autopkgtest - automatic as-installed package testing
-  Authoritative list of virtual package names
-  Policy checklist for upgrading your packages

The Debian Policy Manual the policy relating to packages and details of
the packaging mechanism. It covers everything from required ``gcc``
options to the way the maintainer scripts (``postinst`` etc.) work,
package sections and priorities, etc.

Also useful is the file
``/usr/share/doc/debian-policy/upgrading-checklist.txt.gz``,
which lists changes between versions of policy.

``doc-debian``
--------------------------------------------------------------------------------------------------------------------------------

``doc-debian`` contains lots of useful Debian-specific documentation:

-  Debian Linux Manifesto
-  Constitution for the Debian Project
-  Debian Social Contract
-  Debian Free Software Guidelines
-  Debian Bug Tracking System documentation
-  Introduction to the Debian mailing lists

``developers-reference``
--------------------------------------------------------------------------------------------------------------------------------

The ``developers-reference`` package contains the document you are
reading right now, the Debian Developer's Reference, a set of
guidelines and best practices which has been established by and for
the community of Debian developers.

``maint-guide``
--------------------------------------------------------------------------------------------------------------------------------

The ``maint-guide`` package contains the Debian New Maintainers' Guide.

This document tries to describe the building of a Debian package to
ordinary Debian users and prospective developers. It uses fairly
non-technical language, and it's well covered with working examples.

``debmake-doc``
--------------------------------------------------------------------------------------------------------------------------------

The ``debmake-doc`` package contains the Guide for Debian Maintainers.

This document is newer than Debian New Maintainers' Guide and intends to replace
it. The Guide for Debian Maintainers caters to those learning Debian packaging
and covers a wide range of topics and tools, along with plenty of examples about
various types of packaging issues.

``packaging-tutorial``
--------------------------------------------------------------------------------------------------------------------------------

This tutorial is an introduction to Debian packaging. It teaches
prospective developers how to modify existing packages, how to create
their own packages,  and how to interact with the Debian community.

In addition to the main tutorial, it includes three practical sessions
on modifying the ``grep`` package, and packaging the ``gnujump`` game
and a Java library.

``how-can-i-help``
--------------------------------------------------------------------------------------------------------------------------------

``how-can-i-help`` shows opportunities for contributing to Debian.
``how-can-i-help`` hooks into ``APT`` to list opportunities for contributions to
Debian (orphaned packages, bugs tagged 'newcomer') for packages installed
locally, after each ``APT`` invocation. It can also be invoked directly, and
then lists all opportunities for contribution (not just the new ones).

``docbook-xml``
--------------------------------------------------------------------------------------------------------------------------------

``docbook-xml`` provides the DocBook XML DTDs, which are commonly used
for Debian documentation (as is the older debiandoc SGML DTD).

The ``docbook-xsl`` package provides the XSL files for building and
styling the source to various output formats. You will need an XSLT
processor, such as ``xsltproc``, to use the XSL stylesheets.
Documentation for the stylesheets can be found in the various
``docbook-xsl-doc-*`` packages.

To produce PDF from FO, you need an FO processor, such as ``xmlroff`` or
``fop``. Another tool to generate PDF from DocBook XML is ``dblatex``.

``debiandoc-sgml``
--------------------------------------------------------------------------------------------------------------------------------

``debiandoc-sgml`` provides the DebianDoc SGML DTD, which has been
commonly used for Debian documentation, but is now deprecated
(``docbook-xml`` or ``python3-sphinx`` should be used instead).

``debian-keyring``
--------------------------------------------------------------------------------------------------------------------------------

Contains the public OpenPGP keys of Debian Developers and Maintainers. See
:ref:`key-maint` and the package documentation for more information.

``debian-el``
--------------------------------------------------------------------------------------------------------------------------------

``debian-el`` provides an Emacs mode for viewing Debian binary packages.
This lets you examine a package without unpacking it.
