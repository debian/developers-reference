.. _pkgs:

Managing Packages
********************************************************************************************************************************

This chapter contains information related to creating, uploading,
maintaining, and porting packages.

.. _newpackage:

New packages
================================================================================================================================

If you want to create a new package for the Debian distribution, you
should first check the `Work-Needing and Prospective Packages
(WNPP) <https://www.debian.org/devel/wnpp/>`__ list. Checking the WNPP
list ensures that no one is already working on packaging that software,
and that effort is not duplicated. Read the `WNPP web
pages <https://www.debian.org/devel/wnpp/>`__ for more information.

Assuming no one else is already working on your prospective package, you
must then submit a bug report (:ref:`submit-bug`) against the
pseudo-package ``wnpp`` describing your plan to create a new package,
including, but not limiting yourself to, the description of the package
(so that others can review it), the license of the prospective package,
and the current URL where it can be downloaded from.

You should set the subject of the bug to ``ITP:`` *foo* ``--`` *short
description*, substituting the name of the new package for *foo*. The
severity of the bug report must be set to ``wishlist``. Please send a
copy to ``debian-devel@lists.debian.org`` by using the X-Debbugs-CC
header (don't use CC:, because that way the message's subject won't
indicate the bug number). If you are packaging so many new packages
(>10) that notifying the mailing list in separate messages is too
disruptive, send a summary after filing the bugs to the debian-devel
list instead. This will inform the other developers about upcoming
packages and will allow a review of your description and package name.

Please include a ``Closes: #``\ *nnnnn* entry in the changelog of the
new package in order for the bug report to be automatically closed once
the new package is installed in the archive (see :ref:`upload-bugfix`).

If you think your package needs some explanations for the administrators
of the NEW package queue, include them in your changelog, send to
``ftpmaster@debian.org`` a reply to the email you receive as a
maintainer after your upload, or reply to the rejection email in case
you are already re-uploading.

When closing security bugs include CVE numbers as well as the
``Closes: #``\ *nnnnn*. This is useful for the security team to track
vulnerabilities. If an upload is made to fix the bug before the advisory
ID is known, it is encouraged to modify the historical changelog entry
with the next upload. Even in this case, please include all available
pointers to background information in the original changelog entry.

There are a number of reasons why we ask maintainers to announce their
intentions:

-  It helps the (potentially new) maintainer to tap into the experience
   of people on the list, and lets them know if anyone else is working
   on it already.

-  It lets other people thinking about working on the package know that
   there already is a volunteer, so efforts may be shared.

-  It lets the rest of the maintainers know more about the package than
   the one line description and the usual changelog entry
   ``Initial release`` that gets posted to
   ``debian-devel-changes@lists.debian.org``.

-  It is helpful to the people who live off ``unstable`` (and form our
   first line of testers). We should encourage these people.

-  The announcements give maintainers and other interested parties a
   better feel of what is going on, and what is new, in the project.

Please see https://ftp-master.debian.org/REJECT-FAQ.html\  for
common rejection reasons for a new package.

.. _changelog-entries:

Recording changes in the package
================================================================================================================================

Changes that you make to the package need to be recorded in the
``debian/changelog`` file, for human users to read and comprehend.
These changes should provide a concise description of what was changed,
why (if it's in doubt), and note if any bugs were closed. They also
record when the packaging was completed. This file will be installed in
``/usr/share/doc/``\ *package*\ ``/changelog.Debian.gz``, or
``/usr/share/doc/``\ *package*\ ``/changelog.gz`` for native packages.

The ``debian/changelog`` file conforms to a certain structure, with a
number of different fields. One field of note, the ``distribution``, is
described in :ref:`distribution`. More
information about the structure of this file can be found in the Debian
Policy section titled ``debian/changelog``.

Changelog entries can be used to automatically close Debian bugs when
the package is installed into the archive. See :ref:`upload-bugfix`.

It is conventional that the changelog entry of a package that contains a
new upstream version of the software looks like this:

::

     * New upstream release.

There are tools to help you create entries and finalize the
``changelog`` for release — see :ref:`devscripts` and
:ref:`dpkg-dev-el`.

See also :ref:`bpp-debian-changelog`.

.. _sanitycheck:

Testing the package
================================================================================================================================

Before you upload your package, you should do basic testing on it. At a
minimum, you should try the following activities (you'll need to have an
older version of the same Debian package around):

-  Run ``lintian`` over the package. You can run ``lintian`` as follows:
   ``lintian -v`` *package-version*\ ``.changes``. This will check the
   source package as well as the binary package. If you don't understand
   the output that ``lintian`` generates, try adding the ``-i`` switch,
   which will cause ``lintian`` to output a very verbose description of
   the problem.

   Normally, a package should *not* be uploaded if it causes ``lintian``
   to emit errors (they will start with ``E``).

   For more information on ``lintian``, see :ref:`lintian`.

-  Optionally run ``debdiff`` (see :ref:`debdiff`) to analyze
   changes from an older version, if one exists.

-  Install the package and make sure the software works in an up-to-date
   ``unstable`` system.

-  Upgrade the package from an older version to your new version.

-  Remove the package, then reinstall it.

-  Installing, upgrading and removal of packages can either be tested
   manually or by using the ``piuparts`` tool.

-  Copy the source package in a different directory and try unpacking it
   and rebuilding it. This tests if the package relies on existing files
   outside of it, or if it relies on permissions being preserved on the
   files shipped inside the ``.diff.gz`` file.

.. _sourcelayout:

Layout of the source package
================================================================================================================================

There are two types of Debian source packages:

-  the so-called ``native`` packages, where there is no distinction
   between the original sources and the patches applied for Debian

-  the (more common) packages where there's an original source tarball
   file accompanied by another file that contains the changes made by
   Debian

For the native packages, the source package includes a Debian source
control file (``.dsc``) and the source tarball (``.tar.{gz,bz2,xz}``). A
source package of a non-native package includes a Debian source control
file, the original source tarball (``.orig.tar.{gz,bz2,xz}``) and the
Debian changes (``.diff.gz`` for the source format “1.0” or
``.debian.tar.{gz,bz2,xz}`` for the source format “3.0 (quilt)”).

With source format “1.0”, whether a package is native or not was
determined by ``dpkg-source`` at build time. Nowadays it is recommended
to be explicit about the desired source format by putting either “3.0
(quilt)” or “3.0 (native)” in ``debian/source/format``. The rest of this
section relates only to non-native packages.

The first time a version is uploaded that corresponds to a particular
upstream version, the original source tar file must be uploaded and
included in the ``.changes`` file. Subsequently, this very same tar file
should be used to build the new diffs and ``.dsc`` files, and will not
need to be re-uploaded.

By default, ``dpkg-genchanges`` and ``dpkg-buildpackage`` will include
the original source tar file if and only if the current changelog entry
has a different upstream version from the preceding entry. This behavior
may be modified by using ``-sa`` to always include it or ``-sd`` to
always leave it out.

If no original source is included in the upload, the original source
tar-file used by ``dpkg-source`` when constructing the ``.dsc`` file and
diff to be uploaded *must* be byte-for-byte identical with the one
already in the archive.

Please notice that, in non-native packages, permissions on files that
are not present in the ``*.orig.tar.{gz,bz2,xz}`` will not be preserved,
as diff does not store file permissions in the patch. However, when
using source format “3.0 (quilt)”, permissions of files inside the
``debian`` directory are preserved since they are stored in a tar
archive.

.. _distribution:

Picking a distribution
================================================================================================================================

Each upload needs to specify which distribution the package is intended
for. The package build process extracts this information from the first
line of the ``debian/changelog`` file and places it in the
``Distribution`` field of the ``.changes`` file.

Packages are normally uploaded into ``unstable``. Uploads to
``unstable`` or ``experimental`` should use these suite names in the
changelog entry; uploads for other supported suites should use the suite
codenames, as they avoid any ambiguity.

Actually, there are other possible distributions:
|codename-security|, but read :ref:`bug-security` for more information on those.

It is not possible to upload a package into several distributions at the
same time.

.. _upload-stable:

Special case: uploads to the ``stable`` and ``oldstable`` distributions
--------------------------------------------------------------------------------------------------------------------------------

Uploading to ``stable`` means that the package will be transferred to
the ``proposed-updates-new`` queue for review by the stable release
managers, and if approved will be installed in the
``stable-proposed-updates`` directory of the Debian archive. From there,
it will be included in ``stable`` with the next point release.

Uploads to a supported ``stable`` release should target their suite name in
the changelog, i.e. ``bookworm`` or ``bullseye``. You should normally use
``reportbug`` and the ``release.debian.org`` pseudo-package to send a *source*
``debdiff``, rationale and associated bug numbers to the stable release
managers, and await a request to upload or further information.

If you are confident that the upload will be accepted without changes,
please feel free to upload at the same time as filing the
``release.debian.org`` bug. However if you are new to the process, we would
recommend getting approval before uploading so you get a chance to see
if your expectations align with ours.

Either way, there must be an accompanying bug for tracking, and your
upload must comply with these acceptance criteria defined by the
the stable release managers. These criteria are designed to help the process
be as smooth and frustration-free as possible.

-  The bug you want to fix in ``stable`` must be fixed in ``unstable``
   already (and not waiting in NEW or the delayed queue).
-  The bug should be of severity "important" or higher.
-  Bug meta-data - particularly affected versions - must be
   up to date.
-  Fixes must be minimal and relevant and include a sufficiently
   detailed changelog entry.
-  A source debdiff of the proposed change must be included
   in your request (not just the raw patches or "a debdiff
   can be found at $URL").
-  The proposed package must have a correct version number
   (e.g. ``...+deb12u1`` for ``bookworm`` or ``+deb11u1`` for ``bullseye``)
   and you should be able to explain what testing it has had.
   See the Debian Policy for the version number:
   https://www.debian.org/doc/debian-policy/ch-controlfields.html#special-version-conventions
-  The update must be built in an ``stable`` environment
   or chroot (or ``oldstable`` if you target that).
-  Fixes for security issues should be co-ordinated with the
   security team, unless they have explicitly stated that they
   will not issue an ``DSA`` for the bug (e.g. via a "no-dsa" marker
   in the :ref:`bug-security-tracker`).
-  Do not close ``release.debian.org`` bugs in debian/changelog. They
   will be closed by the release team once the package has reached the
   respective point release.

It is recommended to use ``reportbug`` as it eases the creation of bugs
with correct meta-data. The release team makes extensive use of usertags
to sort and manage requests and incorrectly tagged reports may take
longer to be noticed and processed.

Uploads to the ``oldstable`` distributions are possible as long as it
hasn't been archived. The same rules as for ``stable`` apply.

In the past, uploads to ``stable`` were used to address security
problems as well. However, this practice is deprecated, as uploads used
for Debian security advisories (``DSA``) are automatically copied to the
appropriate ``proposed-updates`` archive when the advisory is released.
See :ref:`bug-security` for detailed
information on handling security problems. If the security team deems
the problem to be too benign to be fixed through a ``DSA``, the stable
release managers are usually willing to include your fix nonetheless in
a regular upload to ``stable``.

.. _upload-stable-updates:

Special case: the ``stable-updates`` suite
--------------------------------------------------------------------------------------------------------------------------------

Sometimes the stable release managers will decide that an update to
stable should be made available to users sooner than the next scheduled
point release. In such cases, they can copy the update to the ``stable-updates``
suite, use of which is enabled by the installer by default.

Initially, the process described in :ref:`upload-stable`. should be followed
as usual. If you think that the upload should be released via
``stable-updates``, mention this in your request. Examples of circumstances in
which the upload may qualify for such treatment are:

- The update is urgent and not of a security nature.  Security updates
  will continue to be pushed through the security archive.  Examples
  include packages broken by the flow of time (c.f. ``spamassassin`` and
  the year 2010 problem) and fixes for bugs introduced by point
  releases.
- The package in question is a data package and the data must be
  updated in a timely manner (e.g. ``tzdata``).
- Fixes to leaf packages that were broken by external changes (e.g.
  video downloading tools and ``tor``).
- Packages that need to be current to be useful (e.g. ``clamav``).
- Uploads to ``stable-updates`` should target their suite name in
  the changelog as usual, e.g. ``bookworm``.

Once the upload has been accepted to ``proposed-updates`` and is ready
for release, the stable release managers will then copy it to the
``stable-updates`` suite and issue a Stable Update Announcement (``SUA``)
via the ``debian-stable-announce`` mailing list.

Any updates released via ``stable-updates`` will be included in ``stable``
with the next point release as usual.

.. _upload-t-p-u:

Special case: uploads to ``testing/testing-proposed-updates``
--------------------------------------------------------------------------------------------------------------------------------

Please see the information in the :ref:`t-p-u` for
details.

.. _upload:

Uploading a package
================================================================================================================================

Source and binary uploads
--------------------------------------------------------------------------------------------------------------------------------

Each upload to Debian consists of a signed ``.changes`` file describing
the requested change to the archive, plus the source and binary package
files that are referenced by the ``.changes`` file.

If possible, the version of a package that is uploaded should be a
source-only changes file.
These are typically named ``*_source.changes``, and reference the source
package, but no binary ``.deb`` or ``.udeb`` packages.
All of the corresponding architecture-dependent and architecture-independent
binary packages, for all architectures, will be built automatically by
the build daemons in a controlled and predictable environment
(see :ref:`wanna-build` for more details).
However, there are several situations where this is not possible.

The first upload of a new source package (see :ref:`newpackage`)
must include binary packages, so that they can be reviewed by the
archive administrators before they are added to Debian.

If new binary packages are added to an existing source package, then the
first upload that lists the new binary packages in ``debian/control``
must include binary packages, again so that they can be reviewed by the
archive administrators before they are added to Debian.
It is preferred for these uploads to be done via the ``experimental``
suite.

Uploads that will be held for review in other queues, such as packages
being added to the ``*-backports`` suites, might also require inclusion
of binary packages.

The build daemons will automatically attempt to build any ``main`` or
``contrib`` package for which the build-dependencies are available.
Packages in ``non-free`` and ``non-free-firmware`` will not be built by
the build daemons unless the package has been marked as suitable for
auto-building
(see :ref:`non-free-buildd`).

The build daemons only install build-dependencies from the ``main``
archive area.
This means that if a source package has build-dependencies that are
in the ``contrib``, ``non-free`` or ``non-free-firmware`` archive areas,
then uploads of that package need to include prebuilt binary packages
for every architecture that will be supported.
By definition this can only be the case for source packages that are
themselves in the ``contrib``, ``non-free`` or ``non-free-firmware``
archive areas.

Bootstrapping a new architecture, or a new version of a package with
circular dependencies (such as a self-hosting compiler), will sometimes
also require an upload that includes binary packages.

Binary packages in the ``main`` archive area that were not built by
Debian's official build daemons will not usually be allowed to migrate
from ``unstable`` to ``testing``, so an upload that contains binary
packages built by the package's maintainer must usually be followed by
a source-only upload after the binary upload has been accepted.
This restriction does not apply to ``contrib``, ``non-free`` or
``non-free-firmware`` packages.

.. _upload-ftp-master:

Uploading to ``ftp-master``
--------------------------------------------------------------------------------------------------------------------------------

To upload a package, you should upload the files (including the signed
changes and dsc file) with anonymous ftp to ``ftp.upload.debian.org`` in
the directory
`/pub/UploadQueue/ <ftp://ftp.upload.debian.org/pub/UploadQueue/>`__. To
get the files processed there, they need to be signed with a key in the
Debian Developers keyring or the Debian Maintainers keyring (see
https://wiki.debian.org/DebianMaintainer\ ).

Please note that you should transfer the changes file last. Otherwise,
your upload may be rejected because the archive maintenance software
will parse the changes file and see that not all files have been
uploaded.

You may also find the Debian packages :ref:`dupload` or
:ref:`dput` useful when uploading packages.These handy programs
help automate the process of uploading packages into Debian.

For removing packages or cancelling an upload, please see
ftp://ftp.upload.debian.org/pub/UploadQueue/README\  and the Debian
package :ref:`dcut`.

Finally, you should think about the status of your package with relation
to ``testing`` before uploading to ``unstable``. If you have a version
in ``unstable`` waiting to migrate then it is generally a good idea
to let it migrate before uploading another new version. You should
also check the :ref:`pkg-tracker` for transition warnings to avoid
making uploads that disrupt ongoing transitions.

.. _delayed-incoming:

Delayed uploads
--------------------------------------------------------------------------------------------------------------------------------

It is sometimes useful to upload a package immediately, but to want this
package to arrive in the archive only a few days later. For example,
when preparing a :ref:`nmu`, you might want to
give the maintainer a few days to react.

An upload to the delayed directory keeps the package in `the deferred
uploads queue <https://ftp-master.debian.org/deferred.html>`__. When the
specified waiting time is over, the package is moved into the regular
incoming directory for processing. This is done through automatic
uploading to ``ftp.upload.debian.org`` in upload-directory
``DELAYED/``\ *X*\ ``-day`` (*X* between 0 and 15). 0-day is uploaded
multiple times per day to ``ftp.upload.debian.org``.

With dput, you can use the ``--delayed`` *DELAY* parameter to put the
package into one of the queues.

.. _s5.6.4:

Security uploads
--------------------------------------------------------------------------------------------------------------------------------

Do **NOT** upload a package to the security upload queue (on
``*.security.upload.debian.org``) without prior authorization from the
security team. If the package does not exactly meet the team's
requirements, it will cause many problems and delays in dealing with the
unwanted upload. For details, please see :ref:`bug-security`.

.. _s5.6.5:

Other upload queues
--------------------------------------------------------------------------------------------------------------------------------

There is an alternative upload queue in Europe at
ftp://ftp.eu.upload.debian.org/pub/UploadQueue/\ . It operates in
the same way as ``ftp.upload.debian.org``, but should be faster for
European developers.

Packages can also be uploaded via ssh to ``ssh.upload.debian.org``;
files should be put ``/srv/upload.debian.org/UploadQueue``. This queue
does not support :ref:`delayed-incoming`.

.. _upload-notification:

Notifications
--------------------------------------------------------------------------------------------------------------------------------

The Debian archive maintainers are responsible for handling package
uploads. For the most part, uploads are automatically handled on a daily
basis by the archive maintenance tools, ``dak process-upload``.
Specifically, updates to existing packages to the ``unstable``
distribution are handled automatically. In other cases, notably new
packages, placing the uploaded package into the distribution is handled
manually. When uploads are handled manually, the change to the archive
may take some time to occur. Please be patient.

In any case, you will receive an email notification indicating that the
package has been added to the archive, which also indicates which bugs
will be closed by the upload. Please examine this notification
carefully, checking if any bugs you meant to close didn't get triggered.

The installation notification also includes information on what section
the package was inserted into. If there is a disparity, you will receive
a separate email notifying you of that. Read on below.

Note that if you upload via queues, the queue daemon software will also
send you a notification by email.

Also note that new uploads are announced on the :ref:`irc-channels`
channel ``#debian-devel-changes``. If your upload fails silently, it
could be that your package is improperly signed, in which case you can
find more explanations on
``ssh.upload.debian.org:/srv/upload.debian.org/queued/run/log``.

.. _override-file:

Specifying the package section, subsection and priority
================================================================================================================================

The ``debian/control`` file's ``Section`` and ``Priority`` fields do not
actually specify where the file will be placed in the archive, nor its
priority. In order to retain the overall integrity of the archive, it is
the archive maintainers who have control over these fields. The values
in the ``debian/control`` file are actually just hints.

The archive maintainers keep track of the canonical sections and
priorities for packages in the ``override file``. If there is a
disparity between the ``override file`` and the package's fields as
indicated in ``debian/control``, then you will receive an email noting
the divergence when the package is installed into the archive. You can
either correct your ``debian/control`` file for your next upload, or
else you may wish to make a change in the ``override
file``.

To alter the actual section that a package is put in, you need to first
make sure that the ``debian/control`` file in your package is accurate.
Next, submit a bug against ``ftp.debian.org`` requesting that the
section or priority for your package be changed from the old section or
priority to the new one. Use a Subject like
``override: PACKAGE1:section/priority, [...],
PACKAGEX:section/priority``, and include the justification for the
change in the body of the bug report.

For more information about ``override files``, see dpkg-scanpackages 1
and https://www.debian.org/Bugs/Developer#maintincorrect\ .

Note that the ``Section`` field describes both the section as well as
the subsection, which are described in :ref:`archive-sections`. If
the section is main, it should be omitted. The list of allowable
subsections can be found in
https://www.debian.org/doc/debian-policy/ch-archive.html#s-subsections\ .

.. _bug-handling:

Handling bugs
================================================================================================================================

Every developer has to be able to work with the Debian `bug tracking
system <https://www.debian.org/Bugs/>`__. This includes knowing how to
file bug reports properly (see :ref:`submit-bug`), how to update
them and reorder them, and how to process and close them.

The bug tracking system's features are described in the `BTS
documentation for developers <https://www.debian.org/Bugs/Developer>`__.
This includes closing bugs, sending followup messages, assigning
severities and tags, marking bugs as forwarded, and other issues.

Operations such as reassigning bugs to other packages, merging separate
bug reports about the same issue, or reopening bugs when they are
prematurely closed, are handled using the so-called control mail server.
All of the commands available on this server are described in the `BTS
control server
documentation <https://www.debian.org/Bugs/server-control>`__.

.. _bug-monitoring:

Monitoring bugs
--------------------------------------------------------------------------------------------------------------------------------

If you want to be a good maintainer, you should periodically check the
`Debian bug tracking system (BTS) <https://www.debian.org/Bugs/>`__ for
your packages. The BTS contains all the open bugs against your packages.
You can check them by browsing this page:
``https://bugs.debian.org/``\ *yourlogin*\ ``@debian.org``.

Maintainers interact with the BTS via email addresses at
``bugs.debian.org``. Documentation on available commands can be found at
https://www.debian.org/Bugs/\ , or, if you have installed the
``doc-debian`` package, you can look at the local files
``/usr/share/doc/debian/bug-*``.

Some find it useful to get periodic reports on open bugs. You can add a
cron job such as the following if you want to get a weekly email
outlining all the open bugs against your packages:

::

   # ask for weekly reports of bugs in my packages
   0 17 * * fri   echo "index maint address" | mail request@bugs.debian.org

Replace *address* with your official Debian maintainer address.

.. _bug-answering:

Responding to bugs
--------------------------------------------------------------------------------------------------------------------------------

When responding to bugs, make sure that any discussion you have about
bugs is sent to the original submitter of the bug, the bug itself and
(if you are not the maintainer of the package) the maintainer. Sending
an email to *123*\ ``@bugs.debian.org`` will send the mail to the
maintainer of the package and record your email with the bug log. If you
don't remember the submitter email address, you can use
*123*\ ``-submitter@bugs.debian.org`` to also contact the submitter of
the bug. The latter address also records the email with the bug log, so
if you are the maintainer of the package in question, it is enough to
send the reply to *123*\ ``-submitter@bugs.debian.org``. Otherwise you
should include *123*\ ``@bugs.debian.org`` so that you also reach the
package maintainer.

If you get a bug which mentions FTBFS, this means Fails to build from
source. Porters frequently use this acronym.

Once you've dealt with a bug report (e.g. fixed it), mark it as ``done``
(close it) by sending an explanation message to
*123*\ ``-done@bugs.debian.org``. If you're fixing a bug by changing and
uploading the package, you can automate bug closing as described in
:ref:`upload-bugfix`.

You should *never* close bugs via the bug server ``close`` command sent
to ``control@bugs.debian.org``. If you do so, the original submitter
will not receive any information about why the bug was closed.

Bug housekeeping
--------------------------------------------------------------------------------------------------------------------------------

As a package maintainer, you will often find bugs in other packages or
have bugs reported against your packages which are actually bugs in
other packages. The bug tracking system's features are described in the
`BTS documentation for Debian
developers <https://www.debian.org/Bugs/Developer>`__. Operations such
as reassigning, merging, and tagging bug reports are described in the
`BTS control server
documentation <https://www.debian.org/Bugs/server-control>`__. This
section contains some guidelines for managing your own bugs, based on
the collective Debian developer experience.

Filing bugs for problems that you find in other packages is one of the
civic obligations of maintainership, see :ref:`submit-bug` for
details. However, handling the bugs in your own packages is even more
important.

Here's a list of steps that you may follow to handle a bug report:

1. Decide whether the report corresponds to a real bug or not. Sometimes
   users are just calling a program in the wrong way because they
   haven't read the documentation. If you diagnose this, just close the
   bug with enough information to let the user correct their problem
   (give pointers to the good documentation and so on). If the same
   report comes up again and again you may ask yourself if the
   documentation is good enough or if the program shouldn't detect its
   misuse in order to give an informative error message. This is an
   issue that may need to be brought up with the upstream author.

   If the bug submitter disagrees with your decision to close the bug,
   they may reopen it until you find an agreement on how to handle it.
   If you don't find any, you may want to tag the bug ``wontfix`` to let
   people know that the bug exists but that it won't be corrected.
   Please make sure that the bug submitter understands the reasons for
   your decision by adding an explanation to the message that adds the
   ``wontfix`` tag.

   If this situation is unacceptable, you (or the submitter) may want to
   require a decision of the technical committee by filing a new bug
   against the ``tech-ctte`` pseudo-package with a summary of the situation.
   Before doing so, please read the `recommended procedure
   <https://www.debian.org/devel/tech-ctte>`__.

2. If the bug is real but it's caused by another package, just reassign
   the bug to the right package. If you don't know which package it
   should be reassigned to, you should ask for help on
   :ref:`irc-channels` or on ``debian-devel@lists.debian.org``.
   Please inform the maintainer(s) of the package you reassign the bug
   to, for example by Cc:ing the message that does the reassign to
   *packagename*\ ``@packages.debian.org`` and explaining your reasons
   in that mail. Please note that a simple reassignment is *not*
   e-mailed to the maintainers of the package being reassigned to, so
   they won't know about it until they look at a bug overview for their
   packages.

   If the bug affects the operation of your package, please consider
   cloning the bug and reassigning the clone to the package that really
   causes the behavior. Otherwise, the bug will not be shown in your
   package's bug list, possibly causing users to report the same bug
   over and over again. You should block "your" bug with the reassigned,
   cloned bug to document the relationship.

3. Sometimes you also have to adjust the severity of the bug so that it
   matches our definition of the severity. That's because people tend to
   inflate the severity of bugs to make sure their bugs are fixed
   quickly. Some bugs may even be dropped to wishlist severity when the
   requested change is just cosmetic.

4. If the bug is real but the same problem has already been reported by
   someone else, then the two relevant bug reports should be merged into
   one using the merge command of the BTS. In this way, when the bug is
   fixed, all of the submitters will be informed of this. (Note,
   however, that emails sent to one bug report's submitter won't
   automatically be sent to the other report's submitter.) For more
   details on the technicalities of the merge command and its relative,
   the unmerge command, see the BTS control server documentation.

5. The bug submitter may have forgotten to provide some information, in
   which case you have to ask them for the required information. You may
   use the ``moreinfo`` tag to mark the bug as such. Moreover if you
   can't reproduce the bug, you tag it ``unreproducible``. Anyone who
   can reproduce the bug is then invited to provide more information on
   how to reproduce it. After a few months, if this information has not
   been sent by someone, the bug may be closed.

6. If the bug is related to the packaging, you just fix it. If you are
   not able to fix it yourself, then tag the bug as ``help``. You can
   also ask for help on ``debian-devel@lists.debian.org`` or
   ``debian-qa@lists.debian.org``. If it's an upstream problem, you have
   to forward it to the upstream author. Forwarding a bug is not enough,
   you have to check at each release if the bug has been fixed or not.
   If it has, you just close it, otherwise you have to remind the author
   about it. If you have the required skills you can prepare a patch
   that fixes the bug and send it to the author at the same time. Make
   sure to send the patch to the BTS and to tag the bug as ``patch``.

7. If you have fixed a bug in your local copy, or if a fix has been
   committed to the VCS repository, you may tag the bug as ``pending``
   to let people know that the bug is corrected and that it will be
   closed with the next upload (add the ``closes:`` in the
   ``changelog``). This is particularly useful if you are several
   developers working on the same package.

8. Once a corrected package is available in the archive, the bug should
   be closed indicating the version in which it was fixed. This can be
   done automatically; read :ref:`upload-bugfix`.

.. _upload-bugfix:

When bugs are closed by new uploads
--------------------------------------------------------------------------------------------------------------------------------

As bugs and problems are fixed in your packages, it is your
responsibility as the package maintainer to close these bugs. However,
you should not close a bug until the package which fixes the bug has
been accepted into the Debian archive. Therefore, once you get
notification that your updated package has been installed into the
archive, you can and should close the bug in the BTS. Also, the bug
should be closed with the correct version.

However, it's possible to avoid having to manually close bugs after the
upload — just list the fixed bugs in your ``debian/changelog`` file,
following a certain syntax, and the archive maintenance software will
close the bugs for you. For example:

::

   acme-cannon (3.1415) unstable; urgency=low

     * Frobbed with options (closes: Bug#98339)
     * Added safety to prevent operator dismemberment, closes: bug#98765,
       bug#98713, #98714.
     * Added man page. Closes: #98725.

Technically speaking, the following Perl regular expression describes
how bug closing changelogs are identified:

::

     /closes:\s*(?:bug)?\#?\s?\d+(?:,\s*(?:bug)?\#?\s?\d+)*/ig

We prefer the ``closes: #``\ *XXX* syntax, as it is the most concise
entry and the easiest to integrate with the text of the ``changelog``.
Unless specified differently by the ``-v``-switch to
``dpkg-buildpackage``, only the bugs closed in the most recent changelog
entry are closed (basically, exactly the bugs mentioned in the
changelog-part in the ``.changes`` file are closed).

Historically, uploads identified as :ref:`nmu` were tagged ``fixed`` instead of being closed, but that
practice was ceased with the advent of version-tracking. The same
applied to the tag ``fixed-in-experimental``.

If you happen to mistype a bug number or forget a bug in the changelog
entries, don't hesitate to undo any damage the error caused. To reopen
wrongly closed bugs, send a ``reopen`` *XXX* command to the bug
tracking system's control address, ``control@bugs.debian.org``. To close
any remaining bugs that were fixed by your upload, email the
``.changes`` file to *XXX*\ ``-done@bugs.debian.org``, where *XXX* is
the bug number, and put Version: *YYY* and an empty line as the first
two lines of the body of the email, where *YYY* is the first version
where the bug has been fixed.

Bear in mind that it is not obligatory to close bugs using the changelog
as described above. If you simply want to close bugs that don't have
anything to do with an upload you made, do it by emailing an explanation
to *XXX*\ ``-done@bugs.debian.org``. Do **not** close bugs in the
changelog entry of a version if the changes in that version of the
package don't have any bearing on the bug.

For general information on how to write your changelog entries, see
:ref:`bpp-debian-changelog`.

.. _bug-security:

Handling security-related bugs
--------------------------------------------------------------------------------------------------------------------------------

Due to their sensitive nature, security-related bugs must be handled
carefully. The Debian Security Team exists to coordinate this activity,
keeping track of outstanding security problems, helping maintainers with
security problems or fixing them themselves, sending security
advisories, and maintaining ``security.debian.org``.

When you become aware of a security-related bug in a Debian package,
whether or not you are the maintainer, collect pertinent information
about the problem, and promptly contact the security team by emailing
``team@security.debian.org``. If desired, email can be encrypted with
the Debian Security Contact key, see
https://www.debian.org/security/faq#contact\  for details. **DO NOT
UPLOAD** any packages for ``stable`` without contacting the team. Useful
information includes, for example:

-  Whether or not the bug is already public.

-  Which versions of the package are known to be affected by the bug.
   Check each version that is present in a supported Debian release, as
   well as ``testing`` and ``unstable``.

-  The nature of the fix, if any is available (patches are especially
   helpful)

-  Any fixed packages that you have prepared yourself (send the
   resulting debdiff or alternatively only the ``.diff.gz`` and ``.dsc``
   files and read :ref:`bug-security-building` first)

-  Any assistance you can provide to help with testing (exploits,
   regression testing, etc.)

-  Any information needed for the advisory (see :ref:`bug-security-advisories`)

As the maintainer of the package, you have the responsibility to
maintain it, even in the stable release. You are in the best position to
evaluate patches and test updated packages, so please see the sections
below on how to prepare packages for the Security Team to handle.

.. _bug-security-tracker:

Debian Security Tracker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The security team maintains a central database, the `Debian Security
Tracker <https://security-tracker.debian.org/>`__. This contains all
public information that is known about security issues: which packages
and versions are affected or fixed, and thus whether stable, testing
and/or unstable are vulnerable. Information that is still confidential
is not added to the tracker.

You can search it for a specific issue, but also on package name. Look
for your package to see which issues are still open. If you can, please
provide more information about those issues, or help to address them in
your package. Instructions are on the tracker web pages.

.. _bug-security-confidentiality:

Confidentiality
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Unlike most other activities within Debian, information about security
issues must sometimes be kept private for a time. This allows software
distributors to coordinate their disclosure in order to minimize their
users' exposure. Whether this is the case depends on the nature of the
problem and corresponding fix, and whether it is already a matter of
public knowledge.

There are several ways developers can learn of a security problem:

-  they notice it on a public forum (mailing list, web site, etc.)

-  someone files a bug report

-  someone informs them via private email

In the first two cases, the information is public and it is important to
have a fix as soon as possible. In the last case, however, it might not
be public information. In that case there are a few possible options for
dealing with the problem:

-  If the security exposure is minor, there is sometimes no need to keep
   the problem a secret and a fix should be made and released.

-  If the problem is severe, it is preferable to share the information
   with other vendors and coordinate a release. The security team keeps
   in contact with the various organizations and individuals and can
   take care of that.

In all cases if the person who reports the problem asks that it not be
disclosed, such requests should be honored, with the obvious exception
of informing the security team in order that a fix may be produced for a
stable release of Debian. When sending confidential information to the
security team, be sure to mention this fact.

Please note that if secrecy is needed you may not upload a fix to
``unstable`` (or anywhere else, such as a public VCS repository). It is
not sufficient to obfuscate the details of the change, as the code
itself is public, and can (and will) be examined by the general public.

There are two reasons for releasing information even though secrecy is
requested: the problem has been known for a while, or the problem or
exploit has become public.

The Security Team has a PGP-key to enable encrypted communication about
sensitive issues. See the `Security Team
FAQ <https://www.debian.org/security/faq#contact>`__ for details.

.. _bug-security-advisories:

Security Advisories
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Security advisories are only issued for the current, released stable
distribution, and *not* for ``testing`` or ``unstable``. When released,
advisories are sent to the ``debian-security-announce@lists.debian.org``
mailing list and posted on `the security web
page <https://www.debian.org/security/>`__. Security advisories are
written and posted by the security team. However they certainly do not
mind if a maintainer can supply some of the information for them, or
write part of the text. Information that should be in an advisory
includes:

-  A description of the problem and its scope, including:

   -  The type of problem (privilege escalation, denial of service,
      etc.)

   -  What privileges may be gained, and by whom (if any)

   -  How it can be exploited

   -  Whether it is remotely or locally exploitable

   -  How the problem was fixed

   This information allows users to assess the threat to their systems.

-  Version numbers of affected packages

-  Version numbers of fixed packages

-  Information on where to obtain the updated packages (usually from the
   Debian security archive)

-  References to upstream advisories, `CVE <https://cve.mitre.org>`__
   identifiers, and any other information useful in cross-referencing
   the vulnerability

.. _bug-security-building:

Preparing packages to address security issues
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One way that you can assist the security team in their duties is to
provide them with fixed packages suitable for a security advisory for
the stable Debian release.

When an update is made to the stable release, care must be taken to
avoid changing system behavior or introducing new bugs. In order to do
this, make as few changes as possible to fix the bug. Users and
administrators rely on the exact behavior of a release once it is made,
so any change that is made might break someone's system. This is
especially true of libraries: make sure you never change the API
(Application Program Interface) or ABI (Application Binary Interface),
no matter how small the change.

This means that moving to a new upstream version is not a good solution.
Instead, the relevant changes should be back-ported to the version
present in the current stable Debian release. Generally, upstream
maintainers are willing to help if needed. If not, the Debian security
team may be able to help.

In some cases, it is not possible to back-port a security fix, for
example when large amounts of source code need to be modified or
rewritten. If this happens, it may be necessary to move to a new
upstream version. However, this is only done in extreme situations, and
you must always coordinate that with the security team beforehand.

Related to this is another important guideline: always test your
changes. If you have an exploit available, try it and see if it indeed
succeeds on the unpatched package and fails on the fixed package. Test
other, normal actions as well, as sometimes a security fix can break
seemingly unrelated features in subtle ways.

Do **NOT** include any changes in your package which are not directly
related to fixing the vulnerability. These will only need to be
reverted, and this wastes time. If there are other bugs in your package
that you would like to fix, make an upload to proposed-updates in the
usual way, after the security advisory is issued. The security update
mechanism is not a means for introducing changes to your package which
would otherwise be rejected from the stable release, so please do not
attempt to do this.

Review and test your changes as much as possible. Check the differences
from the previous version repeatedly (``interdiff`` from the
``patchutils`` package and ``debdiff`` from ``devscripts`` are useful
tools for this, see :ref:`debdiff`).

Be sure to verify the following items:

-  **Target the right distribution** in your ``debian/changelog``:
   |codename-security| (e.g. |codename-stable-security|).
   Do not target *distribution*\ ``-proposed-updates`` or ``stable``!

-  Make descriptive, meaningful changelog entries. Others will rely on
   them to determine whether a particular bug was fixed. Add ``closes:``
   statements for any **Debian bugs** filed. Always include an external
   reference, preferably a **CVE identifier**, so that it can be
   cross-referenced. However, if a CVE identifier has not yet been
   assigned, do not wait for it but continue the process. The identifier
   can be cross-referenced later.

-  Make sure the **version number** is proper. It must be greater than
   the current package, but less than package versions in later
   distributions. If in doubt, test it with ``dpkg
   --compare-versions``. Be careful not to re-use a version number that
   you have already used for a previous upload, or one that conflicts
   with a binNMU. The convention is to append ``+deb``\ *X*\ ``u1``
   (where *X* is the major release number), e.g.
   |version1-revision-deb-version-stable-u1|, of course increasing 1 for
   any subsequent uploads.

-  Unless the upstream source has been uploaded to
   ``security.debian.org`` before (by a previous security update), build
   the upload **with full upstream source** (``dpkg-buildpackage -sa``).
   If there has been a previous upload to ``security.debian.org`` with
   the same upstream version, you may upload without upstream source
   (``dpkg-buildpackage
   -sd``).

-  Be sure to use the **exact same ``*.orig.tar.{gz,bz2,xz}``** as used
   in the normal archive, otherwise it is not possible to move the
   security fix into the main archives later.

-  Build the package on a **clean system** which only has packages
   installed from the distribution you are building for. If you do not
   have such a system yourself, you can use a debian.org machine (see
   :ref:`server-machines`) or setup a chroot (see
   :ref:`pbuilder` and :ref:`debootstrap`).

.. _bug-security-upload:

Uploading the fixed package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Do **NOT** upload a package to the security upload queue (on
``*.security.upload.debian.org``) without prior authorization from the
security team. If the package does not exactly meet the team's
requirements, it will cause many problems and delays in dealing with the
unwanted upload.

Do **NOT** upload your fix to ``proposed-updates`` without coordinating
with the security team. Packages from ``security.debian.org`` will be
copied into the ``proposed-updates`` directory automatically. If a
package with the same or a higher version number is already installed
into the archive, the security update will be rejected by the archive
system. That way, the stable distribution will end up without a security
update for this package instead.

Once you have created and tested the new package and it has been
approved by the security team, it needs to be uploaded so that it can be
installed in the archives. For security uploads, the place to upload to
is ``ftp://ftp.security.upload.debian.org/pub/SecurityUploadQueue/``.

Once an upload to the security queue has been accepted, the package will
automatically be built for all architectures and stored for verification
by the security team.

Uploads that are waiting for acceptance or verification are only
accessible by the security team. This is necessary since there might be
fixes for security problems that cannot be disclosed yet.

If a member of the security team accepts a package, it will be installed
on ``security.debian.org`` as well as proposed for the proper
*distribution*\ ``-proposed-updates`` on ``ftp-master.debian.org``.

.. _archive-manip:

Moving, removing, renaming, orphaning, adopting, and reintroducing packages
================================================================================================================================

Some archive manipulation operations are not automated in the Debian
upload process. These procedures should be manually followed by
maintainers. This chapter gives guidelines on what to do in these cases.

.. _moving-pkgs:

Moving packages
--------------------------------------------------------------------------------------------------------------------------------

Sometimes a package will change its section. For instance, a package
from the ``non-free`` section might be GPL'd in a later version, in
which case the package should be moved to ``main`` or ``contrib``. [1]_

If you need to change the section for one of your packages, change the
package control information to place the package in the desired section,
and re-upload the package (see the `Debian Policy
Manual <https://www.debian.org/doc/debian-policy/>`__ for details). You
must ensure that you include the ``.orig.tar.{gz,bz2,xz}`` in your
upload (even if you are not uploading a new upstream version), or it
will not appear in the new section together with the rest of the
package. If your new section is valid, it will be moved automatically.
If it does not, then contact the ftpmasters in order to understand what
happened.

If, on the other hand, you need to change the ``subsection`` of one of
your packages (e.g., ``devel``, ``admin``), the procedure is slightly
different. Correct the subsection as found in the control file of the
package, and re-upload that. Also, you'll need to get the override file
updated, as described in :ref:`override-file`.

.. _removing-pkgs:

Removing packages
--------------------------------------------------------------------------------------------------------------------------------

If for some reason you want to completely remove a package (say, if it
is an old compatibility library which is no longer required), you need
to file a bug against ``ftp.debian.org`` asking that the package be
removed; as with all bugs, this bug should normally have normal
severity. The bug title should be in the form
``RM:`` *package [architecture list]* ``--`` *reason*, where *package*
is the package to be removed and *reason* is a short summary of the
reason for the removal request. *[architecture list]* is optional and
only needed if the removal request only applies to some architectures,
not all. Note that the ``reportbug`` will create a title conforming to
these rules when you use it to report a bug against the
``ftp.debian.org`` pseudo-package.

If you want to remove a package you maintain, you should note this in
the bug title by prepending ``ROM`` (Request Of Maintainer). There are
several other standard acronyms used in the reasoning for a package
removal; see https://ftp-master.debian.org/removals.html\  for a
complete list. That page also provides a convenient overview of pending
removal requests.

Note that removals can only be done for the ``unstable``,
``experimental`` and ``stable`` distributions. Packages are not removed
from ``testing`` directly. Rather, they will be removed automatically
after the package has been removed from ``unstable`` and no package in
``testing`` depends on it. (Removals from ``testing`` are possible
though by filing a removal bug report against the ``release.debian.org``
pseudo-package. See :ref:`removals`.)

There is one exception when an explicit removal request is not
necessary: If a (source or binary) package is no longer built from
source, it will be removed semi-automatically. For a binary-package,
this means if there is no longer any source package producing this
binary package; if the binary package is just no longer produced on some
architectures, a removal request is still necessary. For a
source-package, this means that all binary packages it refers to have
been taken over by another source package.

In your removal request, you have to detail the reasons justifying the
request. This is to avoid unwanted removals and to keep a trace of why a
package has been removed. For example, you can provide the name of the
package that supersedes the one to be removed.

Usually you only ask for the removal of a package maintained by
yourself. If you want to remove another package, you have to get the
approval of its maintainer. Should the package be orphaned and thus have
no maintainer, you should first discuss the removal request on
``debian-qa@lists.debian.org``. If there is a consensus that the package
should be removed, you should reassign and retitle the ``O:`` bug filed
against the ``wnpp`` package instead of filing a new bug as removal
request.

Further information relating to these and other package removal related
topics may be found at https://wiki.debian.org/ftpmaster_Removals\
and https://qa.debian.org/howto-remove.html\ .

If in doubt concerning whether a package is disposable, email
``debian-devel@lists.debian.org`` asking for opinions. Also of interest
is the ``apt-cache`` program from the ``apt`` package. When invoked as
``apt-cache showpkg`` *package*, the program will show details for *package*,
including reverse depends. Other useful programs include
``apt-cache rdepends``, ``apt-rdepends``, ``build-rdeps`` (in the
``devscripts`` package) and ``grep-dctrl``. Removal of orphaned packages
is discussed on ``debian-qa@lists.debian.org``.

Once the package has been removed, the package's bugs should be handled.
They should either be reassigned to another package in the case where
the actual code has evolved into another package (e.g. ``libfoo12`` was
removed because ``libfoo13`` supersedes it) or closed if the software is
simply no longer part of Debian. When closing the bugs, to avoid marking
the bugs as fixed in versions of the packages in previous Debian
releases, they should be marked as fixed in the version
``<most-recent-version-ever-in-Debian>+rm``.

.. _s5.9.2.1:

Removing packages from ``Incoming``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the past, it was possible to remove packages from ``incoming``.
However, with the introduction of the new incoming system, this is no
longer possible. [4]_ Instead, you have to upload a new revision of your
package with a higher version than the package you want to replace. Both
versions will be installed in the archive but only the higher version
will actually be available in ``unstable`` since the previous version
will immediately be replaced by the higher. However, if you do proper
testing of your packages, the need to replace a package should not occur
too often anyway.

.. _s5.9.3:

Replacing or renaming packages
--------------------------------------------------------------------------------------------------------------------------------

When the upstream maintainers for one of your packages chose to rename
their software (or you made a mistake naming your package), you should
follow a two-step process to rename it. In the first step, change the
``debian/control`` file to reflect the new name and to replace, provide
and conflict with the obsolete package name (see the `Debian Policy
Manual <https://www.debian.org/doc/debian-policy/>`__ for details).
Please note that you should only add a ``Provides`` relation if all
packages depending on the obsolete package name continue to work after
the renaming. Once you've uploaded the package and the package has moved
into the archive, file a bug against ``ftp.debian.org`` asking to remove
the package with the obsolete name (see :ref:`removing-pkgs`). Do not forget to properly reassign the
package's bugs at the same time.

At other times, you may make a mistake in constructing your package and
wish to replace it. The only way to do this is to increase the version
number and upload a new version. The old version will be expired in the
usual manner. Note that this applies to each part of your package,
including the sources: if you wish to replace the upstream source
tarball of your package, you will need to upload it with a different
version. An easy possibility is to replace ``foo_1.00.orig.tar.gz`` with
``foo_1.00+0.orig.tar.gz`` or ``foo_1.00.orig.tar.bz2``. This
restriction gives each file on the ftp site a unique name, which helps
to ensure consistency across the mirror network.

.. _orphaning:

Orphaning a package
--------------------------------------------------------------------------------------------------------------------------------

If you can no longer maintain a package, you need to inform others, and
see that the package is marked as orphaned. You should set the package
maintainer to ``Debian QA Group <packages@qa.debian.org>`` and submit a
bug report against the pseudo package ``wnpp``. The bug report should be
titled ``O:`` *package* ``--`` *short description* indicating that
the package is now orphaned. The severity of the bug should be set to
``normal``; if the package has a priority of standard or higher, it
should be set to important. If you feel it's necessary, send a copy to
``debian-devel@lists.debian.org`` by putting the address in the
X-Debbugs-CC: header of the message (no, don't use CC:, because that way
the message's subject won't indicate the bug number).

If you just intend to give the package away, but you can keep
maintainership for the moment, then you should instead submit a bug
against ``wnpp`` and title it ``RFA:`` *package* ``--`` *short
description*. ``RFA`` stands for ``Request For Adoption``.

More information is on the `WNPP web
pages <https://www.debian.org/devel/wnpp/>`__.

.. _adopting:

Adopting a package
--------------------------------------------------------------------------------------------------------------------------------

A list of packages in need of a new maintainer is available in the
`Work-Needing and Prospective Packages list
(WNPP) <https://www.debian.org/devel/wnpp/>`__. If you wish to take over
maintenance of any of the packages listed in the WNPP, please take a
look at the aforementioned page for information and procedures.

It is not OK to simply take over a package without assent of the current
maintainer — that would be package hijacking. You can, of course,
contact the current maintainer and ask them for permission to take over
the package.

However, when a package has been neglected by the maintainer, you might
be able to take over package maintainership by following the package
salvaging process as described in :ref:`package-salvaging`. If you have reason to believe a
maintainer is no longer active at all, see :ref:`mia-qa`.

Complaints about maintainers should be brought up on the developers'
mailing list. If the discussion doesn't end with a positive conclusion,
and the issue is of a technical nature, consider bringing it to the
attention of the technical committee (see the `technical committee web
page <https://www.debian.org/devel/tech-ctte>`__ for more information).

If you take over an old package, you probably want to be listed as the
package's official maintainer in the bug system. This will happen
automatically once you upload a new version with an updated
``Maintainer`` field, although it can take a few hours after the upload
is done. If you do not expect to upload a new version for a while, you
can use :ref:`pkg-tracker` to get the bug reports. However, make
sure that the old maintainer has no problem with the fact that they will
continue to receive the bugs during that time.

.. _reintroducing-pkgs:

Reintroducing packages
--------------------------------------------------------------------------------------------------------------------------------

Packages are often removed due to release-critical bugs, absent
maintainers, too few users or poor quality in general. While the process
of reintroduction is similar to the initial packaging process, you can
avoid some pitfalls by doing some historical research first.

You should check why the package was removed in the first place. This
information can be found in the removal item in the news section of the
PTS page for the package or by browsing the log of
`removals <https://ftp-master.debian.org/#removed>`__. The removal bug
will tell you why the package was removed and will give some indication
of what you will need to work on in order to reintroduce the package. It
may indicate that the best way forward is to switch to some other piece
of software instead of reintroducing the package.

It may be appropriate to contact the former maintainers to find out if
they are working on reintroducing the package, interested in
co-maintaining the package or interested in sponsoring the package if
needed.

You should do all the things required before introducing new packages
(:ref:`newpackage`).

You should base your work on the latest packaging available that is
suitable. That might be the latest version from ``unstable``, which will
still be present in the `snapshot
archive <https://snapshot.debian.org/>`__.

The version control system used by the previous maintainer might contain
useful changes, so it might be a good idea to have a look there. Check
if the ``control`` file of the previous package contained any headers
linking to the version control system for the package and if it still
exists.

Package removals from ``unstable`` (not ``testing``, ``stable`` or
``oldstable``) trigger the closing of all bugs related to the package.
You should look through all the closed bugs (including archived bugs)
and unarchive and reopen any that were closed in a version ending in
``+rm`` and still apply. Any that no longer apply should be marked as
fixed in the correct version if that is known.

Package removals from unstable also trigger marking the package as
removed in the :ref:`bug-security-tracker`. Debian members should
`mark removed issues as unfixed <https://security-team.debian.org/security_tracker.html#removed-packages>`__
in the security tracker repository and all others should contact the
security team to `report reintroduced
packages <https://security-tracker.debian.org/tracker/data/report>`__.

.. _porting:

Porting and being ported
================================================================================================================================

Debian supports an ever-increasing number of architectures. Even if you
are not a porter, and you don't use any architecture but one, it is part
of your duty as a maintainer to be aware of issues of portability.
Therefore, even if you are not a porter, you should read most of this
chapter.

Porting is the act of building Debian packages for architectures that
are different from the original architecture of the package maintainer's
binary package. It is a unique and essential activity. In fact, porters
do most of the actual compiling of Debian packages. For instance, when a
maintainer uploads a (portable) source package with binaries for the
``i386`` architecture, it will be built for each of the other
architectures, amounting to |number-of-arches| more builds.

.. _kind-to-porters:

Being kind to porters
--------------------------------------------------------------------------------------------------------------------------------

Porters have a difficult and unique task, since they are required to
deal with a large volume of packages. Ideally, every source package
should build right out of the box. Unfortunately, this is often not the
case. This section contains a checklist of *gotchas* often committed by
Debian maintainers — common problems which often stymie porters, and
make their jobs unnecessarily difficult.

The first and most important thing is to respond quickly to bugs or
issues raised by porters. Please treat porters with courtesy, as if they
were in fact co-maintainers of your package (which, in a way, they are).
Please be tolerant of succinct or even unclear bug reports; do your best
to hunt down whatever the problem is.

By far, most of the problems encountered by porters are caused by
*packaging bugs* in the source packages. Here is a checklist of things
you should check or be aware of.

1. Make sure that your ``Build-Depends`` and ``Build-Depends-Indep``
   settings in ``debian/control`` are set properly. The best way to
   validate this is to use the ``debootstrap`` package to create an
   ``unstable`` chroot environment (see :ref:`debootstrap`). Within
   that chrooted environment, install the ``build-essential`` package
   and any package dependencies mentioned in ``Build-Depends`` and/or
   ``Build-Depends-Indep``. Finally, try building your package within
   that chrooted environment. These steps can be automated by the use of
   the ``pbuilder`` program, which is provided by the package of the
   same name (see :ref:`pbuilder`).

   If you can't set up a proper chroot, ``dpkg-depcheck`` may be of
   assistance (see :ref:`dpkg-depcheck`).

   See the `Debian Policy
   Manual <https://www.debian.org/doc/debian-policy/>`__ for
   instructions on setting build dependencies.

2. Don't set architecture to a value other than ``all`` or ``any``
   unless you really mean it. In too many cases, maintainers don't
   follow the instructions in the `Debian Policy
   Manual <https://www.debian.org/doc/debian-policy/>`__. Setting your
   architecture to only one architecture (such as ``i386`` or ``amd64``)
   is usually incorrect.

3. Make sure your source package is correct. Do
   ``dpkg-source -x`` *package*\ ``.dsc`` to make sure your source
   package unpacks properly. Then, in there, try building your package
   from scratch with ``dpkg-buildpackage``.

4. Make sure you don't ship your source package with the
   ``debian/files`` or ``debian/substvars`` files. They should be
   removed by the ``clean`` target of ``debian/rules``.

5. Make sure you don't rely on locally installed or hacked
   configurations or programs. For instance, you should never be calling
   programs in ``/usr/local/bin`` or the like. Try not to rely on
   programs being set up in a special way. Try building your package on
   another machine, even if it's the same architecture.

6. Don't depend on the package you're building being installed already
   (a sub-case of the above issue). There are, of course, exceptions to
   this rule, but be aware that any case like this needs manual
   bootstrapping and cannot be done by automated package builders.

7. Don't rely on the compiler being a certain version, if possible. If
   not, then make sure your build dependencies reflect the restrictions,
   although you are probably asking for trouble, since different
   architectures sometimes standardize on different compilers.

8. Make sure your ``debian/rules`` contains separate ``binary-arch`` and
   ``binary-indep`` targets, as the Debian Policy Manual requires. Make
   sure that both targets work independently, that is, that you can call
   the target without having called the other before. To test this, try
   to run ``dpkg-buildpackage -B``.

9. When you can't support your package on a particular architecture, you
   shouldn't use the Architecture field to reflect that (it's also a pain to
   maintain correctly). If the package fails to build from source, you can just
   let it be and interested people can take a look at the build logs. If the
   package would actually build, the trick is to add a ``Build-Depends`` on
   ``unsupported-architecture [!the-not-supported-arch]``. The buildds will not
   build the package as the build dependencies are not fullfiled on that
   arch. To prevent building on 32-bits architectures, the
   ``architecture-is-64bit`` build dependency can be used, as
   ``architecture-is-little-endian`` can be used to prevent building on big
   endian systems.

.. _porter-guidelines:

Guidelines for porter uploads
--------------------------------------------------------------------------------------------------------------------------------

If the package builds out of the box for the architecture to be ported
to, you are in luck and your job is easy. This section applies to that
case; it describes how to build and upload your binary package so that
it is properly installed into the archive. If you do have to patch the
package in order to get it to compile for the other architecture, you
are actually doing a source NMU, so consult :ref:`nmu-guidelines` instead.

For a porter upload, no changes are being made to the source. You do not
need to touch any of the files in the source package. This includes
``debian/changelog``.

The way to invoke ``dpkg-buildpackage`` is as ``dpkg-buildpackage -B
-m`` *porter-email*. Of course, set *porter-email* to your email
address. This will do a binary-only build of only the
architecture-dependent portions of the package, using the
``binary-arch`` target in ``debian/rules``.

If you are working on a Debian machine for your porting efforts and you
need to sign your upload locally for its acceptance in the archive, you
can run ``debsign`` on your ``.changes`` file to have it signed
conveniently, or use the remote signing mode of ``dpkg-sig``.

.. _binary-only-nmu:

Recompilation or binary-only NMU
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sometimes the initial porter upload is problematic because the
environment in which the package was built was not good enough (outdated
or obsolete library, bad compiler, etc.). Then you may just need to
recompile it in an updated environment. However, you have to bump the
version number in this case, so that the old bad package can be replaced
in the Debian archive (``dak`` refuses to install new packages if they
don't have a version number greater than the currently available one).

You have to make sure that your binary-only NMU doesn't render the
package uninstallable. This could happen when a source package generates
arch-dependent and arch-independent packages that have
inter-dependencies generated using dpkg's substitution variable
``$(Source-Version)``.

Despite the required modification of the changelog, these are called
binary-only NMUs — there is no need in this case to trigger all other
architectures to consider themselves out of date or requiring
recompilation.

Such recompilations require special *magic* version numbering, so that
the archive maintenance tools recognize that, even though there is a new
Debian version, there is no corresponding source update. If you get this
wrong, the archive maintainers will reject your upload (due to lack of
corresponding source code).

The *magic* for a recompilation-only NMU is triggered by using a suffix
appended to the package version number, following the form
``b``\ *number*. For instance, if the latest version you are recompiling
against was version ``2.9-3``, your binary-only NMU should carry a
version of ``2.9-3+b1``. If the latest version was ``3.4+b1`` (i.e, a
native package with a previous recompilation NMU), your binary-only NMU
should have a version number of ``3.4+b2``. [2]_

Similar to initial porter uploads, the correct way of invoking
``dpkg-buildpackage`` is ``dpkg-buildpackage -B`` to only build the
architecture-dependent parts of the package.

.. _source-nmu-when-porter:

When to do a source NMU if you are a porter
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Porters doing a source NMU generally follow the guidelines found in
:ref:`nmu`, just like non-porters.
However, it is expected that the wait cycle for a porter's source NMU is
smaller than for a non-porter, since porters have to cope with a large
quantity of packages. Again, the situation varies depending on the
distribution they are uploading to. It also varies whether the
architecture is a candidate for inclusion into the next stable release;
the release managers decide and announce which architectures are
candidates.

If you are a porter doing an NMU for ``unstable``, the above guidelines
for porting should be followed, with two variations. Firstly, the
acceptable waiting period — the time between when the bug is submitted
to the BTS and when it is OK to do an NMU — is seven days for porters
working on the ``unstable`` distribution. This period can be shortened
if the problem is critical and imposes hardship on the porting effort,
at the discretion of the porter group. (Remember, none of this is
Policy, just mutually agreed upon guidelines.) For uploads to ``stable``
or ``testing``, please coordinate with the appropriate release team
first.

Secondly, porters doing source NMUs should make sure that the bug they
submit to the BTS should be of severity ``serious`` or greater. This
ensures that a single source package can be used to compile every
supported Debian architecture by release time. It is very important that
we have one version of the binary and source package for all
architectures in order to comply with many licenses.

Porters should try to avoid patches which simply kludge around bugs in
the current version of the compile environment, kernel, or libc.
Sometimes such kludges can't be helped. If you have to kludge around
compiler bugs and the like, make sure you ``#ifdef`` your work properly;
also, document your kludge so that people know to remove it once the
external problems have been fixed.

Porters may also have an unofficial location where they can put the
results of their work during the waiting period. This helps others
running the port have the benefit of the porter's work, even during the
waiting period. Of course, such locations have no official blessing or
status, so buyer beware.

.. _porter-automation:

Porting infrastructure and automation
--------------------------------------------------------------------------------------------------------------------------------

There is infrastructure and several tools to help automate package
porting. This section contains a brief overview of this automation and
porting to these tools; see the package documentation or references for
full information.

.. _s5.10.3.1:

Mailing lists and web pages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Web pages containing the status of each port can be found at
https://www.debian.org/ports/\ .

Each port of Debian has a mailing list. The list of porting mailing
lists can be found at https://lists.debian.org/ports.html\ . These
lists are used to coordinate porters, and to connect the users of a
given port with the porters.

.. _s5.10.3.2:

Porter tools
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Descriptions of several porting tools can be found in
:ref:`tools-porting`.

.. _wanna-build:

``wanna-build``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The ``wanna-build`` system is used as a distributed, client-server build
distribution system. It is usually used in conjunction with build
daemons running the ``buildd`` program. ``Build daemons`` are ``slave``
hosts, which contact the central ``wanna-build`` system to receive a
list of packages that need to be built.

``wanna-build`` is not yet available as a package; however, all Debian
porting efforts are using it for automated package building. The tool
used to do the actual package builds, ``sbuild``, is available as a
package; see its description in :ref:`sbuild`. Please note that the
packaged version is not the same as the one used on build daemons, but
it is close enough to reproduce problems.

Most of the data produced by ``wanna-build`` that is generally useful to
porters is available on the web at https://buildd.debian.org/\ .
This data includes nightly updated statistics, queueing information and
logs for build attempts.

We are quite proud of this system, since it has so many possible uses.
Independent development groups can use the system for different
sub-flavors of Debian, which may or may not really be of general
interest (for instance, a flavor of Debian built with ``gcc`` bounds
checking). It will also enable Debian to recompile entire distributions
quickly.

The wanna-build team, in charge of the buildds, can be reached at
``debian-wb-team@lists.debian.org``. To determine who (wanna-build team,
release team) and how (mail, BTS) to contact, refer to
https://lists.debian.org/debian-project/2009/03/msg00096.html\ .

When requesting binNMUs or give-backs (retries after a failed build),
please use the format described at
https://release.debian.org/wanna-build.txt\ .

.. _packages-arch-specific:

When your package is *not* portable
--------------------------------------------------------------------------------------------------------------------------------

Some packages still have issues with building and/or working on some of
the architectures supported by Debian, and cannot be ported at all, or
not within a reasonable amount of time. An example is a package that is
SVGA-specific (only available for ``i386`` and ``amd64``), or uses other
hardware-specific features not supported on all architectures.

In order to prevent broken packages from being uploaded to the archive,
and wasting buildd time, you need to do a few things:

-  First, make sure your package *does* fail to build on architectures
   that it cannot support. There are a few ways to achieve this. The
   preferred way is to have a small testsuite during build time that
   will test the functionality, and fail if it doesn't work. This is a
   good idea anyway, as this will prevent (some) broken uploads on all
   architectures, and also will allow the package to build as soon as
   the required functionality is available.

   Additionally, if you believe the list of supported architectures is
   pretty constant, you should change ``any`` to a list of supported
   architectures in ``debian/control``. This way, the build will fail
   also, and indicate this to a human reader without actually trying.

-  In order to prevent autobuilders from needlessly trying to build your
   package, it must be included in ``Packages-arch-specific``, a list
   used by the ``wanna-build`` script. The current version is available
   as https://wiki.debian.org/PackagesArchSpecific\ ; please see
   the top of the file for whom to contact for changes.

Please note that it is insufficient to only add your package to
``Packages-arch-specific`` without making it fail to build on
unsupported architectures: A porter or any other person trying to build
your package might accidentally upload it without noticing it doesn't
work. If in the past some binary packages were uploaded on unsupported
architectures, request their removal by filing a bug against
``ftp.debian.org``.

.. _non-free-buildd:

Marking non-free packages as auto-buildable
--------------------------------------------------------------------------------------------------------------------------------

By default packages from the ``non-free`` and ``non-free-firmware``
sections are not built by the autobuilder network (mostly because
the license of the packages could disapprove). To enable a package
to be built, you need to perform the following steps:

1. Check whether it is legally allowed and technically possible to
   auto-build the package;

2. Add ``XS-Autobuild: yes`` into the header part of ``debian/control``;

3. Send an email to ``non-free@buildd.debian.org`` and explain why the
   package can legitimately and technically be auto-built.

.. _nmu:

Non-Maintainer Uploads (NMUs)
================================================================================================================================

Every package has one or more maintainers. Normally, these are the
people who work on and upload new versions of the package. In some
situations, it is useful that other developers can upload a new version
as well, for example if they want to fix a bug in a package they don't
maintain, when the maintainer needs help to respond to issues. Such
uploads are called *Non-Maintainer Uploads (NMU)*.

.. _nmu-guidelines:

When and how to do an NMU
--------------------------------------------------------------------------------------------------------------------------------

Before doing an NMU, consider the following questions:

-  Have you geared the NMU towards helping the maintainer? As there
   might be disagreement on the notion of whether the maintainer
   actually needs help or not, the DELAYED queue exists to give time to
   the maintainer to react and has the beneficial side-effect of
   allowing for independent reviews of the NMU diff.

-  Does your NMU really fix bugs? ("Bugs" means any kind of bugs, e.g.
   wishlist bugs for packaging a new upstream version, but care should
   be taken to minimize the impact to the maintainer.) Fixing cosmetic
   issues or changing the packaging style in NMUs is discouraged.

-  Did you give enough time to the maintainer? When was the bug reported
   to the BTS? Being busy for a week or two isn't unusual. Is the bug so
   severe that it needs to be fixed right now, or can it wait a few more
   days?

-  How confident are you about your changes? Please remember the
   Hippocratic Oath: "Above all, do no harm." It is better to leave a
   package with an open grave bug than applying a non-functional patch,
   or one that hides the bug instead of resolving it. If you are not
   100% sure of what you did, it might be a good idea to seek advice
   from others. Remember that if you break something in your NMU, many
   people will be very unhappy about it.

-  Have you clearly expressed your intention to NMU, at least in the
   BTS? If that didn't generate any feedback, it might also be a good
   idea to try to contact the maintainer by other means (email to the
   maintainer addresses or private email, IRC).

-  If the maintainer is usually active and responsive, have you tried to
   contact them? In general it should be considered preferable that
   maintainers take care of an issue themselves and that they are given
   the chance to review and correct your patch, because they can be
   expected to be more aware of potential issues which an NMUer might
   miss. It is often a better use of everyone's time if the maintainer
   is given an opportunity to upload a fix on their own.

When doing an NMU, you must first make sure that your intention to NMU
is clear. Then, you must send a patch with the differences between the
current package and your proposed NMU to the BTS. The ``nmudiff`` script
in the ``devscripts`` package might be helpful.

While preparing the patch, you had better be aware of any
package-specific practices that the maintainer might be using. Taking
them into account reduces the burden of integrating your changes into
the normal package workflow and thus increases the chances that
integration will happen. A good place to look for possible
package-specific practices is
|debian/README.source|_.

.. |debian/README.source| replace:: ``debian/README.source``
.. _debian/README.source: https://www.debian.org/doc/debian-policy/ch-source.html#s-readmesource

Unless you have an excellent reason not to do so, you must then give
some time to the maintainer to react (for example, by uploading to the
``DELAYED`` queue). Here are some recommended values to use for delays:

-  Upload fixing only release-critical bugs older than 7 days, with no
   maintainer activity on the bug for 7 days and no indication that a
   fix is in progress: 0 days

-  Upload fixing only release-critical bugs older than 7 days: 2 days

-  Upload fixing only release-critical and important bugs: 5 days

-  Other NMUs: 10 days

Those delays are only examples. In some cases, such as uploads fixing
security issues, or fixes for trivial bugs that block a transition, it
is desirable that the fixed package reaches ``unstable`` sooner.

Sometimes, release managers decide to encourage NMUs with shorter delays for
a subset of bugs (e.g release-critical bugs older than 7 days). Also,
some maintainers list themselves in the `Low Threshold NMU
list <https://wiki.debian.org/LowThresholdNmu>`__, and accept that NMUs
are uploaded without delay. But even in those cases, it's still a good
idea to give the maintainer a few days to react before you upload,
especially if the patch wasn't available in the BTS before, or if you
know that the maintainer is generally active.

After you upload an NMU, you are responsible for the possible problems
that you might have introduced. You must keep an eye on the package
(subscribing to the package on the PTS is a good way to achieve this).

This is not a license to perform NMUs thoughtlessly. If you NMU when it
is clear that the maintainers are active and would have acknowledged a
patch in a timely manner, or if you ignore the recommendations of this
document, your upload might be a cause of conflict with the maintainer.
You should always be prepared to defend the wisdom of any NMU you
perform on its own merits.

.. _nmu-changelog:

NMUs and ``debian/changelog``
--------------------------------------------------------------------------------------------------------------------------------

Just like any other (source) upload, NMUs must add an entry to
``debian/changelog``, telling what has changed with this upload. The
first line of this entry must explicitly mention that this upload is an
NMU, e.g.:

::

     * Non-maintainer upload.

The way to version NMUs differs for native and non-native packages.

If the package is a native package (without a Debian revision in the
version number), the version must be the version of the last maintainer
upload, plus ``+nmu``\ *X*, where *X* is a counter starting at ``1``. If
the last upload was also an NMU, the counter should be increased. For
example, if the current version is ``1.5``, then an NMU would get
version ``1.5+nmu1``.

If the package is not a native package, you should add a minor version
number to the Debian revision part of the version number (the portion
after the last hyphen). This extra number must start at ``1``. For
example, if the current version is ``1.5-2``, then an NMU would get
version ``1.5-2.1``. If a new upstream version is packaged in the NMU,
the Debian revision is set to ``0``, for example ``1.6-0.1``.

In both cases, if the last upload was also an NMU, the counter should be
increased. For example, if the current version is ``1.5+nmu3`` (a native
package which has already been NMUed), the NMU would get version
``1.5+nmu4``.

A special versioning scheme is needed to avoid disrupting the
maintainer's work, since using an integer for the Debian revision will
potentially conflict with a maintainer upload already in preparation at
the time of an NMU, or even one sitting in the ftp NEW queue. It also
has the benefit of making it visually clear that a package in the
archive was not made by the official maintainer.

If you upload a package to testing or stable, you sometimes need to
"fork" the version number tree. This is the case for security uploads,
for example. For this, a version of the form ``+deb``\ *X*\ ``u``\ *Y*
should be used, where *X* is the major release number, and *Y* is a
counter starting at ``1``. For example, while |codename-stable|
(Debian |version-stable|) is stable, a security NMU to stable for a
package at version ``1.5-3`` would have version
|version2-revision-deb-version-stable-u1|, whereas a security upload to
|codename-testing| would get version
|version2-revision-deb-version-testing-u1|.

.. _nmu-delayed:

Using the ``DELAYED/`` queue
--------------------------------------------------------------------------------------------------------------------------------

Having to wait for a response after you request permission to NMU is
inefficient, because it costs the NMUer a context switch to come back to
the issue. The ``DELAYED`` queue (see :ref:`delayed-incoming`) allows the developer doing the NMU to
perform all the necessary tasks at the same time. For instance, instead
of telling the maintainer that you will upload the updated package in 7
days, you should upload the package to ``DELAYED/7`` and tell the
maintainer that they have 7 days to react. During this time, the
maintainer can ask you to delay the upload some more, or cancel your
upload.

You can cancel your upload using :ref:`dcut`. In case you uploaded
``foo_1.2-1.1_all.changes`` to a ``DELAYED`` queue, you can run ``dcut cancel
foo_1.2-1.1_all.changes`` to cancel your upload. The ``.changes`` file
does not need to be present locally as you instruct :ref:`dcut` to upload a
command file removing a remote filename. The ``.changes`` file name is the same
that you used when uploading.

The ``DELAYED`` queue should not be used to put additional pressure on
the maintainer. In particular, it's important that you are available to
cancel or delay the upload before the delay expires since the maintainer
cannot cancel the upload themselves.

If you make an NMU to ``DELAYED`` and the maintainer updates the package
before the delay expires, your upload will be rejected because a newer
version is already available in the archive. Ideally, the maintainer
will take care to include your proposed changes (or at least a solution
for the problems they address) in that upload.

.. _nmu-maintainer:

NMUs from the maintainer's point of view
--------------------------------------------------------------------------------------------------------------------------------

When someone NMUs your package, this means they want to help you to keep
it in good shape. This gives users fixed packages faster. You can
consider asking the NMUer to become a co-maintainer of the package.
Receiving an NMU on a package is not a bad thing; it just means that the
package is interesting enough for other people to work on it.

To acknowledge an NMU, include its changes and changelog entry in your
next maintainer upload. If you do not acknowledge the NMU by including
the NMU changelog entry in your changelog, the bugs will remain closed
in the BTS but will be listed as affecting your maintainer version of
the package.

Note that if you ever need to revert a NMU that packages a new upstream
version, it is recommended to use a fake upstream version like
*CURRENT*\ ``+really``\ *FORMER* until one can upload the latest version
again. More information can be found in
https://www.debian.org/doc/debian-policy/ch-controlfields.html#epochs-should-be-used-sparingly\ .

.. _nmu-binnmu:

Source NMUs vs Binary-only NMUs (binNMUs)
--------------------------------------------------------------------------------------------------------------------------------

The full name of an NMU is *source NMU*. There is also another type,
namely the *binary-only NMU*, or *binNMU*. A binNMU is also a package
upload by someone other than the package's maintainer. However, it is a
binary-only upload.

When a library (or other dependency) is updated, the packages using it
may need to be rebuilt. Since no changes to the source are needed, the
same source package is used.

BinNMUs are usually triggered on the buildds by wanna-build. An entry is
added to ``debian/changelog``, explaining why the upload was needed and
increasing the version number as described in :ref:`binary-only-nmu`. This entry should not be included
in the next upload.

Buildds upload packages for their architecture to the archive as
binary-only uploads. Strictly speaking, these are binNMUs. However, they
are not normally called NMU, and they don't add an entry to
``debian/changelog``.

.. _nmu-qa-upload:

NMUs vs QA uploads
--------------------------------------------------------------------------------------------------------------------------------

NMUs are uploads of packages by somebody other than their assigned
maintainer. There is another type of upload where the uploaded package
is not yours: QA uploads. QA uploads are uploads of orphaned packages.

QA uploads are very much like normal maintainer uploads: they may fix
anything, even minor issues; the version numbering is normal, and there
is no need to use a delayed upload. The difference is that you are not
listed as the ``Maintainer`` or ``Uploader`` for the package. Also, the
changelog entry of a QA upload has a special first line:

::

    * QA upload.

If you want to do an NMU, and it seems that the maintainer is not
active, it is wise to check if the package is orphaned (this information
is displayed on the package's Package Tracking System page). When doing
the first QA upload to an orphaned package, the maintainer should be set
to ``Debian QA Group
<packages@qa.debian.org>``. Orphaned packages which did not yet have a
QA upload still have their old maintainer set. There is a list of them
at https://qa.debian.org/orphaned.html\ .

Instead of doing a QA upload, you can also consider adopting the package
by making yourself the maintainer. You don't need permission from
anybody to adopt an orphaned package; you can just set yourself as
maintainer and upload the new version (see :ref:`adopting`).

.. _nmu-team-upload:

NMUs vs team uploads
--------------------------------------------------------------------------------------------------------------------------------

Sometimes you are fixing and/or updating a package because you are
member of a packaging team (which uses a mailing list as ``Maintainer``
or ``Uploader``; see :ref:`collaborative-maint`) but you don't want to add
yourself to ``Uploaders`` because you do not plan to contribute
regularly to this specific package. If it conforms with your team's
policy, you can perform a normal upload without being listed directly as
``Maintainer`` or ``Uploader``. In that case, you should start your
changelog entry with the following line:

::

    * Team upload.

.. _package-salvaging:

Package Salvaging
================================================================================================================================

Package salvaging is the process by which one attempts to save a package
that, while not officially orphaned, appears poorly maintained or
completely unmaintained. This is a weaker and faster procedure than
orphaning a package officially through the powers of the MIA team.
Salvaging a package is not meant to replace MIA handling, and differs in
that it does not imply anything about the overall activity of a
maintainer. Instead, it handles a package maintainership transition for
a single package only, leaving any other package or Debian membership or
upload rights (when applicable) untouched.

Note that the process is only intended for actively taking over
maintainership. Do not start a package salvaging process when you do not
intend to maintain the package for a prolonged time. If you only want to
fix certain things, but not take over the package, you must use the NMU
process, even if the package would be eligible for salvaging. The NMU
process is explained in :ref:`nmu`.

Another important thing to remember: It is not acceptable to hijack
others' packages. If followed, this salvaging process will help you to
ensure that your endeavour is not a hijack but a (legal) salvaging
procedure, and you can counter any allegations of hijacking with a
reference to this process. Thanks to this process, new contributors
should no longer be afraid to take over packages that have been
neglected or entirely forgotten.

The process is split into two phases: In the first phase you determine
whether the package in question is *eligible* for the salvaging process.
Only when the eligibility has been determined you may enter the second
phase, the *actual* package salvaging.

For additional information, rationales and FAQs on package salvaging,
please visit the `Salvaging
Packages <https://wiki.debian.org/PackageSalvaging>`__ page on the
Debian wiki.

.. _ps-eligibility:

When a package is eligible for package salvaging
--------------------------------------------------------------------------------------------------------------------------------

A package becomes eligible for salvaging when it has been neglected by
the current maintainer. To determine that a package has really been
neglected by the maintainer, the following indicators give a rough idea
what to look for:

-  NMUs, especially if there has been more than one NMU in a row.

-  Bugs filed against the package do not have answers from the
   maintainer.

-  Upstream has released several versions, but despite there being a bug
   entry asking for it, it has not been packaged.

-  There are QA issues with the package.

You will have to use your judgement as to whether a given combination
factors constitutes neglect; in case the maintainer disagrees they have
only to say so (see below). If you're not sure about your judgement or
simply want to be on the safe side, there is a more precise (and
conservative) set of conditions in the `Package
Salvaging <https://wiki.debian.org/PackageSalvaging>`__ wiki page. These
conditions represent a current Debian consensus on salvaging criteria.
In any case you should explain your reasons for thinking the package is
neglected when you file an Intent to Salvage bug later.

.. _ps-guidelines:

How to salvage a package
--------------------------------------------------------------------------------------------------------------------------------

If and *only* if a package has been determined to be eligible for
package salvaging, any prospective maintainer may start the following
package salvaging procedure.

1. Open a bug with the severity "important" against the package in
   question, expressing the intent to take over maintainership of the
   package. For this, the title of the bug should start with
   ``ITS: package-name``\  [3]_. You may alternatively offer to only
   take co-maintenance of the package. When you file the bug, you must
   inform all maintainers, uploaders and if applicable the packaging
   team explicitly by adding them to ``X-Debbugs-CC``. Additionally, if
   the maintainer(s) seem(s) to be generally inactive, please inform the
   MIA team by adding ``mia@qa.debian.org`` to ``X-Debbugs-CC`` as well.
   As well as the explicit expression of the intent to salvage, please
   also take the time to document your assessment of the eligibility in
   the bug report, for example by listing the criteria you've applied
   and adding some data to make it easier for others to assess the
   situation.

2. In this step you need to wait in case any objections to the salvaging
   are raised; the maintainer, any current uploader or any member of the
   associated packaging team of the package in question may object
   publicly in response to the bug you've filed within ``21 days``, and
   this terminates the salvaging process.

   The current maintainers may also agree to your intent to salvage by
   filing a (signed) public response to the the bug. They might propose
   that you become a co-maintainer instead of the sole maintainer. On
   team maintained packages, a member of the associated team can accept
   your salvaging proposal by sending out a signed agreement notice to
   the ITS bug, alternatively inviting you to become a new co-maintainer
   of the package. The team may require you to keep the package under
   the team's umbrella, but then may ask or invite you to join the team.
   In any of these cases where you have received the OK to proceed, you
   can upload the new package immediately as the new (co-)maintainer,
   without the need to utilise the ``DELAYED`` queue as described in the
   next step.

3. After the 21 days delay, if no answer has been sent to the bug from
   the maintainer, one of the uploaders or team, you may upload the new
   release of the package into the ``DELAYED`` queue with a minimum
   delay of ``seven days``. You should close the salvage bug in the
   changelog and you must also send an nmudiff to the bug ensuring that
   copies are sent to the maintainer and any uploaders (including teams)
   of the package by ``CC'ing`` them in the mail to the BTS.

   During the waiting time of the ``DELAYED`` queue, the maintainer can
   accept the salvaging, do an upload themselves or (ask to) cancel the
   upload. The latter two of these will also stop the salvaging process,
   but the maintainer must reply to the salvaging bug with more
   information about their action.

.. _collaborative-maint:

Collaborative maintenance
================================================================================================================================

Collaborative maintenance is a term describing the sharing of Debian
package maintenance duties by several people. This collaboration is
almost always a good idea, since it generally results in higher quality
and faster bug fix turnaround times. It is strongly recommended that
packages with a priority of ``standard`` or which are part of the base
set have co-maintainers.

Generally there is a primary maintainer and one or more co-maintainers.
The primary maintainer is the person whose name is listed in the
``Maintainer`` field of the ``debian/control`` file. Co-maintainers are
all the other maintainers, usually listed in the ``Uploaders`` field of
the ``debian/control`` file.

In its most basic form, the process of adding a new co-maintainer is
quite easy:

-  Set up the co-maintainer with access to the sources you build the
   package from. Generally this implies you are using a network-capable
   version control system, such as ``Git``. Salsa (see
   :ref:`salsa-debian-org`) provides Git repositories, amongst other
   collaborative tools.

-  Add the co-maintainer's correct maintainer name and address to the
   ``Uploaders`` field in the first paragraph of the ``debian/control``
   file.

   ::

      Uploaders: John Buzz <jbuzz@debian.org>, Adam Rex <arex@debian.org>

-  Using the PTS (:ref:`pkg-tracker`), the co-maintainers should
   subscribe themselves to the appropriate source package.

Another form of collaborative maintenance is team maintenance, which is
recommended if you maintain several packages with the same group of
developers. In that case, the ``Maintainer`` and ``Uploaders`` field of
each package must be managed with care. It is recommended to choose
between one of the two following schemes:

1. Put the team member mainly responsible for the package in the
   ``Maintainer`` field. In the ``Uploaders``, put the mailing list
   address, and the team members who care for the package.

2. Put the mailing list address in the ``Maintainer`` field. In the
   ``Uploaders`` field, put the team members who care for the package.
   In this case, you must make sure the mailing list accepts bug reports
   without any human interaction (like moderation for non-subscribers).

In any case, it is a bad idea to automatically put all team members in
the ``Uploaders`` field. It clutters the Developer's Package Overview
listing (see :ref:`ddpo`) with packages one doesn't really care for,
and creates a false sense of good maintenance. For the same reason, team
members do not need to add themselves to the ``Uploaders`` field just
because they are uploading the package once, they can do a “Team upload”
(see :ref:`nmu-team-upload`). Conversely, it is a
bad idea to keep a package with only the mailing list address as a
``Maintainer`` and no ``Uploaders``.

.. _testing:

The testing distribution
================================================================================================================================

.. _testing-basics:

Basics
--------------------------------------------------------------------------------------------------------------------------------

Packages are usually installed into the ``testing`` distribution after
they have undergone some degree of testing in ``unstable``.

They must be in sync on all architectures and mustn't have dependencies
that make them uninstallable; they also have to have generally no known
release-critical bugs at the time they're installed into ``testing``.
This way, ``testing`` should always be close to being a release
candidate. Please see below for details.

.. _testing-unstable:

Updates from unstable
--------------------------------------------------------------------------------------------------------------------------------

The scripts that update the ``testing`` distribution are run twice each
day, right after the installation of the updated packages; these scripts
are called ``britney``. They generate the ``Packages`` files for the
``testing`` distribution, but they do so in an intelligent manner; they
try to avoid any inconsistency and to use only non-buggy packages.

The inclusion of a package from ``unstable`` is conditional on the
following:

-  The package must have been available in ``unstable`` for a certain
   number of days, see :ref:`bpp-changelog-urgency`. Please note that
   the urgency is sticky, meaning that the highest urgency uploaded
   since the previous ``testing`` transition is taken into account;

-  It must not have new release-critical bugs (RC bugs affecting the
   version available in ``unstable``, but not affecting the version in
   ``testing``);

-  It must be available on all architectures on which it has previously
   been built in ``unstable``. :ref:`dak-ls` may be of interest
   to check that information;

-  It must not break any dependency of a package which is already
   available in ``testing``;

-  The packages on which it depends must either be available in
   ``testing`` or they must be accepted into ``testing`` at the same
   time (and they will be if they fulfill all the necessary criteria);

-  The phase of the project. I.e. automatic transitions are turned off
   during the *freeze* of the ``testing`` distribution.

To find out whether a package is progressing into ``testing`` or not,
see the ``testing`` script output on the `web page of the testing
distribution <https://www.debian.org/devel/testing>`__, or use the
program ``grep-excuses`` which is in the ``devscripts`` package. This
utility can easily be used in a crontab 5 to keep yourself informed of
the progression of your packages into ``testing``.

The ``update_excuses`` file does not always give the precise reason why
the package is refused; you may have to find it on your own by looking
for what would break with the inclusion of the package. The `testing web
page <https://www.debian.org/devel/testing>`__ gives some more
information about the usual problems which may be causing such troubles.

Sometimes, some packages never enter ``testing`` because the set of
interrelationship is too complicated and cannot be sorted out by the
scripts. See below for details.

Some further dependency analysis is shown on
https://release.debian.org/migration/\  — but be warned: this page
also shows build dependencies that are not considered by britney.

.. _outdated:

Out-of-date
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For the ``testing`` migration script, outdated means: There are
different versions in ``unstable`` for the release architectures (except
for the architectures in outofsync_arches; outofsync_arches is a list of
architectures that don't keep up (in ``britney.py``), but currently,
it's empty). Outdated has nothing whatsoever to do with the
architectures this package has in ``testing``.

Consider this example:

+----------+-------+-----+
|          | alpha | arm |
+==========+=======+=====+
| testing  | 1     | \-  |
+----------+-------+-----+
| unstable | 1     | 2   |
+----------+-------+-----+

The package is out of date on ``alpha`` in ``unstable``, and will not go
to ``testing``. Removing the package would not help at all; the package
is still out of date on ``alpha``, and will not propagate to
``testing``.

However, if ftp-master removes a package in ``unstable`` (here on
``arm``):

+----------+-------+-----+-----------+
|          | alpha | arm | hurd-i386 |
+==========+=======+=====+===========+
| testing  | 1     | 1   | \-        |
+----------+-------+-----+-----------+
| unstable | 2     | \-  | 1         |
+----------+-------+-----+-----------+

In this case, the package is up to date on all release architectures in
``unstable`` (and the extra ``hurd-i386`` doesn't matter, as it's not a
release architecture).

Sometimes, the question is raised if it is possible to allow packages in
that are not yet built on all architectures: No. Just plainly no.
(Except if you maintain glibc or so.)

.. _removals:

Removals from testing
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sometimes, a package is removed to allow another package in: This
happens only to allow *another* package to go in if it's ready in every
other sense. Suppose e.g. that ``a`` cannot be installed with the new
version of ``b``; then ``a`` may be removed to allow ``b`` in.

Of course, there is another reason to remove a package from ``testing``:
it's just too buggy (and having a single RC-bug is enough to be in this
state).

Furthermore, if a package has been removed from ``unstable``, and no
package in ``testing`` depends on it any more, then it will
automatically be removed.

.. _circular:

Circular dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A situation which is not handled very well by britney is if package
``a`` depends on the new version of package ``b``, and vice versa.

An example of this is:

+---+-----------------+-----------------+
|   | testing         | unstable        |
+===+=================+=================+
| a | 1; depends: b=1 | 2; depends: b=2 |
+---+-----------------+-----------------+
| b | 1; depends: a=1 | 2; depends: a=2 |
+---+-----------------+-----------------+

Neither package ``a`` nor package ``b`` is considered for update.

Currently, this requires some manual hinting from the release team.
Please contact them by sending mail to
``debian-release@lists.debian.org`` if this happens to one of your
packages.

.. _s5.13.2.4:

Influence of package in testing
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Generally, there is nothing that the status of a package in ``testing``
means for transition of the next version from ``unstable`` to
``testing``, with two exceptions: If the RC-bugginess of the package
goes down, it may go in even if it is still RC-buggy. The second
exception is if the version of the package in ``testing`` is out of sync
on the different arches: Then any arch might just upgrade to the version
of the source package; however, this can happen only if the package was
previously forced through, the arch is in outofsync_arches, or there was
no binary package of that arch present in ``unstable`` at all during the
``testing`` migration.

In summary this means: The only influence that a package being in
``testing`` has on a new version of the same package is that the new
version might go in easier.

Details
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you are interested in details, this is how britney works:

The packages are looked at to determine whether they are valid
candidates. This gives the update excuses. The most common reasons why a
package is not considered are too young, RC-bugginess, and out of date
on some arches. For this part of britney, the release managers have
hammers of various sizes, called hints (see below), to force britney to
consider a package.

Now, the more complex part happens: Britney tries to update ``testing``
with the valid candidates. For that, britney tries to add each valid
candidate to the testing distribution. If the number of uninstallable
packages in ``testing`` doesn't increase, the package is accepted. From
that point on, the accepted package is considered to be part of
``testing``, such that all subsequent installability tests include this
package. Hints from the release team are processed before or after this
main run, depending on the exact type.

If you want to see more details, you can look it up on
https://release.debian.org/britney/update_output/\ .

The hints are available via
https://release.debian.org/britney/hints/\ , where you can find
the `description <https://release.debian.org/doc/britney/hints.html>`__
as well. With the hints, the Debian Release team can block or unblock
packages, ease or force packages into ``testing``, remove packages from
``testing``, approve uploads to :ref:`t-p-u` or
override the urgency.

.. _t-p-u:

Direct updates to testing
--------------------------------------------------------------------------------------------------------------------------------

The ``testing`` distribution is fed with packages from ``unstable``
according to the rules explained above. However, in some cases, it is
necessary to upload packages built only for ``testing``. For that, you
may want to upload to ``testing-proposed-updates``.

Keep in mind that packages uploaded there are not automatically
processed; they have to go through the hands of the release manager. So
you'd better have a good reason to upload there. In order to know what a
good reason is in the release managers' eyes, you should read the
instructions that they regularly give on
``debian-devel-announce@lists.debian.org``.

You should not upload to ``testing-proposed-updates`` when you can
update your packages through ``unstable``. If you can't (for example
because you have a newer development version in ``unstable``), you may
use this facility. Even if a package is frozen, updates through
``unstable`` are possible, if the upload via ``unstable`` does not
pull in any new dependencies.

Version numbers are usually selected by appending ``+deb``\ *X*\ ``uY``,
where *X* is the major release number of Debian and *Y* is a counter
starting at ``1``. e.g. |version1-revision-deb-version-stable-u1|.

Please make sure you didn't miss any of these items in your upload:

-  Make sure that your package really needs to go through
   ``testing-proposed-updates``, and can't go through ``unstable``;

-  Make sure that you included only the minimal amount of changes;

-  Make sure that you included an appropriate explanation in the
   changelog;

-  Make sure that you've written the testing :ref:`codenames`
   (e.g. |codename-testing|) into your target distribution;

-  Make sure that you've built and tested your package in ``testing``,
   not in ``unstable``;

-  Make sure that your version number is higher than the version in
   ``testing`` and ``testing-proposed-updates``, and lower than in
   ``unstable``;

-  Ask for authorization for uploading from the release managers.

-  After uploading and successful build on all platforms, contact the
   release team at ``debian-release@lists.debian.org`` and ask them to
   approve your upload.

.. _faq:

Frequently asked questions
--------------------------------------------------------------------------------------------------------------------------------

.. _rc:

What are release-critical bugs, and how do they get counted?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

All bugs of some higher severities are by default considered
release-critical; currently, these are ``critical``, ``grave`` and
``serious`` bugs.

Such bugs are presumed to have an impact on the chances that the package
will be released with the ``stable`` release of Debian: in general, if a
package has open release-critical bugs filed on it, it won't get into
``testing``, and consequently won't be released in ``stable``.

The ``unstable`` bug count comprises all release-critical bugs that are
marked to apply to *package*/*version* combinations available in
``unstable`` for a release architecture. The ``testing`` bug count is
defined analogously.

.. _s5.13.4.2:

How could installing a package into ``testing`` possibly break other packages?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The structure of the distribution archives is such that they can only
contain one version of a package; a package is defined by its name. So
when the source package ``acmefoo`` is installed into ``testing``, along
with its binary packages ``acme-foo-bin``, ``acme-bar-bin``,
``libacme-foo1`` and ``libacme-foo-dev``, the old version is removed.

However, the old version may have provided a binary package with an old
soname of a library, such as ``libacme-foo0``. Removing the old
``acmefoo`` will remove ``libacme-foo0``, which will break any packages
that depend on it.

Evidently, this mainly affects packages that provide changing sets of
binary packages in different versions (in turn, mainly libraries).
However, it will also affect packages upon which versioned dependencies
have been declared of the ==, <=, or << varieties.

When the set of binary packages provided by a source package changes in
this way, all the packages that depended on the old binaries will have
to be updated to depend on the new binaries instead. Because installing
such a source package into ``testing`` breaks all the packages that
depended on it in ``testing``, some care has to be taken now: all the
depending packages must be updated and ready to be installed themselves
so that they won't be broken, and, once everything is ready, manual
intervention by the release manager or an assistant is normally
required.

If you are having problems with complicated groups of packages like
this, contact ``debian-devel@lists.debian.org`` or
``debian-release@lists.debian.org`` for help.

.. [1]
   See the `Debian Policy
   Manual <https://www.debian.org/doc/debian-policy/>`__ for guidelines
   on what section a package belongs in.

.. [2]
   In the past, such NMUs used the third-level number on the Debian part
   of the revision to denote their recompilation-only status; however,
   this syntax was ambiguous with native packages and did not allow
   proper ordering of recompile-only NMUs, source NMUs, and security
   NMUs on the same package, and has therefore been abandoned in favor
   of this new syntax.

.. [3]
   ITS is shorthand for *"Intend to Salvage"*

.. [4]
   Though, if a package still is in the upload queue and hasn't
   been moved to Incoming yet, it can be removed. (see
   :ref:`upload-ftp-master`)

.. _backports:

The Stable backports archive
================================================================================================================================

.. _backports-rules:

Basics
--------------------------------------------------------------------------------------------------------------------------------

Once a package reaches the ``testing`` distribution,
it is possible for anyone with upload rights in Debian (see below about
this) to build and upload a backport of that package to ``stable-backports``,
to allow easy installation of the version from ``testing``
onto a system that is tracking the ``stable`` distribution.

One should not upload a version of a package to ``stable-backports``
until the matching version has already reached the ``testing`` archive.

.. _backports-security-upload:

Exception to the testing-first rule
--------------------------------------------------------------------------------------------------------------------------------

The only exception to the above rule,
is when there's an important security fix that deserves a quick upload:
in such a case, there is no need to delay an upload
of the security fix to the ``stable-backports`` archive.
However, it is strongly advised
that the package is first fixed in ``unstable``
before uploading a fix to the ``stable-backports`` archive.

.. _backports-who:

Who can maintain packages in the stable-backports archive?
--------------------------------------------------------------------------------------------------------------------------------

It is not necessarily up to the original package maintainer
to maintain the ``stable-backports`` version of the package.
Anyone can do it,
and one doesn't even need approval from the original maintainer to do so.
It is however good practice to first get in touch
with the original maintainer of the package
before attempting to start
the maintenance of a package in ``stable-backports``.
The maintainer can, if they wish,
decide to maintain the backport themselves,
or help you doing so.
It is not uncommon, for example,
to apply a patch to the unstable version of a package,
to facilitate its backporting.

.. _backports-when:

When can one start uploading to stable-backports?
--------------------------------------------------------------------------------------------------------------------------------

The new ``stable-backports`` is created
before the freeze of the next ``stable`` suite.
However, it is not allowed to upload there
until the very end of the freeze cycle.
The ``stable-backports`` archive
is usually opened a few weeks before the final release
of the next ``stable`` suite,
but it doesn't make sense to upload
until the release has actually happened.

.. _backports-how-long:

How long must a package be maintained when uploaded to stable-backports?
--------------------------------------------------------------------------------------------------------------------------------

The ``stable-backports`` archive
is maintained for bugs and security issues
during the whole life-cycle of the Debian ``stable`` suite.
Therefore, an upload to ``stable-backports``,
implies a willingness to maintain the backported package
for the duration of the ``stable`` suite,
which can be expected to be about 3 years
from its initial release.

The person uploading to backports
is also supposed to maintain the backported packages
for security during the lifetime of ``stable``.

It is to be noted that the ``stable-backports`` isn't part of the LTS
or ELTS effort. The ``stable-backports`` FTP masters will close the
``stable-backports`` repositories for uploads once ``stable`` reaches
end-of-life (ie: when ``stable`` becomes maintained by the LTS team only).
Therefore there won't be any maintenance of packages from ``stable-backports``
after the official end of life of the ``stable`` suite, as uploads will 
not be accepted.

.. _backports-how-often:

How often shall one upload to stable-backports?
--------------------------------------------------------------------------------------------------------------------------------

The packages in backports are supposed to follow
the developments that are happening in Testing.
Therefore, it is expected that any significant update in
``testing`` should trigger an upload into ``stable-backports``,
until the new ``stable`` is released. However,
please do not backport minor version changes without
user visible changes or bugfixes.

.. _backports-know-more:

How can one learn more about backporting?
--------------------------------------------------------------------------------------------------------------------------------

You can learn more about
`how to contribute <https://backports.debian.org/Contribute/>`__
directly on the backport web site.

It is also recommended to read
the `Frequently Asked Questions (FAQ) <https://backports.debian.org/FAQ/>`__.
